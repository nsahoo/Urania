from B2DXFitters import *
from B2DXFitters.WS import WS as WS
from ROOT import *
from ROOT import RooFit
from ROOT import RooWorkspace 
from optparse import OptionParser
from math     import pi, log
from  os.path import exists
import os, sys, gc


def getCombinedData(workspace, decay, mc, mode, sample, year, merge, debug):
    if debug:
        print "[INFO] getCombinedData(...)"
        print "[INFO] workspace: %s"%(workspace.GetName())
        print "[INFO] decay: %s, mode: %s, sample: %s, year: %s, merge %s"%(decay,mode,sample,year,str(merge))
        if mc:
            print "[INFO] You are combining MC data sets"

    t = TString("_")
    decayTS = TString(decay) 
    if mc:
        datasetTS = TString("dataSetMC_")+decayTS+t
    else:
        datasetTS = TString("dataSet_")+decayTS+t
    modeTS = TString(mode)
    sampleTS = TString(sample)
    yearTS = TString(year)
    if merge == "pol" or merge == "both":
        sampleTS = TString("both")
    if merge == "year" or merge == "both":
        yearTS = TString("run1")
    smyhs = GeneralUtils.GetSampleModeYearHypo(sampleTS, modeTS, yearTS, TString(""), merge, debug )
    data = []
    i = 0
    for smyh in smyhs:
        name = datasetTS +smyh
        data.append(GeneralUtils.GetDataSet(workspace,name,debug))
        if i != 0:
            data[0].append(data[i])
        i = i+1

    return data[0]


def getExpectedValue(var,par,year,dsmode,pol,myconfigfile):
    #print var, par, year, dsmode                                                                                                                                                         
    if type(myconfigfile[var][par][year.Data()][dsmode.Data()]) == float:
        return myconfigfile[var][par][year.Data()][dsmode.Data()]
    else:
        return myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]

    exit(0)

def getYield(mode, year, dsmode, pol, myconfigfile):
    
    if type(myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]) == float:
        return myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]
    elif "pol" in merge:
        if (myconfigfile["Yields"][mode][year.Data()][dsmode.Data()].has_key("Up") and myconfigfile["Yields"][mode][year.Data()][dsmode.Data()].has_key("Down")):
            return  myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]["Up"]+myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]["Down"]
        elif myconfigfile["Yields"][mode][year.Data()][dsmode.Data()].has_key("Both"):
            return myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]["Both"]
        else:
            print "[ERROR] Polarity set for merging, no input data" 
            exit(-1) 

    elif myconfigfile["Yields"][mode][y.Data()][dsmode.Data()].has_key(p.Data()):
        return myconfigfile["Yields"][mode][y.Data()][dsmode.Data()][p.Data()]

def getExpectedYield(mode,year,dsmode,pol,mergeList, myconfigfile):
    #print mode, year, dsmode                                                                                                                                                               
    p = GeneralUtils.CheckPolarityCapital(TString(pol),False) 
    y = GeneralUtils.CheckDataYearCapital(TString(year),False)
    yields = 0.0 

    if "run1" or "run2" or "runs" in merge:
        if myconfigfile["Yields"][mode].has_key(y.Data()):
            yields = getYield(mode, y, dsmode, p, myconfigfile)          
        else: 
            Run1 = ["2011","2012"] 
            Run2 = ["2015","2016","2017","2018"]
            if y == "Run1":
                for r in Run1:
                    for my in mergeList:
                        if ( my == r ):
                            if myconfigfile["Yields"][mode].has_key(my):
                                yields += getYield(mode, TString(my), dsmode, p, myconfigfile)
            elif y == "Run2":
                for r in Run2:
                    for my in mergeList:
                        if ( my == r ):
                            if myconfigfile["Yields"][mode].has_key(my):
                                yields += getYield(mode, TString(my), dsmode, p, myconfigfile)
            elif y == "Runs":
                Runs = Run1 + Run2
                for r in Runs:
                    for my in mergeList:
                        if ( my == r ):
                            if myconfigfile["Yields"][mode].has_key(my):
                                yields += getYield(mode, TString(my), dsmode, p, myconfigfile)
    else:
        if year != "":
            yields = getYield(mode, TString(year), dsmode, p, myconfigfile)
        elif year == "run1":
            print "[ERROR] Merge option tells that polarities should be merged, while magnet polarity is %s"%(pol)
            exit(0)
        else:
            print "[ERROR] Wrong year: %s"%(year)
            exit(0)

    return yields


def setConstantIfSoConfigured(var, par, mode, dmode, pol, myconfigfile):
    if type(myconfigfile[par][mode]["Fixed"]) == bool:
        if myconfigfile[par][mode]["Fixed"] == True :
            var.setConstant()
            print "[INFO] Parameter: %s set to be constant with value %lf"%(var.GetName(),var.getValV())
        else:
            print "[INFO] Parameter: %s floats in the fit"%(var.GetName())
    elif myconfigfile[par][mode]["Fixed"].has_key(dmode):
        if myconfigfile[par][mode]["Fixed"][dmode]:
            var.setConstant()
            print "[INFO] Parameter: %s set to be constant with value %lf"%(var.GetName(),var.getValV())
        else:
            print "[INFO] Parameter: %s floats in the fit"%(var.GetName())
    else:
        print "[INFO] Parameter: %s floats in the fit"%(var.GetName())

def getObservables (MDSettings, workData, toys, debug):

#    if (not toys ):
    observables = MDSettings.GetObsSet(False,True,True,True,True, True)
#    else:
#        observables = MDSettings.GetObsSet(False,True,True,True,False, False)

#    if MDSettings.CheckTagVar() == True:
#        tagDecCombName = TString("tagDecComb")
#        tagDecComb = GeneralUtils.GetCategory(workData, tagDecCombName, debug)
#        tagOmegaCombName= TString("tagOmegaComb")
#        tagOmegaComb = GeneralUtils.GetObservable(workData, tagOmegaCombName, debug)

#        observables.add(tagDecComb)
#        observables.add(tagOmegaComb)

    if debug:
        observables.Print("v")

    return observables

def readVariables(myconfigfile, label, prefix, workInt, sm, merge, bound, debug):

    t = TString("_")
    variables = []

    names = []

    for i in range(0,bound):

        labelTS = TString(label)
        print labelTS
        if myconfigfile.has_key(label):
            if myconfigfile[label]["type"] != "RooKeysPdf":

                var = myconfigfile[label]
                for v in var:
                    if v != "type" and v != "scaleSigma" and v != "components" and v!= "name" and v!= "decay":
                        lTS = TString(label)
                        
                        year = myconfigfile[label][v]
                        for y in year:
                            if y != "Fixed":
                                mode = myconfigfile[label][v][y]
                                for m in mode:
                                    pol = myconfigfile[label][v][y][m]
                                    if type(pol) == float:
                                        pol = ["both"]

                                    for p in pol:
                                        pp = GeneralUtils.CheckPolarity(TString(p),False) 
                                        yy = GeneralUtils.CheckDataYear(TString(y),False)
                                        mm = GeneralUtils.CheckDMode(TString(m),False)
                                        if mm == "":
                                            mm = GeneralUtils.CheckKKPiMode(TString(m),False)
                                        if m == "All":
                                            mm = "all"
                                        if yy == "run1": 
                                            yr2 = TString("Run1")
                                        else:
                                            yr2 = TString(y)
                                        if pp == "both":
                                            p2 = TString("Both")
                                        else:
                                            p2 = TString(p) 
                                        t = TString("_")
                                        ssmm = t + pp + t + mm + t + yy

                                        nameVar = TString(prefix) + t + v + ssmm
                                        
                                        if nameVar in names:
                                            continue
                                        else:
                                            names.append(nameVar)
                                        expectedValue = getExpectedValue(label,v,yr2,TString(m),p2,myconfigfile)
                                        vTS = TString(v)
                                        if vTS.Contains("frac"):
                                            boundDown = 0.0
                                            boundUp = 1.0
                                        elif vTS.Contains("mean"):
                                            boundDown = expectedValue-50.0;
                                            boundUp = expectedValue+50.0;
                                        else:
                                            if expectedValue < 0.0:
                                                boundUp = 0.0
                                                boundDown = expectedValue*2.0
                                            else:
                                                boundDown = 0.0
                                                boundUp =expectedValue*2.0

                                        if myconfigfile[label].has_key("scaleSigma") and ( v == "sigma1" or v == "sigma2"):
                                            f1 =  myconfigfile[label]["scaleSigma"][yr2.Data()]["frac1"]
                                            f2 =  myconfigfile[label]["scaleSigma"][yr2.Data()]["frac2"]

                                            if v == "sigma1":
                                                expectedValue2 = getExpectedValue(label,"sigma2",yr2,TString(m),p2,myconfigfile)
                                            elif v == "sigma2":
                                                expectedValue2 = getExpectedValue(label,"sigma1",yr2,TString(m),p2,myconfigfile)

                                            oldvalue = expectedValue
                                            if expectedValue>expectedValue2:
                                                f = f1
                                            else:
                                                f = f2
                                            expectedValue = expectedValue*f
                                            print "[INFO] Change value %s to %s = %s * %s"%(str(oldvalue), str(expectedValue), str(oldvalue), str(f))
                                            
                                        variables.append(WS(workInt, RooRealVar(nameVar.Data() , nameVar.Data(), expectedValue,  boundDown, boundUp)))
                                        setConstantIfSoConfigured(variables[variables.__len__()-1], label, v, m, p, myconfigfile)
        #else:
        #    print "[ERROR] Cannot find the label: %s. Please specify in config file"%(label)
        #    exit(0)

    return workInt 

def getPDFNameFromConfig(myconfigfile, key, var, pdfN, pdfK):

    t = TString("_") 
    names = [] 
    if myconfigfile.has_key(key):
        if myconfigfile[key].has_key("name"):
            year = myconfigfile[key]["name"]
            print year
            for y in year:
                mode = myconfigfile[key]["name"][y]
                print mode
                for m in mode:
                    pol = myconfigfile[key]["name"][y][m]
                    print pol 
                    if type(pol) == float:
                        pol = ["both"]
                    for p in pol:
                        pp = GeneralUtils.CheckPolarity(TString(p),False)
                        yy = GeneralUtils.CheckDataYear(TString(y),False)
                        mm = GeneralUtils.CheckDMode(TString(m),False)
                        if mm == "":
                            mm = GeneralUtils.CheckKKPiMode(TString(m),False)

                        if myconfigfile[key]["name"][y][m] == "Both" or myconfigfile[key]["name"][y][m] == "Up" or myconfigfile[key]["name"][y][m] == "Down":
                            namepdf = myconfigfile[key]["name"][y][m][p]
                        else:
                            namepdf = myconfigfile[key]["name"][y][m]
                        name = var + t + pp + t + mm + t + yy     
                        if name in names:
                            continue
                        else:
                            names.append(name)
                        pdfN = GeneralUtils.AddToList(pdfN, namepdf) 
                        pdfK = GeneralUtils.AddToList(pdfK, name)

    return pdfN, pdfK  

def getType(myconfigfile, key):
    if myconfigfile.has_key(key):
        if myconfigfile[key].has_key("type"):
            return  TString(myconfigfile[key]["type"])
    return  TString("None")

def getPIDKComponents(myconfigfile, key, component):
    if myconfigfile.has_key(key):
        if myconfigfile[key].has_key("components"):
            if myconfigfile[key]["components"][component] == True:
                return "true"
            else:
                return "false"
    return "none"

def getSigOrCombPDF(myconfigfile,keys, typemode, work, workInt,sm, merge, bound,dim, obs, debug):

    beautyMass = obs[0]
    charmMass = obs[1]
    bacPIDK = obs[2]
    if dim > 0:
        prefix1 = typemode+"_"+beautyMass.GetName()
        workInt = readVariables(myconfigfile, keys[0], prefix1, workInt, sm, merge, bound, debug)
    if dim > 1:
        charmMass = obs[1]
        prefix2 = typemode+"_"+charmMass.GetName()
        workInt = readVariables(myconfigfile, keys[1], prefix2, workInt, sm, merge, bound, debug)
    if dim > 2:
        bacPIDK = obs[2]
        prefix3 = typemode+"_"+bacPIDK.GetName()
        workInt = readVariables(myconfigfile, keys[2], prefix3, workInt, sm, merge, bound, debug)

    typeBs = getType(myconfigfile,keys[0])
    typeDs = getType(myconfigfile,keys[1])
    typePIDK = getType(myconfigfile,keys[2])

    types = GeneralUtils.GetList(typeBs, typeDs, typePIDK)

    if dim>2: 
        nameKaon = getPIDKComponents(myconfigfile, keys[2], "Kaon")
        namePion = getPIDKComponents(myconfigfile, keys[2], "Pion")
        nameProton = getPIDKComponents(myconfigfile, keys[2], "Proton")
        pidkNames = GeneralUtils.GetList(nameKaon, namePion, nameProton)
    else:
        pidkNames = GeneralUtils.GetList(TString("None"), TString("None"), TString("None"))
    decay =  TString(myconfigfile["Decay"])
    
    combEPDF = []
    for i in range(0,bound):
        print "I Am here"
        pdfN = GeneralUtils.GetList(TString("pdf_names"))
        pdfK = GeneralUtils.GetList(TString("pdf_keys"))
        
        if dim > 0:
            pdfN, pdfK = getPDFNameFromConfig(myconfigfile, keys[0], prefix1, pdfN, pdfK )
        if dim > 1:
            pdfN, pdfK = getPDFNameFromConfig(myconfigfile, keys[1], prefix2, pdfN, pdfK )
        if dim > 2:
            pdfN, pdfK = getPDFNameFromConfig(myconfigfile, keys[2], prefix3, pdfN, pdfK )

        if debug:    
            GeneralUtils.printList(pdfN)
            GeneralUtils.printList(pdfK)
             

        EPDF = Bs2Dsh2011TDAnaModels.build_SigOrCombo(beautyMass,charmMass, bacPIDK, work, workInt, sm[i], TString(typemode), 
                                                      merge, decay, types, pdfN, pdfK, pidkNames, dim, debug)
    
        t = TString(keys[0])
        if (t.Contains("Signal")):
            combEPDF.append(WS(workInt,EPDF))
        else:
            combEPDF.append(EPDF)

    return combEPDF, workInt


#------------------------------------------------------------------------------                                                                                                
def getBkgTypes(myconfigfile, decay, obsName, debug):
    
    t = "_"
    types = GeneralUtils.GetList(TString("pdf_types"))
    bkgs = GeneralUtils.GetList(TString("bkgs"))

    if myconfigfile.has_key("Backgrounds"):
        modes = myconfigfile["Backgrounds"]
        for mode in modes:
            bkgs = GeneralUtils.AddToList(bkgs, TString(mode))
            if myconfigfile.has_key(mode+"Shape"):
                for name in obsName:
                    if myconfigfile[mode+"Shape"].has_key(name):
                        if myconfigfile[mode+"Shape"][name].has_key("type"):
                            namepdf = mode
                            namepdf = namepdf + t + name + t
                            namepdf = namepdf + myconfigfile[mode+"Shape"][name]["type"]
                            types = GeneralUtils.AddToList(types, namepdf)
                    else:
                        modeTS = TString(mode)
                        decayTS = TString(decay)
                        if name == "BeautyMass":
                            namepdf = modeTS + t + name + t +"RooKeysPdf"
                        elif name == "BacPIDK":
                            if (decayTS.Contains("DsK") and modeTS.Contains("DsK") ) or ( decayTS.Contains("DsPi") and modeTS.Contains("DsPi") ) :
                                namepdf = modeTS + t + name + t +"Signal"
                            else:
                                namepdf = modeTS + t + name + t +"Fixed"
                        elif name == "CharmMass":
                            if decayTS.Contains("Dsst") and modeTS.Contains("Ds"):
                                namepdf = modeTS + t + name + t +"RooKeysPdf"
                            elif decayTS.Contains("Ds") and modeTS.Contains("Ds"):
                                namepdf = modeTS + t + name + t +"Signal"
                            elif decayTS.Contains("Lc") and modeTS.Contains("Lc"):
                                namepdf = modeTS + t + name + t +"Signal"
                            elif (decayTS.Contains("D") and not decayTS.Contains("Ds")) and (modeTS.Contains("Dst")) :
                                namepdf = modeTS + t + name + t +"Signal"
                            elif (decayTS.Contains("D") and not decayTS.Contains("Ds")) and (modeTS.Contains("D") and not modeTS.Contains("Dst")) :
                                namepdf = modeTS + t + name + t +"Signal"
                            else:
                                namepdf = modeTS + t + name + t +"RooKeysPdf"
                        types = GeneralUtils.AddToList(types, namepdf)

            else:
                mode = TString(mode)
                decay = TString(decay) 
                for name in obsName:
                    if name == "BeautyMass":
                        namepdf = mode + t + name + t +"RooKeysPdf"
                    elif name == "BacPIDK":
                        if (decay.Contains("DsK") and mode.Contains("DsK") ) or ( decay.Contains("DsPi") and mode.Contains("DsPi") ) :
                            namepdf = mode + t + name + t +"Signal"
                        else:    
                            namepdf = mode + t + name + t +"Fixed"
                    elif name == "CharmMass":
                        if decay.Contains("Dsst") and mode.Contains("Ds"):
                            namepdf = mode + t + name + t +"RooKeysPdf"
                        elif decay.Contains("Ds") and mode.Contains("Ds"):
                            namepdf = mode + t + name + t +"Signal"
                        elif decay.Contains("Lc") and mode.Contains("Lc"):
                            namepdf = mode + t + name + t +"Signal"
                        elif (decay.Contains("D") and not decay.Contains("Ds")) and (mode.Contains("Dst")) :
                            namepdf = mode + t + name + t +"Signal"
                        elif (decay.Contains("D") and not decay.Contains("Ds")) and (mode.Contains("D") and not mode.Contains("Dst")) :
                            namepdf = mode + t + name + t +"Signal"
                        else: 
                            namepdf = mode + t + name + t +"RooKeysPdf"

                types = GeneralUtils.AddToList(types, namepdf)
        
    else:
        print "-----------------------------------------------------------------------------------------------"
        print "[ERROR] New MDFit requires defintion of basic backgrounds, example:"
        print "configdict[\"Backgrounds\"] = [\"Bs2DsPi\",\"Bd2DPi\"]"  
        exit(0) 

    if debug:
        print "-------------------------------------"
        print "Background PDFs"
        print "-------------------------------------"
        GeneralUtils.printList(types)
        
    return types, bkgs 

#------------------------------------------------------------------------------   
def getShapeTypes(myconfigfile, obsName, debug):
    
    t = "_"
    
    modes = myconfigfile["Yields"]
    types = GeneralUtils.GetList(TString("pdf_types"))

    check = [] 

    for mode in modes:
        if mode == "Combinatorial":
            mode = "CombBkg"
        
        if myconfigfile.has_key(mode+"Shape"): 
            for name in obsName:
                if myconfigfile[mode+"Shape"].has_key(name):
                    if myconfigfile[mode+"Shape"][name].has_key("type"):
                        namepdf = mode 
                        namepdf = namepdf + t + name + t 
                        namepdf = namepdf + myconfigfile[mode+"Shape"][name]["type"]
                        types = GeneralUtils.AddToList(types, namepdf)
                        check.append(namepdf) 

    ### Backwards compatibiliy, old fashion config file
    keysSig  = ["BsSignalShape","DsSignalShape","PIDKSignalShape"]
    keysComb = ["BsCombinatorialShape","DsCombinatorialShape","PIDKCombinatorialShape"]
    if myconfigfile.has_key("BsSignalShape") or myconfigfile.has_key("DsSignalShape") or myconfigfile.has_key("PIDKSignalShape") :
        print "-----------------------------------------------------------------------------------------------"
        print "[WARNING] You are using old fashion shape declaration. It is not as flexible as the new."
        print "[WARNING] Please consider using configdict[\"SignalShape\"][\"BeautyMass\"][\"type\"] instead"
        print "-----------------------------------------------------------------------------------------------"
        j = 0 
        for name in obsName:
            namepdf = "Signal_"+name
            if myconfigfile.has_key(keysSig[j]):
                if myconfigfile[keysSig[j]].has_key("type"):
                    namepdf = namepdf + "_"+myconfigfile[keysSig[j]]["type"] 
                    if namepdf not in check:
                        types = GeneralUtils.AddToList(types, namepdf)
                        check.append(namepdf)
                    else:
                        print "[ERROR] type of PDF already added. Unify your config file!"
                        exit(0)
            j = j + 1 
    if myconfigfile.has_key("BsCombinatorialShape") or myconfigfile.has_key("DsCombinatorialShape") or myconfigfile.has_key("PIDKCombinatorialShape") :
        print "-----------------------------------------------------------------------------------------------"
        print "[WARNING] You are using old fashion shape declaration. It is not as flexible as the new."
        print "[WARNING] Please consider using configdict[\"CombinatorialShape\"][\"BeautyMass\"][\"type\"] instead"
        print "-----------------------------------------------------------------------------------------------"
        j = 0
        for name in obsName:
            namepdf = "CombBkg_"+name
            if myconfigfile.has_key(keysComb[j]):
                if myconfigfile[keysComb[j]].has_key("type"):
                    namepdf = namepdf + "_"+myconfigfile[keysComb[j]]["type"]
                    if namepdf not in check:
                        print namepdf 
                        types = GeneralUtils.AddToList(types, namepdf)
                        check.append(namepdf)
                    else:
                        print "[ERROR] type of PDF already added. Unify your config file!"
                        exit(0)
            j = j + 1
    if debug:
        print "-------------------------------------"
        print "Analytical PDFs"
        print "-------------------------------------"
        GeneralUtils.printList(types)



    return types

#------------------------------------------------------------------------------ 
def readVariablesForShapes(myconfigfile, workInt, obsName, merge, bound, sm, debug):
    newSignal = True
    newCombo = True
    keysSig  = ["BsSignalShape","DsSignalShape","PIDKSignalShape"]
    keysComb = ["BsCombinatorialShape","DsCombinatorialShape","PIDKCombinatorialShape"]

    if not myconfigfile.has_key("Backgrounds"):    
        print "-----------------------------------------------------------------------------------------------"
        print "[ERROR] New MDFit requires defintion of basic backgrounds, example:"
        print "configdict[\"Backgrounds\"] = [\"Bs2DsPi\",\"Bd2DPi\"]"
        exit(0)

    for i in range(0,bound):
#        print i
#        print bound
        yr = GeneralUtils.CheckDataYear(sm[i])
        mm = GeneralUtils.GetModeCapital(sm[i],debug)
#        print yr
        dmode = GeneralUtils.GetModeCapital(sm[i],debug)
        backgrounds = myconfigfile["Backgrounds"]+["Signal","CombBkg"] 
        pol = GeneralUtils.CheckPolarityCapital(sm[i],debug)
        for bkg in backgrounds:
            if bkg == "Signal":
                ### Backwards compatibiliy, old fashion config file 
                if myconfigfile.has_key("BsSignalShape") or myconfigfile.has_key("DsSignalShape") or myconfigfile.has_key("PIDKSignalShape") :
                    newSignal = False
                    print "-----------------------------------------------------------------------------------------------"
                    print "[WARNING] You are using old fashion shape declaration. It is not as flexible as the new."
                    print "[WARNING] Please consider using configdict[\"SignalShape\"][\"BeautyMass\"][\"type\"] instead"
                    print "-----------------------------------------------------------------------------------------------"
            if bkg == "CombBkg" or bkg == "Combinatorial":
                ### Backwards compatibiliy, old fashion config file 
                if myconfigfile.has_key("BsCombinatorialShape") or myconfigfile.has_key("DsCombinatorialShape") or myconfigfile.has_key("PIDKCombinatorialShape") :
                    newCombo = False
                    print "----------------------------------------------------------------------------------------------------"
                    print "[WARNING] You are using old fashion shape declaration. It is not as flexible as the new."
                    print "[WARNING] Please consider using configdict[\"CombinatorialShape\"][\"BeautyMass\"][\"type\"] instead"
                    print "-----------------------------------------------------------------------------------------------------"

            ### Backwards compatibiliy, old fashion config file 
            if bkg == "Signal" and newSignal == False:
                j = 0
                for name in obsName:
                    prefix = "Signal_"+name
                    workInt = readVariables(myconfigfile, keysSig[j], prefix, workInt, sm[i], merge, bound, debug)
                    j = j + 1
            ### Backwards compatibiliy, old fashion config file 
            if (bkg == "CombBkg" or bkg == "Combinatorial") and newCombo == False:
                j = 0
                for name in obsName:
                    prefix = "CombBkg_"+name
                    workInt = readVariables(myconfigfile, keysComb[j], prefix, workInt, sm[i], merge, bound, debug)
                    j = j + 1
            if myconfigfile.has_key(bkg+"Shape"):
                key = bkg+"Shape"
                for name in obsName:
                    if myconfigfile[bkg+"Shape"].has_key(name):
                        prefix = bkg+"_"+name
                        workInt = readVariables(myconfigfile[bkg+"Shape"], name, prefix, workInt, sm[i], merge, bound, debug)

    return workInt 


def checkMerge(mode, pol, year, merge):
    m = list(mode)
    s = list(pol)
    y = list(year) 

    if "pol" in merge:
        s = ["both"]
    
    if "kkpi" in merge:
        if "nonres" and"phipi"and "kstk" in mode:
            m.remove("nonres")
            m.remove("phipi")
            m.remove("kstk")
            m.append("kkpi")
        else:
            print "[ERROR] merge constains kkpi but nonres, phipi and kstk are not specified in mode. Make sure you know what you do."
            exit(-1)
    
    if "run1" in merge:
        run1_years = ["2011","2012"] 
        for run1_year in run1_years:
            if run1_year in year:
                y.remove(run1_year)
        y.append("run1") 

    if "run2" in merge:
        run2_years = ["2015","2016","2017","2018"]
        for run2_year in run2_years:
            if run2_year in year:
                y.remove(run2_year)
        y.append("run2")

    if "runs" in merge:
        y = ["runs"] 

    return s,m,y 


def getSampleModeYear(mode, pol, year, debug):
    
    sm = [] 
    t = "_" 
    for p in pol:
        for m in mode:
            for y in year:
                sm.append(p + t + m + t + y)

    l = getStdVector( sm, debug ) 

    return l

def getStdVector( python_list, debug ):
    
    if len(python_list) !=0:
        stdlist = GeneralUtils.GetList(python_list[0])
    else:
        print "[ERROR] Python list empty"
        exit(-1)

    for i in range(1,len(python_list)):
        stdlist = GeneralUtils.AddToList(stdlist, python_list[i])

    print "[INFO] Std::vector: "
    for i in range(0,len(python_list)):
        print "[INFO] ",stdlist[i]

    return stdlist 

def getTopLevelSMY(mode, pol, year, merge):
    
    if (mode[0] == "all") or ("all" in merge) or (len(mode)==5): 
        m = "all"
    elif ( ("nonres" in merge) and ("phipi" in merge) and ("kstk" in merge) and (len(mode) == 3)):
        m = "3modeskkpi" 
    elif ( mode[0] == "kkpi") or ("kkpi" in merge):
        m = "kkpi"
    else:
        m = mode[0] 

    if ( pol[0] == "both" ) or ( "pol" in merge):
        p = "both"
    else:
        p = pol[0]

    if ( year[0] == "all") or (year[0] == "runs") or ( "runs" in merge ) or (( "run1" in merge ) and ("run2" in merge)): 
        y = "runs"
    elif ( year[0] == "run1") or ( "run1" in merge ):
        y = "run1"
    elif ( year[0] == "run2") or ( "run2" in merge ):
        y = "run2"
    else:
        y = year[0]

    return m, p, y 
