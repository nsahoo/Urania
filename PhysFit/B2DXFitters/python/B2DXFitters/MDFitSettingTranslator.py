from B2DXFitters import *
from ROOT import *


class TranslatorFake: 
    def _init_ ( self ):
        name = "MDFitFakeSettings"
        md = MDFitterSettings(name,name)
        md.SetDataCuts("All","")        
        self.mdfit = md
        return self.mdfit 

class Translator:

    def __init__( self, myconfigfile, name , full) :
        md = MDFitterSettings(name,name)
        years = ["2011","2012","2015","2016","2017","2018"] 

        if myconfigfile.has_key("dataName"):
            md.SetConfigFile(myconfigfile["dataName"])

        if myconfigfile.has_key("BasicVariables"):
            names = myconfigfile["BasicVariables"]
            for name in sorted(names):
                if  name == "BeautyMass":
                    md.SetMassB(myconfigfile["BasicVariables"][name]["InputName"], name,
                                myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "CharmMass":
                    md.SetMassD(myconfigfile["BasicVariables"][name]["InputName"], name,
                                myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BeautyTime":
                    md.SetTime(myconfigfile["BasicVariables"][name]["InputName"], name,
                               myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BeautyTimeErr":
                    md.SetTerr(myconfigfile["BasicVariables"][name]["InputName"], name,
                               myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BacP":
                    md.SetMom(myconfigfile["BasicVariables"][name]["InputName"], name,
                               myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BacPT":
                    md.SetTrMom(myconfigfile["BasicVariables"][name]["InputName"], name,
                                myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BacPIDK":
                    md.SetPIDK(myconfigfile["BasicVariables"][name]["InputName"], name,
                               myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "nTracks":
                    md.SetTracks(myconfigfile["BasicVariables"][name]["InputName"], name,
                                myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BacCharge":
                    md.SetID(myconfigfile["BasicVariables"][name]["InputName"], name,
                                myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif name == "BDTG":
                    md.SetBDTG(myconfigfile["BasicVariables"][name]["InputName"], name,
                               myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif "TagDec" in name or "obsTag" in name:
                    md.AddTagVar(myconfigfile["BasicVariables"][name]["InputName"], name,
                                 myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])
                elif "Mistag" in name or "obsEta" in name:
                    md.AddTagOmegaVar(myconfigfile["BasicVariables"][name]["InputName"], name,
                                      myconfigfile["BasicVariables"][name]["Range"][0], myconfigfile["BasicVariables"][name]["Range"][1])

        else:
            print "[ERROR] You didn't defined any observable!"
            exit(0) 

        if myconfigfile.has_key('TaggingCalibration'):
            tags = ["OS", "SS"]
            for tag in tags:
                if myconfigfile["TaggingCalibration"].has_key(tag):
                    use = True
                    
                    if myconfigfile["TaggingCalibration"][tag].has_key("use"):
                        use = myconfigfile["TaggingCalibration"][tag]["use"] 
                        md.SetCalibration(tag,
                                      myconfigfile["TaggingCalibration"][tag]["p0"],
                                      myconfigfile["TaggingCalibration"][tag]["p1"],
                                      myconfigfile["TaggingCalibration"][tag]["average"],
                                      use)
            
            

        if full == True:            
            if myconfigfile.has_key("ObtainPIDTemplates"):
                size1 = len(myconfigfile["ObtainPIDTemplates"]["Variables"])
                size2 = len(myconfigfile["ObtainPIDTemplates"]["Bins"])
                if size1 != size2:
                    print '[ERROR] Size of variables for PID weighting is not equal to size bins. Please check if you set a number of bins for each variable'
                    exit(0)
                md.SetWeightingDim(size1)
                for var in myconfigfile["ObtainPIDTemplates"]["Variables"]:
                    md.AddPIDWeightVar(var)
                for bin in myconfigfile["ObtainPIDTemplates"]["Bins"]:
                    md.AddPIDWeightBin(bin)
            
            if myconfigfile.has_key('Calibrations'):
                year = myconfigfile["Stripping"]
                particle = ["Pion","Kaon","Proton","Combinatorial"]
            
                for y in year:
                    for p in particle:
                    
                        if myconfigfile["Calibrations"][y].has_key(p):
                            polarity = myconfigfile["Calibrations"][y][p]
                            for Pol in polarity:
                                if Pol == "Down":
                                    pol = TString("down")
                                elif Pol == "Up":
                                    pol = TString("up")
                                else:
                                    pol = TString("unknown")
                                
                                strip = myconfigfile["Stripping"][y]
                                dataName = ""
                                workName = "" 
                                pidVarName = ""
                                weightName = ""
                                var1Name = ""
                                var2Name = ""
                                fileName = myconfigfile["Calibrations"][y][p][Pol]["FileName"]
                                if  myconfigfile["Calibrations"][y][p][Pol].has_key("Type"):
                                    t = myconfigfile["Calibrations"][y][p][Pol]["Type"]
                                    if t == "Special" or fileName == "":
                                        if myconfigfile["Calibrations"][y][p][Pol].has_key("WorkName"):
                                            workName = myconfigfile["Calibrations"][y][p][Pol]["WorkName"]
                                        if myconfigfile["Calibrations"][y][p][Pol].has_key("DataName"):
                                            dataName = myconfigfile["Calibrations"][y][p][Pol]["DataName"]
                                        if myconfigfile["Calibrations"][y][p][Pol].has_key("PIDVarName"):
                                            pidVarName = myconfigfile["Calibrations"][y][p][Pol]["PIDVarName"]
                                        if myconfigfile["Calibrations"][y][p][Pol].has_key("WeightName"):
                                            weightName = myconfigfile["Calibrations"][y][p][Pol]["WeightName"]
                                        if myconfigfile["Calibrations"][y][p][Pol].has_key("Variables"):
                                            var1Name = myconfigfile["Calibrations"][y][p][Pol]["Variables"][0]
                                            var2Name = myconfigfile["Calibrations"][y][p][Pol]["Variables"][1]
                                        if p == "Combinatorial":
                                            if ( fileName == "" ):
                                                md.SetPIDComboShapeFor5Modes()
                                else:
                                    t = "" 
                             
                                md.SetCalibSample(fileName, workName, dataName, p, y, pol, strip, t, var1Name, var2Name, pidVarName, weightName)


        if myconfigfile.has_key('IntegratedLuminosity'):
            for year in years: 
                if myconfigfile["IntegratedLuminosity"].has_key(year):
                    md.SetLum(year, myconfigfile["IntegratedLuminosity"][year]["Down"], myconfigfile["IntegratedLuminosity"][year]["Up"])
 
        if myconfigfile.has_key('AdditionalVariables'):
            size = len(myconfigfile['AdditionalVariables'])
            for t in myconfigfile['AdditionalVariables']:

                args = [ myconfigfile['AdditionalVariables'][t]["InputName"] ]
                if myconfigfile['AdditionalVariables'][t].has_key('OutputName'):
                    args.append(myconfigfile['AdditionalVariables'][t]["OutputName"])
                else:
                    args.append(t)
                tR = TString(t)
                if (tR.Contains("DECAY") == False and tR.Contains("decay") == False) and ( tR.Contains("DEC") or tR.Contains("dec")):
                    args.append(-1.0)
                    args.append(1.0)
                    args.append("Additional Tagger") 
                else: 
                    args.append(myconfigfile["AdditionalVariables"][t]["Range"][0])
                    args.append(myconfigfile["AdditionalVariables"][t]["Range"][1])
                    args.append("Additional") 
                args.append(t) 
                if myconfigfile['AdditionalVariables'][t].has_key('Log'):
                    args.append(myconfigfile['AdditionalVariables'][t]['Log'])
                md.AddVariable(*args)

        
        if myconfigfile.has_key('AdditionalCuts'):
            Dmodes = ["All","KKPi","KPiPi","PiPiPi","NonRes","KstK","PhiPi","HHHPi0"]
            i = 0
            for Dmode in Dmodes:
                if myconfigfile['AdditionalCuts'].has_key(Dmode):
                    if myconfigfile['AdditionalCuts'][Dmode].has_key("Data"):
                        md.SetDataCuts(Dmode, myconfigfile['AdditionalCuts'][Dmode]['Data'])
                    if myconfigfile['AdditionalCuts'][Dmode].has_key("MC"):
                        md.SetMCCuts(Dmode, myconfigfile['AdditionalCuts'][Dmode]['MC'])
                    if myconfigfile['AdditionalCuts'][Dmode].has_key("MCID"):
                        md.SetIDCut(Dmode, myconfigfile['AdditionalCuts'][Dmode]['MCID'])
                    if myconfigfile['AdditionalCuts'][Dmode].has_key("MCTRUEID"):
                        md.SetTRUEIDCut(Dmode, myconfigfile['AdditionalCuts'][Dmode]['MCTRUEID'])
                    if myconfigfile['AdditionalCuts'][Dmode].has_key("BKGCAT"):
                        md.SetBKGCATCut(Dmode, myconfigfile['AdditionalCuts'][Dmode]['BKGCAT'])
                    if myconfigfile['AdditionalCuts'][Dmode].has_key("DsHypo"):
                        md.SetDsHypoCut(Dmode, myconfigfile['AdditionalCuts'][Dmode]['DsHypo'])

        if myconfigfile.has_key('WeightingMassTemplates'):
            md.SetMassWeighting(False)
            if myconfigfile["WeightingMassTemplates"].has_key("Shift"):
                shift = myconfigfile["WeightingMassTemplates"]["Shift"]
                print shift
                for s in shift:
                    if s in years:
                        var = myconfigfile["WeightingMassTemplates"]["Shift"][s]
                        for v in var:
                            md.SetShift(s,myconfigfile["WeightingMassTemplates"]["Shift"][s][v],v)
                    else:
                        for year in years:
                            md.SetShift(year,myconfigfile["WeightingMassTemplates"]["Shift"][s],s)

                
            hists = myconfigfile["WeightingMassTemplates"]
            for hist in hists:
                if hist != "Shift":
                    if hist != "RatioDataMC":
                        md.SetMassWeighting(True)

                    if myconfigfile["WeightingMassTemplates"][hist].has_key("FileLabel"): 
                        if myconfigfile["WeightingMassTemplates"][hist].has_key("Var"):
                            var = myconfigfile["WeightingMassTemplates"][hist]["Var"]
                        if myconfigfile["WeightingMassTemplates"][hist].has_key("HistName"):
                            histName = myconfigfile["WeightingMassTemplates"][hist]["HistName"]
                        givenYears = myconfigfile["WeightingMassTemplates"][hist]["FileLabel"]
                        for gY in givenYears:
                            fileName = myconfigfile["WeightingMassTemplates"][hist]["FileLabel"][gY]
                            md.SetPIDProperties(hist, gY, fileName, var[0], var[1], histName)

                    else:
                        for year in years:
                            if myconfigfile["WeightingMassTemplates"][hist].has_key(year):
                                if myconfigfile["WeightingMassTemplates"][hist][year].has_key("Var"):
                                    var = myconfigfile["WeightingMassTemplates"][hist][year]["Var"]
                                if myconfigfile["WeightingMassTemplates"][hist][year].has_key("HistName"):
                                    histName = myconfigfile["WeightingMassTemplates"][hist][year]["HistName"]
                                if myconfigfile["WeightingMassTemplates"][hist][year].has_key("FileLabel"):
                                    fileName = myconfigfile["WeightingMassTemplates"][hist][year]["FileLabel"]
                                md.SetPIDProperties(hist, year, fileName, var[0], var[1], histName)

            if myconfigfile["WeightingMassTemplates"].has_key("RatioDataMC"):
                md.SetRatioDataMC(True)

        if myconfigfile.has_key('DsChildrenPrefix'):
            if myconfigfile['DsChildrenPrefix'].has_key("Child1"):
                md.SetChildPrefix(0,myconfigfile['DsChildrenPrefix']['Child1'])
            if myconfigfile['DsChildrenPrefix'].has_key("Child2"):
                md.SetChildPrefix(1,myconfigfile['DsChildrenPrefix']['Child2'])
            if myconfigfile['DsChildrenPrefix'].has_key("Child3"):
                md.SetChildPrefix(2,myconfigfile['DsChildrenPrefix']['Child3'])
            
        self.mdfit = md
        
    def getConfig( self ) :
        return self.mdfit
