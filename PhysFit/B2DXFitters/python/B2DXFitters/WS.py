"""
@file WS.py

@author Manuel Schiller <manuel.schiller@nikhef.nl>
@date 2014-04-29

@brief build Gaussian constraints
"""

from ROOT import RooFit

def WS(ws, obj, opts=[]):
    """ "swallow" object into a workspace, returns swallowed object """
    name = obj.GetName()
    wsobj = ws.obj(name)
    if obj.InheritsFrom('RooAbsArg') or obj.InheritsFrom('RooAbsData') or obj.InheritsFrom('RooAbsReal'):
        if None == wsobj:
            ws.__getattribute__('import')(obj, *opts)
            wsobj = ws.obj(name)
        else:
            if wsobj.Class() != obj.Class():
                raise TypeError()
    elif obj.InheritsFrom('RooArgSet'):
        if None == wsobj:
            ws.defineSet(name, obj, True)
            wsobj = ws.set(name)
        else:
            if wsobj.Class() != obj.Class():
                raise TypeError()
    else:
        if None == wsobj:
            ws.__getattribute__('import')(obj, name)
            wsobj = ws.obj(name)
        else:
            if wsobj.Class() != obj.Class():
                raise TypeError()
    return wsobj
