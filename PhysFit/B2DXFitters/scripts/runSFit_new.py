#!/usr/bin/env python
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to run a toy MC fit for the CP asymmetry observables        #
#   in Bs -> Ds K                                                             #
#   with the FitMeTool fitter                                                 #
#                                                                             #
#   Example usage:                                                            #
#      python runBs2DsKCPAsymmObsFitterOnData.py [-d -s]                      #
#                                                                             #
#   Author: Eduardo Rodrigues                                                 #
#   Date  : 14 / 06 / 2011                                                    #
#   Author: Manuel Schiller                                                   #
#   Author: Agnieszka Dziurda                                                 #
#   Author: Vladimir Vava Gligorov                                            #
#   Author: Ulrich Eitschberger
# --------------------------------------------------------------------------- #

# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.
# make sure the environment is set up properly
""":"
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
	 -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do 
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
	# clean up incomplete LHCb software environment so we can run
	# standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        # try to find from where script is executed, use current directory as
        # fallback
        tmp="$(dirname $0)"
        tmp=${tmp:-"$cwd"}
        # convert to absolute path
        tmp=`readlink -f "$tmp"`
        # move up until standalone/setup.sh found, or root reached
        while test \( \! -d "$tmp"/standalone \) -a -n "$tmp" -a "$tmp"\!="/"; do
            tmp=`dirname "$tmp"`
        done
        if test -d "$tmp"/standalone; then
            cd "$tmp"/standalone
            . ./setup.sh
        else
            echo `basename $0`: Unable to locate standalone/setup.sh
            exit 1
        fi
	    cd "$cwd"
        unset tmp
        unset cwd
    fi
fi

# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
	    /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
	        continue
	    fi
	    echo adding $k to LD_PRELOAD
	    if test -z "$LD_PRELOAD"; then
	        export LD_PRELOAD="$k"
	        break 3
	    else
	        export LD_PRELOAD="$LD_PRELOAD":"$k"
	        break 3
	    fi
	done
    done
done

# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
__doc__ = """ real docstring """
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  # noqa
from ROOT import *
import B2DXFitters
from B2DXFitters import *

from optparse import OptionParser
from math     import pi, log
from  os.path import exists
import os, sys, gc
from array import array


gROOT.SetBatch()

AcceptanceFunction       =  'PowLawAcceptance'#BdPTAcceptance'  # None/BdPTAcceptance/DTAcceptanceLHCbNote2007041

# MISCELLANEOUS
bName = 'B_{s}'
#------------------------------------------------------------------------------ 
def printLine(bold):
    if bold:
        print "===============================================================================================================================" 
    else:
        print "-------------------------------------------------------------------------------------------------------------------------------" 

#------------------------------------------------------------------------------ 
def printMessage( message, bold = True ):
    printLine(bold)
    print "[INFO] ",message 
    printLine(bold)

#------------------------------------------------------------------------------                                                                                                                
def setConstantIfSoConfigured(var,myconfigfile) :
    defName = var.GetName()+"_default" 
    if (var.GetName() or defName) in myconfigfile["constParams"] : 
        var.setConstant()
        print "[INFO] Parameter: %s set to be constant with value %lf"%(var.GetName(),var.getValV())
    else:
        print "[INFO] Parameter: %s floats in the fit"%(var.GetName())
        print "[INFO]   ",var.Print() 

#------------------------------------------------------------------------------
def getCPparameters(decay,myconfigfile):

    from B2DXFitters import cpobservables

    if decay.Contains("K"):
        argLf = myconfigfile["StrongPhase"]-myconfigfile["WeakPhase"]
        argLbarfbar = myconfigfile["StrongPhase"]+myconfigfile["WeakPhase"]
        modLf = myconfigfile["ModLf"]
        ACPobs = cpobservables.AsymmetryObservables(argLf,argLbarfbar,modLf,True)
        ACPobs.printtable()
        Cf    = ACPobs.Cf()
        Sf    = ACPobs.Sf()
        Df    = ACPobs.Df()
        Sfbar = ACPobs.Sfbar()
        Dfbar = ACPobs.Dfbar()
    else:
        Cf    = 1.0
        Sf    = 0.0   
        Df    = 0.0
        Sfbar = 0.0
        Dfbar = 0.0
    

    limit = [-3.0,3.0]
    if myconfigfile.has_key("CPlimit"):
        limit[0] = myconfigfile["CPlimit"]["lower"]
        limit[1] = myconfigfile["CPlimit"]["upper"] 

    sigC = RooRealVar('C_%s'%(decay.Data()), 'C coeff.', Cf, limit[0], limit[1])
    sigS = RooRealVar('S_%s'%(decay.Data()), 'S coeff.', Sf, limit[0], limit[1])
    sigD = RooRealVar('D_%s'%(decay.Data()), 'D coeff.', Df, limit[0], limit[1])
    sigCbar = RooFormulaVar('Cbar_%s'%(decay.Data()),'Cbar coeff.', "-1*@0",RooArgList(sigC))
#    sigCbar = RooRealVar('Cbar_%s'%(decay.Data()), 'Cbar coeff.', Cf, limit[0], limit[1])
    sigSbar = RooRealVar('Sbar_%s'%(decay.Data()), 'Sbar coeff.', Sfbar, limit[0], limit[1])
    sigDbar = RooRealVar('Dbar_%s'%(decay.Data()), 'Dbar coeff.', Dfbar, limit[0], limit[1])
    setConstantIfSoConfigured(sigC,myconfigfile)
    setConstantIfSoConfigured(sigS,myconfigfile)
    setConstantIfSoConfigured(sigD,myconfigfile)
#    setConstantIfSoConfigured(sigCbar,myconfigfile)
    setConstantIfSoConfigured(sigSbar,myconfigfile)
    setConstantIfSoConfigured(sigDbar,myconfigfile)
    
    return sigC, sigS, sigD, sigCbar, sigSbar, sigDbar

#------------------------------------------------------------------------------
def getTagEff(myconfig, ws, mdSet, par, lab):
    numTag = mdSet.GetNumTagVar()
    tagList = []
    for i in range(0,numTag):
        if mdSet.CheckUseTag(i) == True:
            tagList.append(str(mdSet.GetTagMatch(i)))

    from B2DXFitters.WS import WS as WS
    tagEffList = RooArgList()
    tagEffSig = [] 
    tagEff = 0.0 
    
    if tagList.__len__() == 1:
        if myconfig["TaggingCalibration"].has_key(tagList[0]): 
            if myconfig["TaggingCalibration"][tagList[0]].has_key(par):
                tagEff = myconfig["TaggingCalibration"][tagList[0]][par]
                varName = par+'_'+tagList[0]
            elif myconfig["TaggingCalibration"][tagList[0]].has_key(lab): 
                tagEff = myconfig["TaggingCalibration"][tagList[0]][lab][par]
                varName = par+'_'+tagList[0]+"_"+str(lab) 
            else:
                print "[ERROR] Tagging efficiency is unknown. Please check the config file."
                exit(0)
            tagEffSig.append(WS(ws,RooRealVar(varName, 'Signal tagging efficiency', tagEff, 0.0, 1.0))) 
            setConstantIfSoConfigured(tagEffSig[0],myconfig)
            tagEffList.add(tagEffSig[0])
            
        else:
            print "[ERROR] Tagging efficiency is unknown. Please check the config file."
            exit(0)
    elif tagList.__len__()  == 2:
        if myconfig["TaggingCalibration"][tagList[0]].has_key(lab):
            tagEff0 = myconfig["TaggingCalibration"][tagList[0]][lab][par]
            tagEff1 = myconfig["TaggingCalibration"][tagList[1]][lab][par]
            varName1 = par+'_'+tagList[0]+"_"+str(lab)
            varName2 = par+'_'+tagList[1]+"_"+str(lab)
        else:
            tagEff0 = myconfig["TaggingCalibration"][tagList[0]][par]
            tagEff1 = myconfig["TaggingCalibration"][tagList[1]][par]
            varName1 = par+'_'+tagList[0]
            varName2 = par+'_'+tagList[1]

        tagValue = [tagEff0, tagEff1]
        varName = [varName1, varName2] #[tagEff0 - tagEff0*tagEff1,  tagEff1 - tagEff0*tagEff1, tagEff0*tagEff1]
        i = 0 
        for tag in tagList:
            tagEffSig.append(WS(ws,RooRealVar(varName[i], 'Signal tagging efficiency', tagValue[i], 0.0, 1.0)))
            s = tagEffSig.__len__()
            setConstantIfSoConfigured(tagEffSig[s-1],myconfig)
            tagEffList.add(tagEffSig[s-1]) 
            i = i+1
    else:
        print "[ERROR] More than two taggers are not supported"
        exit(0) 
    return tagEffList, ws 

#------------------------------------------------------------------------------
def getCalibratedMistag(myconfig, ws, mdSet, mistag, par):
    numTag = mdSet.GetNumTagVar()
    tagList = []
    for i in range(0,numTag):
        if mdSet.CheckUseTag(i) == True:
            tagList.append(str(mdSet.GetTagMatch(i)))

    from B2DXFitters.WS import WS as WS
    p0 = []
    p1 = []
    av = []
    
    mistagCalibList = RooArgList()
    mistagCalib = []
    print tagList 


    tagNum = mdSet.CheckNumUsedTag()
    numOfTemp = pow(2,tagNum)-1;
    
    for i in range(0,numOfTemp):
        name = par+'_'+str(tagList[i])
        p0.append(WS(ws,RooRealVar('p0_'+str(name), 'p0_B_'+str(name), myconfig["TaggingCalibration"][tagList[i]]["p0"], 0.0, 1.0)))
        p1.append(WS(ws,RooRealVar('p1_'+str(name), 'p1_B_'+str(name), myconfig["TaggingCalibration"][tagList[i]]["p1"], 0.0, 2.0)))
        av.append(WS(ws,RooRealVar('average_'+str(name), 'av_B_'+str(name), myconfig["TaggingCalibration"][tagList[i]]["average"], 0.0, 1.0)))
        s = p0.__len__()
        setConstantIfSoConfigured(p0[i],myconfig)
        setConstantIfSoConfigured(p1[i],myconfig)
        setConstantIfSoConfigured(av[i],myconfig)
        mistagCalib.append(MistagCalibration("mistagCalib_"+str(name), "mistagCalib_"+str(name), mistag, p0[i], p1[i], av[i]))
        mistagCalibList.add(mistagCalib[i])

    return mistagCalibList, ws

#------------------------------------------------------------------------------
def getCalibrationParameter(myconfigfile, ws, name, tagList, parName, rangeDown, rangeUp, mean = False): 
    from B2DXFitters.WS import WS as WS
    tagName = str(tagList)
    if myconfigfile["TaggingCalibration"][tagName].has_key(name):
        varName = str(parName)+'_'+str(tagName)+"_"+str(name)
        if ( mean ): 
            varName = str(parName)+'_mean_'+str(tagName)+"_"+str(name)
        var = WS(ws,RooRealVar(varName, varName,  myconfigfile["TaggingCalibration"][tagName][name][parName], rangeDown, rangeUp))
    else: 
        varName = str(parName)+'_'+str(tagName)
        if ( mean ):
            varName = str(parName)+'_mean_'+str(tagName)
        var = WS(ws,RooRealVar(varName, varName,  myconfigfile["TaggingCalibration"][tagName][parName], rangeDown, rangeUp))
    setConstantIfSoConfigured(var,myconfigfile)
    return var, ws 

#------------------------------------------------------------------------------
def getScaledTerr(myconfigfile, ws, name):
    
    from B2DXFitters.WS import WS as WS
    if name != "":
        label = "_"+name
    varNames = ["p0","p1","p2"]     
    values = {}
    
    for varName in varNames: 
        values[varName] = {} 
        values[varName]["val"] = {}
        values[varName]["shared"] = {} 
        
        if myconfigfile["Resolution"].has_key(name):
            values[varName]["val"] = myconfigfile["Resolution"][name]["scaleFactor"][varName]
            values[varName]["shared"] = False
        else:
            values[varName]["val"] = myconfigfile["Resolution"]["scaleFactor"][varName]
            values[varName]["shared"] = True


    trm_scale = {} 

    for varName in varNames: 
        trm_scale[varName] = {} 
        if values[varName]["shared"] == False: 
            trm_scale[varName] = WS(ws, RooRealVar( 'trm_scale_'+varName, 'Gaussian resolution model mean '+varName, values[varName]["val"], 'ps' ))
        else:
            trm_scale[varName] = WS(ws, RooRealVar( 'trm_scale_'+varName, 'Gaussian resolution model mean '+varName, values[varName]["val"], 'ps' ))

    
    print "--------------------------------------------------------------"
    if myconfigfile.has_key("UsedResolution"):
        print "[INFO] Used resolution: ",myconfigfile["UsedResolution"]
    if name != "":
        print "[INFO]    for the label: ",name
    print "[INFO] s0: ",trm_scale["p0"].GetName(), trm_scale["p0"].getValV()
    print "[INFO] s1: ",trm_scale["p1"].GetName(), trm_scale["p1"].getValV()
    print "[INFO] s2: ",trm_scale["p2"].GetName(), trm_scale["p2"].getValV()

    return trm_scale, ws 

#------------------------------------------------------------------------------
def getTrmMean(myconfigfile, ws, name):
    from B2DXFitters.WS import WS as WS
    if name != "":
        label = "_"+name
    if myconfigfile["Resolution"].has_key(name):
        m = WS(ws, RooRealVar( 'trm_mean'+str(name) , 'Gaussian resolution model mean', myconfigfile["Resolution"][name]["meanBias"], 'ps' ))
    else:
        m = WS(ws, RooRealVar( 'trm_mean', 'Gaussian resolution model mean', myconfigfile["Resolution"]["meanBias"], 'ps' ))
    print "--------------------------------------------------------------"
    print "[INFO] mean: ",m.GetName(), m.getValV()

    return m, ws

#------------------------------------------------------------------------------
def getSplinesVariables(myconfigfile, ws, name, i):
    from B2DXFitters.WS import WS as WS
    lab = "_"+name 
    if myconfigfile["Acceptance"].has_key(name):
        var = WS(ws, RooRealVar("var"+str(i+1)+str(lab), "var"+str(i+1)+str(lab), myconfigfile["Acceptance"][name]["values"][i], 0.0, 10.0))
    else:
        var =  WS(ws, RooRealVar("var"+str(i+1), "var"+str(i+1), myconfigfile["Acceptance"]["values"][i], 0.0, 10.0))
    
    #print "[INFO]  ",var.GetName(), var.getValV()
    setConstantIfSoConfigured(var,myconfigfile)
    
    return var, ws

#------------------------------------------------------------------------------
def getProd(myconfigfile, ws, name):
    from B2DXFitters.WS import WS as WS
    if myconfigfile.has_key("Asymmetries"):
        if myconfigfile["Asymmetries"].has_key(name):
            value = myconfigfile["Asymmetries"][name]["Production"]
            varName = "aprod_"+name
        else:
            value = myconfigfile["Asymmetries"]["Production"]
            varName = "aprod"

        var = WS(ws, RooConstVar(varName,varName,value))
    else:
        var = WS(ws, RooConstVar("aprod","aprod",0))

    print "[INFO] Production asymmetry: ",var.GetName()," set to be: ",var.getValV()

    return var, ws 

#------------------------------------------------------------------------------
def getDet(myconfigfile, ws, name):
    from B2DXFitters.WS import WS as WS
    if myconfigfile.has_key("Asymmetries"):
        if myconfigfile["Asymmetries"].has_key(name):
            value = myconfigfile["Asymmetries"][name]["Detection"]
            varName = "adet_"+name
        else:
            value = myconfigfile["Asymmetries"]["Detection"]
            varName = "adet"

        var = WS(ws, RooRealVar(varName,varName,value))
    else:
        var = WS(ws, RooRealVar("adet","adet",0))

    print "[INFO] Detection asymmetry: ",var.GetName()," set to be: ",var.getValV()

    return var, ws

#------------------------------------------------------------------------------
def runSFit(debug, wsname,
            pereventmistag, pereventterr, 
            toys, pathName, treeName, workName, 
            configName, scan, 
            binned, plotsWeights, 
            sample, mode, year, merge,
            unblind, mc,
            sim, jobs) :

    from B2DXFitters import taggingutils, cpobservables
    from B2DXFitters.mdfitutils import getCombinedData as getCombinedData
    from B2DXFitters.mdfitutils import getStdVector as getStdVector
    from B2DXFitters.mdfitutils import checkMerge as checkMerge
    from B2DXFitters.mdfitutils import getSampleModeYear as getSampleModeYear

    # Get the configuration file
    myconfigfilegrabber = __import__(configName,fromlist=['getconfig']).getconfig
    myconfigfile = myconfigfilegrabber()

    print "=========================================================="
    print "FITTER IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS"
    for option in myconfigfile :
        if option == "constParams" :
            for param in myconfigfile[option] :
                print param, "is constant in the fit"
        else :
            print option, " = ", myconfigfile[option] 
    print "=========================================================="
    
    RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-7)
    RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-7)
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooIntegrator1D').setCatLabel('extrapolation','WynnEpsilon')
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooIntegrator1D').setCatLabel('maxSteps','1000')
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooIntegrator1D').setCatLabel('minSteps','0')
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooAdaptiveGaussKronrodIntegrator1D').setCatLabel('method','21Points')
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooAdaptiveGaussKronrodIntegrator1D').setRealValue('maxSeg', 1000)

    if sim:
        if myconfigfile.has_key("Labels") == False:
            print "[ERROR] For simultanous fit you need \"Labels\" in your config file, an example: configdict[\"Labels\"] = [\"20152016\",\"2017\"]" 
            sys.exit(-1)

    # Reading data set
    #-----------------------

    from B2DXFitters.MDFitSettingTranslator import Translator
    mdt = Translator(myconfigfile,"MDSettings",False)
    MDSettings = mdt.getConfig()
    MDSettings.Print("v")

    from B2DXFitters.WS import WS as WS
    ws = RooWorkspace("intWork","intWork")

    decay = TString(myconfigfile["Decay"])
    hypo = myconfigfile.get("BachelorHypo", "")

    print "[INFO] hypo: ",hypo
    #    Constant
    #---------------------
    zero = RooConstVar('zero', '0', 0.)
    one = RooConstVar('one', '1', 1.)
    minusone = RooConstVar('minusone', '-1', -1.)
    two = RooConstVar('two', '2', 2.)
    label = []
    smy = {} 
    path = {} 

    #   Sample Mode Year Labels 
    #-------------------------------

    config_labels = myconfigfile.get('Labels', ['default'])
    for mode_part, sample_part, year_part, merge_part, label_part, path_part in zip(mode, sample, year, merge, config_labels, pathName):
        s, m, y = checkMerge(mode_part, sample_part, year_part,merge_part)
        if debug:
            printMessage("File to open: "+str(pathName),False) 
            printMessage("Initial samples: ",False)
        sm_init = getSampleModeYear(mode_part, sample_part, year_part, debug)
        if debug:
            printMessage("Target samples: ",False)
        sm = getSampleModeYear(m, s, y, debug)
        label.append(label_part)
        path[label_part] = path_part
        smy[label_part] = sm

    #    Reading Data Sets from Files
    #--------------------------------------------
    printMessage("Reading Data Sets from Files ")
 
    if not mc:
        for lab in label: 
            ws = SFitUtils.ReadDataFromSWeights(TString(path[lab]), TString(treeName), MDSettings, smy[lab], hypo, False, toys, False, False, ws, lab, debug) #unweighted data
            ws = SFitUtils.ReadDataFromSWeights(TString(path[lab]), TString(treeName), MDSettings, smy[lab], hypo, True, toys, False, True, ws, lab, debug)  #weighted data 
        ws.Print()
    else:
        ws = GeneralUtils.LoadWorkspace(TString(pathName), TString(workName),debug)

    #    DataSet Reading from Workspace 
    #--------------------------------------------
    printMessage("Reading data from Workspaces")
    
    data = {}  #unweighted data 
    dataW = {} # weighted data 

    for lab in label:
        data[lab] = {}
        dataW[lab] = {}

    if not mc:
        for lab in label: 
            nameData = TString("dataSet_time_") + TString(lab)
            nameDataWA = TString("dataSet_time_weighted_") + TString(lab)
            data[lab] = GeneralUtils.GetDataSet(ws,   nameData, debug)
            dataW[lab] = GeneralUtils.GetDataSet(ws,   nameDataWA, debug)
    else:
        for lab in label:
            data[lab] = getCombinedData(ws, myconfigfile["Decay"], mc, mode, sample, year, merge, debug)
            dataW[lab] = data[lab] 


    nEntries = [] 
    for lab in label: 
        nEntries.append(dataW[lab].numEntries())
        dataW[lab].Print("v")

    # Reading observables from data 
    #-------------------------------------------- 
    printMessage("Reading observables from data")
    
    obs = dataW[label[0]].get()
    time = obs.find(MDSettings.GetTimeVarOutName().Data())
    terr = obs.find(MDSettings.GetTerrVarOutName().Data())
    id = obs.find(MDSettings.GetIDVarOutName().Data())

    # Get Tagger List
    numTag = MDSettings.CheckNumUsedTag()
    tagList = []
    for i in range(0,2):
        if MDSettings.CheckUseTag(i) == True:
            tagList.append(str(MDSettings.GetTagMatch(i)))
            
    tag = []
    mistag = []
    for i in range(0,numTag):
        tag.append(obs.find("TagDec"+tagList[i]))
        mistag.append(obs.find("Mistag"+tagList[i]))
        mistag[i].setRange(0, 0.5)


    observables = RooArgSet(time,id)
    for i in range(0,numTag):
        observables.add(tag[i])

    if debug: 
        observables.Print("v")

    # Plotting sWeights if needed 
    #--------------------------------------------
    if plotsWeights: # and not mc:
        name = TString("sfit")
        obs2 = data.get()
        weight2 = obs2.find("sWeights")
        swpdf = GeneralUtils.CreateHistPDF(data, weight2, name, 100, debug)
        GeneralUtils.SaveTemplate(data, swpdf, weight2, name)
        exit(0)

    # Physical parameters
    #-----------------------
    printMessage("Physical parameters ")

    gammas = RooRealVar('Gammas_%s'%decay.Data(), '%s average lifetime' % bName, myconfigfile["Gammas"], 0., 5., 'ps^{-1}')
    setConstantIfSoConfigured(gammas,myconfigfile)
    deltaGammas = RooRealVar('deltaGammas_%s'%decay.Data(), 'Lifetime difference', myconfigfile["DeltaGammas"], -1., 1., 'ps^{-1}')
    setConstantIfSoConfigured(deltaGammas,myconfigfile)

    deltaMsBlindingState = RooCategory("deltaMsBlindingState", "DeltaMs_{} blind state".format(decay.Data()))
    deltaMsBlindingState.defineType("Unblind", 0)
    deltaMsBlindingState.defineType("Blind", 1)
    deltaMsBlindingState.setLabel("Unblind" if (unblind or toys) else "Blind")
    deltaMsUnblind = RooRealVar('DeltaMs_%s'%decay.Data(), '#Delta m_{s}', myconfigfile["DeltaMs"], 1., 30., 'ps^{-1}')
    deltaMsUnblind.setError(0.5)
    setConstantIfSoConfigured(deltaMsUnblind,myconfigfile)
    if toys:
        unblindTitle = "Simulated"
    elif unblind:
        unblindTitle = "Unblind"
    else:
        unblindTitle = "Blind"
    deltaMs = RooUnblindOffset(
        "{} DeltaMs_{}".format(unblindTitle, decay.Data()),
        '#Delta m_{s}',
        'Pandabaerhatsnichtschwer',
        5 * 0.021,  # 5 sigma scale of current world precision
        deltaMsUnblind,
        deltaMsBlindingState,
    )

    # Decay time acceptance model
    # ---------------------------
    printMessage("Acceptance model: splines")

    tMax = time.getMax()
    tMin = time.getMin()
    binName = TString("splineBinning")
    numKnots = myconfigfile["Acceptance"]["knots"].__len__()
    TimeBin = RooBinning(tMin,tMax,binName.Data())
    for i in range(0, numKnots):
        print "[INFO]   knot %s in place %s "%(str(i), str(myconfigfile["Acceptance"]["knots"][i]))
        TimeBin.addBoundary(myconfigfile["Acceptance"]["knots"][i])

    TimeBin.removeBoundary(tMin)
    TimeBin.removeBoundary(tMax)
    TimeBin.removeBoundary(tMin)
    TimeBin.removeBoundary(tMax)
    TimeBin.Print("v")
    time.setBinning(TimeBin, binName.Data())
    time.setRange(tMin, tMax)
    listCoeff = GeneralUtils.GetCoeffFromBinning(TimeBin, time)
    printLine(False) 

    tacc_list = {} 
    tacc_var = {} 
    spl = {} 

    for lab in label: 
        tacc_list[lab] = {} 
        tacc_var[lab] = {} 
        tacc_list[lab] = RooArgList()
        tacc_var[lab] = []

        for i in range(0,numKnots):
            tacc, ws = getSplinesVariables(myconfigfile, ws, lab, i)
            tacc_var[lab].append(tacc)
            tacc_list[lab].add(tacc_var[lab][i])
            
        if myconfigfile["Acceptance"].has_key(lab): 
            tacc_var[lab].append(WS(ws, RooRealVar("var"+str(numKnots+1)+"_"+str(lab), "var"+str(numKnots+1)+"_"+str(lab), 1.0)))
        else:
            tacc_var[lab].append(WS(ws, RooRealVar("var"+str(numKnots+1), "var"+str(numKnots+1), 1.0)))
    
        lenght = tacc_var[lab].__len__()
        tacc_list[lab].add(tacc_var[lab][lenght-1])
        print "[INFO]   n-2: ",tacc_var[lab][lenght-2].GetName(), tacc_var[lab][lenght-2].getValV()
        print "[INFO]   n-1: ",tacc_var[lab][lenght-1].GetName(), tacc_var[lab][lenght-1].getValV()
        if myconfigfile["Acceptance"].has_key(lab):
            name = "var"+str(numKnots+2)+"_"+str(lab)
        else:
            name = "var"+str(numKnots+2)
        tacc_var[lab].append(WS(ws, RooAddition(name,name, RooArgList(tacc_var[lab][lenght-2],tacc_var[lab][lenght-1]), listCoeff)))
        tacc_list[lab].add(tacc_var[lab][lenght])
        print "[INFO]   n: ",tacc_var[lab][lenght].GetName(), tacc_var[lab][lenght].getValV()

        spl[lab] = {} 
        spl[lab] = RooCubicSplineFun("splinePdf_"+str(lab), "splinePdf_"+str(lab), time, "splineBinning", tacc_list[lab])
        printLine(False) 

    # Decay time resolution model
    # ---------------------------
    trm = []
    terrpdf = []
    
    if not pereventterr:
        print '[INFO] Resolution model: Triple gaussian model'
        trm = PTResModels.tripleGausEffModel( time, spl,
                                              myconfigfile["Resolution"]["scaleFactor"], myconfigfile["Resolution"]["meanBias"],
                                              myconfigfile["Resolution"]["shape"]["sigma1"], 
                                              myconfigfile["Resolution"]["shape"]["sigma2"], 
                                              myconfigfile["Resolution"]["shape"]["sigma3"],
                                              myconfigfile["Resolution"]["shape"]["frac1"], 
                                              myconfigfile["Resolution"]["shape"]["frac2"],
                                              debug
                                              )
        
        terrpdf = None
    else :
        # the decay time error is an extra observable !
        printMessage("Resolution model: Gaussian with per-event observable")

        observables.add( terr )
        trm_scale = RooRealVar( 'trm_scale', 'Gaussian resolution model scale factor', 1.0)
        
        trm_mean = {} 
        scale = {} 
        terr_scaled = {} 
        trm = {} 
        terrpdf = {} 
        
        for lab in label: 
            trm_mean[lab] = {}
            scale[lab] = {}
            terr_scaled[lab] = {}
            trm[lab] = {}
            terrpdf[lab] = {} 

            trm_mean[lab], ws = getTrmMean(myconfigfile, ws, lab) 
            scale[lab], ws = getScaledTerr(myconfigfile, ws, lab)
            terr_scaled[lab] = RooFormulaVar( 'trm_scaled_terr_'+str(lab),"scale", "@0+@1*@3+@2*@3*@3",RooArgList(scale[lab]["p0"],scale[lab]["p1"],scale[lab]["p2"],terr))
            trm[lab] = RooGaussEfficiencyModel("resmodel_"+lab, "resmodel_"+lab, time, spl[lab], trm_mean[lab], terr_scaled[lab], trm_mean[lab], trm_scale )

            terrpdfName = TString("terrpdf_")+TString(lab)
            terrpdf[lab] = GeneralUtils.CreateHistPDF(dataW[lab], terr, terrpdfName, 20, debug)
            GeneralUtils.SaveTemplate(dataW[lab], terrpdf[lab], terr, terrpdfName)
            
        if debug:
            print trm_mean
            print scale
            print terr_scaled
            print trm
            print terrpdf

    # read out if constraints for the tagging calibration parameters should be used
    if myconfigfile.has_key("ConstrainsForTaggingCalib"):
        constraints_for_tagging_calib = myconfigfile["ConstrainsForTaggingCalib"]
    else:
        constraints_for_tagging_calib = False


    # Per-event mistag
    # ---------------------------
    printMessage("Flavour Tagging") 
    printMessage("Flavour Tagging Constrains: "+str(constraints_for_tagging_calib),False)

    if pereventmistag:
        print "[INFO] Mistag model: per-event observable"
        
        p0 = {} 
        dp0 = {}
        p1 = {}
        dp1 = {}

        p0_mean = {}
        dp0_mean = {}
        p1_mean = {}
        dp1_mean = {}

        taggingMultiVarGaussSet = {} #RooArgSet()

        avetacalib = {} #[]

        mistagCalibList = {} #RooArgList()
        constList = {} #RooArgSet()
        

        for i in range(0,numTag):
            observables.add(mistag[i])


        print numTag, tagList.__len__()
        for lab in label:
            p0[lab]  = []
            dp0[lab] = []
            p1[lab]  = []
            dp1[lab] = []
            avetacalib[lab] = []
            for i in range(0,numTag):
                #get mean p0 and p1 as well as delta p0 and delta p1 values
                par_p0, ws = getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "p0", 0., 0.5)
                p0[lab].append(par_p0)
                par_dp0, ws = getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "dp0", -1.0, 1.0)
                dp0[lab].append(par_dp0)
                par_p1, ws = getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "p1", 0.5, 1.5)
                p1[lab].append(par_p1) 
                par_dp1, ws = getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "dp1", -1.0, 1.0)
                dp1[lab].append(par_dp1) 
                par_av, ws = getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "average", 0.0, 1.0)
                avetacalib[lab].append(par_av) 

                if constraints_for_tagging_calib:
                    p0_mean[lab] = []
                    dp0_mean[lab] = [] 
                    p1_mean[lab] = []
                    dp1_mean[lab] = [] 
                    p0_mean[lab].append(getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "p0", 0., 0.5, True))
                    dp0_mean[lab].append(getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "dp0", -1.0, 1.0, True))
                    p1_mean[lab].append(getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "p1", 0.5, 1.5, True))
                    dp1_mean[lab].append(getCalibrationParameter(myconfigfile, ws, lab, tagList[i], "dp1", -1.0, 1.0, True))
    
        if debug:
            print p0
            print p1
            print dp0
            print dp1 
            print avetacalib

        if constraints_for_tagging_calib:
            elem = {} 
            for lab in label:
                print "[INFO] Tagging parameters are constrained in the fit"
                if not myconfigfile["TaggingCalibration"][tagList[i]].has_key("cov"):
                    if not myconfigfile["TaggingCalibration"][tagList[i]][lab].has_key("cov"):
                        print "[ERROR] Covariance matrix of at least one tagger not set. Please check the config file."
                        exit(0)
                elem[lab] = {} 
                elem[lab] = 4*4*[0.]

                for j in range(0,4):
                    for k in range(0,4):
                        if myconfigfile["TaggingCalibration"][tagList[i]].has_key(lab): 
                            val = myconfigfile["TaggingCalibration"][tagList[i]][lab]["cov"][j][k]
                        else: 
                            val = myconfigfile["TaggingCalibration"][tagList[i]]["cov"][j][k]
                        elem[lab][j*4+k] = val 

                getattr(ws,'import')(TMatrixDSym(4),'cov_matrix_'+str(name)+"_"+str(lab))

                ws.obj('cov_matrix_'+str(name)+"_"+str(lab)).SetMatrixArray(array( 'd', elem[lab]))
                print "[INFO] Not yet fully supported" 
                exit(0) 
                taggingMultiVarGaussSet[lab] =  {} 
                taggingMultiVarGaussSet[lab] = RooArgSet()
                taggingMultiVarGaussSet[lab].add(WS(ws,RooMultiVarGaussian('tag_cons_multivg_'+str(name)+"_"+str(lab), 
                                                                           'tag_cons_multivg_'+str(name)+"_"+str(lab),
                                                                           RooArgList(p0[lab][i],dp0[lab][i],p1[lab][i],dp1[lab][i]),
                                                                           RooArgList(p0_mean[lab][i],dp0_mean[lab][i],p1_mean[lab][i],dp1_mean[lab][i]),
                                                                           ws.obj('cov_matrix_'+str(name)+"_"+str(lab)))))

        mistagPDFList = {}
        for lab in label: 
            mistagPDFList[lab] = {} 
            mistagPDFList[lab] = SFitUtils.CreateDifferentMistagTemplates(dataW[lab], MDSettings, 50, True, lab, debug)
    
    else :
        print "[INFO] Mistag model: average mistag" 
        mistagHistPdf = None 
        mistagCalibrated =  mistag 
        

    # CP observables
    # --------------------------
    one1 = RooConstVar('one1', '1', 1.)
    one2 = RooConstVar('one2', '1', 1.)
    
    # Tagging                                                                                                                                                                                  
    # -------
    printMessage("Tagging efficiency", False)
    tagEffSigList = {}
    for lab in label: 
        tagEffSigList[lab] = {}
        tagEffSigList[lab], ws  = getTagEff(myconfigfile,ws,MDSettings,"tagEff",lab)
    if debug:
        print tagEffSigList

    # Production, detector and tagging asymmetries
    # --------------------------------------------
    printMessage("Tagging assymetries", False)
    aTagEffSigList = {} 
    for lab in label:
        aTagEffSigList[lab] = {} 
        aTagEffSigList[lab], ws  = getTagEff(myconfigfile,ws,MDSettings,"aTagEff",lab)
    if debug:
        aTagEffSigList

    printMessage("Production and detection assymetries", False)
    aProd = {} 
    aDet = {} 
    for lab in label:
        aProd[lab] = {}
        aDet[lab] = {} 
        aProd[lab], ws = getProd(myconfigfile, ws, name)
        aDet[lab], ws = getDet(myconfigfile, ws, name)

        #aDet_const_mean = RooConstVar('aDet_const_mean','aDet_const_mean',myconfigfile["Asymmetries"]["detector"])
        #aDet_const_err  = RooConstVar('aDet_const_err', 'aDet_const_err', 0.005)
        #aDet_const_g    = RooGaussian('aDet_const_g','aDet_const_g',aDet,aDet_const_mean,aDet_const_err)
        #constList.add(aDet_const_g)
    if debug:
        print aProd
        print aDet
    
    # Coefficient in front of sin, cos, sinh, cosh
    # --------------------------------------------
    printMessage("Coefficient in front of sin, cos, sinh, cosh") 
    C,S,D, Cbar, Sbar,Dbar = getCPparameters(decay,myconfigfile)
    flag = 0

    otherargs = {} 
    for lab in label: 
        otherargs[lab] = []
        if pereventmistag:
            for i in range(0, numTag):
                otherargs[lab].append(tag[i])
                otherargs[lab].append(mistag[i])
                otherargs[lab].append(p0[lab][i])
                otherargs[lab].append(p1[lab][i])
                otherargs[lab].append(dp0[lab][i])
                otherargs[lab].append(dp1[lab][i])
                otherargs[lab].append(avetacalib[lab][i])
                otherargs[lab].append(tagEffSigList[lab][i])
                otherargs[lab].append(aTagEffSigList[lab][i])
        else:
            print "No per-event mistag not supported! Will certainly crash now..."
            sys.exit(-1)
            
        otherargs[lab].append(aProd[lab])
        otherargs[lab].append(aDet[lab])

    for lab in label:
        for i in range(0, numTag):
            print mistagPDFList[lab][i]
        
    cosh = {}
    sinh = {} 
    cos = {}
    sin = {}
    for lab in label:
        cosh[lab] = {}
        sinh[lab] = {} 
        sin[lab] = {}
        cos[lab] = {} 
        cosh[lab] = DecRateCoeff_Bd('signal_cosh_'+str(lab), 'signal_cosh', DecRateCoeff_Bd.kCosh, id, one1, one2, *otherargs[lab])
        sinh[lab] = DecRateCoeff_Bd('signal_sinh_'+str(lab), 'signal_sinh', DecRateCoeff_Bd.kSinh, id, D, Dbar, *otherargs[lab])
        cos[lab] =  DecRateCoeff_Bd('signal_cos_'+str(lab) , 'signal_cos' , DecRateCoeff_Bd.kCos,  id, C, Cbar, *otherargs[lab])
        sin[lab] =  DecRateCoeff_Bd('signal_sin_'+str(lab), 'signal_sin' , DecRateCoeff_Bd.kSin,  id, S, Sbar, *otherargs[lab])
    
    if debug:
        print cosh
        print sinh
        print cos
        print sin

    #Dec Rates to check significance!
#    Sbar_minus = RooFormulaVar( 'Sbar_minus',"Sbar_minus", "-1*@0",RooArgList(Sbar))
#
#    cosh = DecRateCoeff_Bd('signal_cosh', 'signal_cosh', DecRateCoeff_Bd.kCosh, id, one1, one2, *otherargs)
#    sinh = DecRateCoeff_Bd('signal_sinh', 'signal_sinh', DecRateCoeff_Bd.kSinh, id, D, D, *otherargs)
#    cos =  DecRateCoeff_Bd('signal_cos' , 'signal_cos' , DecRateCoeff_Bd.kCos,  id, C, C, *otherargs)
#    sin =  DecRateCoeff_Bd('signal_sin' , 'signal_sin' , DecRateCoeff_Bd.kSin,  id, Sbar_minus, Sbar, *otherargs)

    #if debug:
    #    print "[INFO] sin, cos, sinh, cosh created"
    #    cosh.Print("v")
    #    sinh.Print("v")
    #    cos.Print("v")
    #    sin.Print("v")
    #    exit(0)

    # Time PDF
    # ---------------------------
    printMessage("Time PDF") 

    tauinv          = Inverse( "tauinv","tauinv", gammas)
    
    timePDF = {}
    for lab in label:
        timePDF[lab] = {} 
        name_time = TString("time_signal")+lab
        timePDF[lab] = RooBDecay(name_time.Data(),name_time.Data(),
                                 time, tauinv, deltaGammas,
                                 cosh[lab], sinh[lab], cos[lab], sin[lab],
                                 deltaMs, trm[lab], RooBDecay.SingleSided)

    if debug:
        for lab in label:
            timePDF[lab].printComponentTree()
            printLine(False) 

    # Conditional PDF gor per-event mistag or per-event terr
    # --------------------------------------------------------
    printMessage("Conditional PDF")
    condPDF = {} 

    if pereventterr and pereventmistag:
        for lab in label: 
            noncondset = RooArgSet(time, id)
            condpdfset = RooArgSet(terrpdf[lab])
            for i in range(0,numTag):
                condPDF[lab] = {} 
                noncondset.add(tag[i])
                condpdfset.add(mistagPDFList[lab][i])
                name_timeterr = TString("signal_TimeTimeerrPdf_")+lab
                # default way with conditional pdf and extra pdfs for terr and all mistags
                condPDF[lab] = RooProdPdf(name_timeterr.Data(), name_timeterr.Data(),
                                          condpdfset, RooFit.Conditional(RooArgSet(timePDF[lab]), noncondset))
                # new idea to use only terr as external PDF (maybe even this is not necessary) => need to use RooConditional in FitTo later
                #        totPDF = RooProdPdf(name_timeterr.Data(), name_timeterr.Data(),
                #                                     RooArgSet(terrpdf), RooFit.Conditional(RooArgSet(timePDF), RooArgSet(time, id, tag[0], tag[1], mistag[0], mistag[1])))
                
    else:
        condPDF = timePDF

    if debug:
        for lab in label:
            condPDF[lab].printComponentTree()
            printLine(False)

    # Binning data set if needed 
    # --------------------------------------------------------
    dataWA = {}
    if binned:
        printMessage("Binning dats set: --binned = "+str(binned))
        time.setBins(150)
        terr.setBins(10)
        if pereventmistag:
            for i in range(0, numTag):
                mistag[i].setBins(10)
        dataWA_binned = {}

        for lab in label:
            dataW[lab].Print("v")
            printLine(False)
            observables.Print("v")
            printLine(False)
            dataWA[lab] = {}
            dataWA[lab] = RooDataHist("dataWA_binned"+str(lab),"dataWA_binned"+str(lab),observables,dataW[lab])
            dataWA[lab].Print("v")
    else:
        dataWA = dataW
        
    # Getting the Total PDF 
    # --------------------------------------------------------
    printMessage("Total PDF")
    if sim:
        if binned: 
            printMessage("Binned version not supported yet for Simultaneous fit")
            exit(0)

        printMessage("Simultaneous PDF")

        cat = RooCategory("category", "category")
        for lab in label:
            cat.defineType(lab)
        cat = WS(ws, cat)
        if debug:
            cat.Print("v")
            printLine(False) 

        totPDF = RooSimultaneous("time_signal", "time_signal", cat)
        for lab in label:
            totPDF.addPdf(condPDF[lab], lab)
            
        printMessage("Creating combData",False)
        nameData = TString("combData")
        combData = RooDataSet( nameData.Data(), nameData.Data(), data[label[0]].get(), RooFit.Index(cat), 
                               RooFit.Import(label[0],data[label[0]]), 
                               RooFit.WeightVar("sWeights",True))
        combData.Print("v") 
        for lab in label: 
            if ( lab == label[0] ): continue
            tmpDataName = "combData_"+data[lab].GetName() 
            tmpData = RooDataSet( tmpDataName, tmpDataName, data[lab].get(), RooFit.Index(cat),
                               RooFit.Import(lab,data[lab]),
                               RooFit.WeightVar("sWeights",True))
            if debug:
                printMessage("Adding data set to combData",False) 
                tmpData.Print("v") 

            combData.append(tmpData) 

        printMessage("Final combData",False) 

        combData.Print("v")
        print "[INFO] Weighted data set: ",combData.isWeighted()
        printLine(False)
    else:
        totPDF = condPDF[label[0]]
        combData = dataWA[label[0]]
        combData.SetName("combData") 
        totPDF.SetName("time_signal") 

    if debug:
        totPDF.printComponentTree()
        printLine(False)
    

    '''
    if debug :
    print 'DATASET NOW CONTAINS', nEntries, 'ENTRIES!!!!' 
    data.Print("v")
    for i in range(0,nEntries) : 
    obs = data.get(i)
    obs.Print("v")
    w = data.weight()
    #data.get(i).Print("v")
    #print data.weight()
    #print cos.getValV(obs)
    #print sin.getValV(obs)
    #print cosh.getValV(obs)
    #print sinh.getValV(obs)
    '''        

    # Delta Ms likelihood scan
    # ---------------------------
    if scan:
        RooMsgService.instance().Print('v')
        RooMsgService.instance().deleteStream(1002)
        if debug:
            print "Likelihood scan performing"
        nll = RooNLLVar("nll", "-log(sig)", totPDF[0], dataWA,
                        RooFit.NumCPU(int(jobs)), RooFit.Silence(True))
        pll  = RooProfileLL("pll",  "",  nll, RooArgSet(deltaMs))
        h = pll.createHistogram("h",deltaMs,RooFit.Binning(200))
        h.SetLineColor(kRed)
        h.SetLineWidth(2)
        h.SetTitle("Likelihood Function - Delta Ms")
        like = TCanvas("like", "like", 1200, 800)
        like.cd()
        h.Draw()
        like.Update()
        n = TString("likelihood_Delta_Ms.pdf")
        like.SaveAs(n.Data())
        exit(0)

    # Fitting options
    # -----------------------------
    fitOptsTmp = [
        RooFit.Save(1),
        RooFit.Optimize(2),
        RooFit.Strategy(2),
        RooFit.SumW2Error(True),
        RooFit.Extended(False),
        RooFit.NumCPU(int(jobs)),
        RooFit.Minimizer("Minuit2", "migrad"),
        RooFit.Offset(True),
        RooFit.Hesse(True),
    ]
    if unblind or toys:
        fitOptsTmp.append(RooFit.Verbose(True))
    else:
        fitOptsTmp.append(RooFit.PrintLevel(-1))
        fitOptsTmp.append(RooFit.Warnings(False))
        fitOptsTmp.append(RooFit.PrintEvalErrors(-1))

    if constraints_for_tagging_calib:
        fitOptsTmp.append(RooFit.ExternalConstraints(taggingMultiVarGaussSet))

    fitOpts = RooLinkedList()
    for cmd in fitOptsTmp:
        fitOpts.Add(cmd)

    # Fitting
    # -----------------------------
    printMessage((
        "Fitting --unblind = {} "
        "--binned = {} "
        "--toys = {} "
        "tagging constraints = {}"
        ).format(unblind, binned, toys, constraints_for_tagging_calib))
    fitOpts.Print("v")
    printLine(False)

    myfitresult = totPDF.fitTo(combData, fitOpts)

    # Printing results 
    # -----------------------------
    printMessage("Printing results") 

    if unblind: 
        myfitresult.Print("v")
        myfitresult.correlationMatrix().Print()
        myfitresult.covarianceMatrix().Print()
    else:
        print '[INFO Result] Matrix quality is',myfitresult.covQual()
        par = myfitresult.floatParsFinal()
        const = myfitresult.constPars()
        print "[INFO Result] Status: ",myfitresult.status()
        print "[INFO Result] -------------- Constant parameters ------------- "
        for i in range(0,const.getSize()):
            print "[INFO Result] parameter %s  set to be  %0.4lf"%(const[i].GetName(), const[i].getValV())

        print "[INFO Result] -------------- Floated parameters ------------- "

        for i in range(0,par.getSize()):
            name = TString(par[i].GetName())
            if ( name.Contains(decay)):
                print "[INFO Result] parameter %s = (XXX +/- %0.4lf)"%(par[i].GetName(), par[i].getError())
            else:
                print "[INFO Result] parameter %s = (%0.4lf +/- %0.4lf)"%(par[i].GetName(), par[i].getValV(), par[i].getError())
        
    # Saving results to workspace
    # ----------------------------- 
    workout = RooWorkspace("workspace","workspace")
    getattr(workout,'import')(combData)
    getattr(workout,'import')(totPDF)
    getattr(workout,'import')(myfitresult)
    workout.writeToFile(wsname)

#------------------------------------------------------------------------------

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-d', '--debug',
                    dest    = 'debug',
                    default = False,
                    action  = 'store_true',
                    help    = 'print debug information while processing'
)
parser.add_argument('-s', '--save',
                    dest    = 'wsname',
                    metavar = 'WSNAME',
                    default = 'WS_Time_DsPi.root',  
                    help    = 'save the model PDF and generated dataset to file "WS_WSNAME.root"'
                )

parser.add_argument( '--pereventmistag',
                     dest = 'pereventmistag',
                     default = False,
                     action = 'store_true',
                     help = 'Use the per-event mistag?'
                   )

parser.add_argument( '--pereventterr',
                     dest = 'pereventterr',
                     default = False,
                     action = 'store_true',
                     help = 'Use the per-event time errors?'
                   )

parser.add_argument( '-t','--toys',
                     dest = 'toys',
                     action = 'store_true', 
                     default = False,
                     help = 'are we working with toys?'
                   )

parser.add_argument( '--f','--fileName',
                     dest = 'fileName',
                     nargs='+',
                     help = 'name of the inputfiles'
                 )

parser.add_argument( '--treeName',
                     dest = 'treeName',
                     default = 'merged',
                     help = 'name of the workspace'
                 )
parser.add_argument( '--workName',
                     dest = 'workName',
                     default = 'workspace',
                     help = 'name of the workspace'
                 )
parser.add_argument( '--scan',
                     dest = 'scan',
                     default = False,
                     action = 'store_true'
                 )

parser.add_argument( '--cat',
                     dest = 'cat',
                     default = False,
                     action = 'store_true'
                   )

parser.add_argument( '--configName',
                     dest = 'configName',
                     default = 'Bs2DsPiConfigForNominalDMSFit')

#parser.add_option( '--configNameMDFitter',
#                   dest = 'configNameMD',
#                   default = 'Bs2DsPiConfigForNominalMassFitBDTGA')

parser.add_argument( '--plotsWeights',
                     dest = 'plotsWeights',
                     default = False,
                     action = 'store_true'
                 )

parser.add_argument( '--sim',
                     dest = 'sim',
                     default = False,
                     action = 'store_true'
                 )

parser.add_argument( '-p', '--pol','--polarity',
                   dest = 'pol',
                   nargs='+',
                   default = 'down',
                   help = 'Polarity can be: up, down, both '
                   )
parser.add_argument( '-m', '--mode',
                   dest = 'mode',
                   nargs='+',
                   default = 'kkpi',
                   help = 'Mode can be: all, kkpi, kpipi, pipipi, nonres, kstk, phipi'
                   )

parser.add_argument( '--merge',
                     nargs='+',
                     dest = 'merge',
                     default = "",
                     help = 'merge can be: pol, run1, run2, runs'
                   )
parser.add_argument( '--year',
                   nargs='+',
                   dest = 'year',
                   default = "",
                   help = 'year of data taking can be: 2011, 2012, 2015, 2016, 2017')

parser.add_argument( '--binned',
                     dest = 'binned',
                     default = False,
                     action = 'store_true',
                     help = 'binned data Set'
                   )
parser.add_argument( '--unblind',
                     dest = 'unblind',
                     default = False,
                     action = 'store_true',
                     help = 'unblind results'
                   )
parser.add_argument( '--MC',
                     dest = 'mc',
                     default = False,
                     action = 'store_true',
                     help = 'fit MC samples'
                   )
parser.add_argument('-j', '--jobs', default=1, help='''Specify the number of
                    cores used by RooFit''')


# -----------------------------------------------------------------------------

if __name__ == '__main__' :
  
    try:
        args = parser.parse_args()
    except:
        parser.print_help()
        sys.exit(-1)

    print "=========================================================="
    print "FITTER IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS"
    print vars(args)
    print "=========================================================="

    # check length of list args argument
    if len(args.year) != len(args.fileName):
        raise ConfigurationError('Number of filenames provided must match number of configured years')

    # translate merged years
    tmp_years = []
    tmp_merge = []
    tmp_mode = []

    tmp_pol = []
    if args.pol[0] == 'both':
        pol = ['up', 'down']
    else:
        pol = args.pol

    for i, y in enumerate(args.year):
        if y == '20152016':
            y = ['2015', '2016']
            tmp_merge.append(['pol', 'run2'])  # always merge pol and runs for now
        else:
            y = [y]
            tmp_merge.append(['pol'])  # always merge pol for now
        tmp_years.append(y)
        tmp_pol.append(pol)
        tmp_mode.append(args.mode)
    args.year = tmp_years
    args.pol = tmp_pol
    args.merge = tmp_merge
    args.mode = tmp_mode

    config = args.configName
    last = config.rfind("/")
    directory = config[:last+1]
    configName = config[last+1:]
    p = configName.rfind(".")
    configName = configName[:p]

    import sys
    sys.path.append(directory)

    runSFit( args.debug,
             args.wsname,
             args.pereventmistag,
             args.pereventterr,
             args.toys,
             args.fileName,
             args.treeName,
             args.workName,
             configName,
             #configNameMD,
             args.scan,
             args.binned,
             args.plotsWeights,
             args.pol, 
             args.mode,
             args.year,
             args.merge,
             args.unblind,
             args.mc,
             args.sim,
             args.jobs,
    )
# -----------------------------------------------------------------------------
