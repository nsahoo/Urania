#!/usr/bin/env python
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to plot the Bd -> D pi time models                          #
#                                                                             #
#   Example usage:                                                            #
#      python plotBs2DsPiTimeModelsOnData.py WS_Time_DsPi.root                #
#                                                                             #
#   Author: Eduardo Rodrigues                                                 #
#   Date  : 01 / 06 / 2011                                                    #
#   Author: Agnieszka Dziurda                                                 #
#   Author: Vladimir Vava Gligorov                                            #
#                                                                             #
# --------------------------------------------------------------------------- #

# -----------------------------------------------------------------------------
# settings for running without GaudiPython
# -----------------------------------------------------------------------------
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
        # clean up incomplete LHCb software environment so we can run
        # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
	export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        # try to find from where script is executed, use current directory as
        # fallback
        tmp="$(dirname $0)"
        tmp=${tmp:-"$cwd"}
        # convert to absolute path
        tmp=`readlink -f "$tmp"`
        # move up until standalone/setup.sh found, or root reached
        while test \( \! -d "$tmp"/standalone \) -a -n "$tmp" -a "$tmp"\!="/"; do
            tmp=`dirname "$tmp"`
        done
        if test -d "$tmp"/standalone; then
            cd "$tmp"/standalone
            . ./setup.sh
        else
            echo `basename $0`: Unable to locate standalone/setup.sh
            exit 1
        fi
            cd "$cwd"
        unset tmp
        unset cwd
    fi
fi
# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
            /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
                continue
            fi
            echo adding $k to LD_PRELOAD
            if test -z "$LD_PRELOAD"; then
                export LD_PRELOAD="$k"
                break 3
            else
                export LD_PRELOAD="$LD_PRELOAD":"$k"
                break 3
            fi
        done
    done
done
# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
__doc__ = """ real docstring """
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
#"
from B2DXFitters import *
from ROOT import *

from ROOT import RooFit
from optparse import OptionParser
from math     import pi, log
from  os.path import exists
import os, sys, gc
gROOT.SetBatch()
#gROOT.ProcessLine(".x ../root/.rootlogon.C")

# MODELS
signalModelOnly = True

# PLOTTING CONFIGURATION
plotData  =  True
plotModel =  True

#------------------------------------------------------------------------------                                                                                                                                        
def getDataCut(smy):
    c = [ ]
    for s in smy:
        c.append("category==category::%s"%(s))

    cut = c[0]
    for i in range(1,c.__len__()):
        cut = cut +" || " +c[i]
    
    print "Total cut on data: %s"%(cut)

    return cut

#------------------------------------------------------------------------------
def plotDataSet(dataset, frame, bin, sim, cat) :

    if sim: 
        datacut = getDataCut(cat)
        dataset.plotOn( frame,
                        RooFit.Cut(datacut),
                        RooFit.Binning( bin ),
                        RooFit.DataError(RooAbsData.SumW2),
                        RooFit.Name("dataSetCut"))
        dataout = dataset.reduce(datacut) 
        dataout.Print("v")
        print dataout.numEntries(), dataout.sumEntries() 
        return dataout

    else:
        dataset.plotOn(frame,RooFit.Binning(bin),RooFit.DataError(RooAbsData.SumW2),
                       RooFit.Name("dataSetCut"))
        return dataset 
    
#------------------------------------------------------------------------------
def plotFitModel(model, frame, wksp, myconfigfile, logscale, decay, debug, 
                 namePDF, sim, category, cat, data, time) :
    if debug :
        model.Print('t')
        frame.Print('v')

    if sim: 
        components = namePDF[cat[0]]
        comp = cat[0]
        for c in cat:
            if c != cat[0]:
                components = components +","+namePDF[c]
                comp = comp+","+c 

        model.printComponentTree()
        #print 
        if len(cat) == 1:
            fr = model.plotOn(frame,
                              RooFit.ProjWData(RooArgSet(category),data),
                              RooFit.Slice(category,comp), 
                              RooFit.LineColor(kBlue+3),RooFit.Name("FullPdf"))
        else:
            fr = model.plotOn(frame,
                              RooFit.ProjWData(RooArgSet(category),data), 
                              RooFit.LineColor(kBlue+3),RooFit.Name("FullPdf"))
    else:
        fr = model.plotOn(frame,
                          RooFit.LineColor(kBlue+3),RooFit.Name("FullPdf"))

#    exit(0) 
    t = "_" 
    if "Acceptance" in myconfigfile.keys():
        
        var = {}
        tacc_list = {} 
        numKnots = myconfigfile["Acceptance"]["knots"].__len__()
        
        tMax = time.getMax()
        tMin = time.getMin()
        binName = TString("splineBinning")
        numKnots = myconfigfile["Acceptance"]["knots"].__len__()
        TimeBin = RooBinning(tMin,tMax,binName.Data())
        for i in range(0, numKnots):
            print "[INFO]   knot %s in place %s "%(str(i), str(myconfigfile["Acceptance"]["knots"][i]))
            TimeBin.addBoundary(myconfigfile["Acceptance"]["knots"][i])

        TimeBin.removeBoundary(tMin)
        TimeBin.removeBoundary(tMax)
        TimeBin.removeBoundary(tMin)
        TimeBin.removeBoundary(tMax)
        TimeBin.Print("v")
        time.setBinning(TimeBin, binName.Data())
        time.setRange(tMin, tMax)
        
        if sim: 
            for c in cat:
                var[c] = [] 
                tacc_list[c] = {} 
                tacc_list[c] = RooArgList()
                for i in range(0,numKnots+1):
                    varName = "var%d"%(int(i+1))
                    vatmp = wksp.var(varName)
                    if not vatmp:
                        varName = varName+t+c
                        vatmp = wksp.var(varName)
                    var[c].append(vatmp) 

                    print "[INFO] Load %s with value %0.3lf"%(var[c][i].GetName(),var[c][i].getValV())
                    tacc_list[c].add(var[c][i])
                varAdd = wksp.obj("var%d"%(numKnots+2))
                if not varAdd:
                    varAdd = wksp.obj("var%d"%(numKnots+2)+t+c)
                varAdd = RooAddition(varAdd)
                print "[INFO] Load %s with value %0.3lf"%(varAdd.GetName(),varAdd.getValV())
                tacc_list[c].add(varAdd)
        else:
            tacc_list["def"] = {}
            tacc_list["def"] = RooArgList()
            var["def"] = []

            # check if knot variables have cat slug
            if wksp.var('var1'):
                knot_slug = ''
            else:
                knot_slug = '_' + cat[0]

            for i in range(0,numKnots+1):
                varName = "var{}{}".format(i+1, knot_slug)
                vatmp = wksp.var(varName)
                if not vatmp:
                    print('Error while loading knot variable. This is the workspace variable content:')
                    wksp.allVars().Print('v')
                    raise Exception('Knot variables {} not present in workspace.'.format((varName)))
                var["def"].append(vatmp)

                print "[INFO] Load %s with value %0.3lf"%(var["def"][i].GetName(),var["def"][i].getValV())
                tacc_list["def"].add(var["def"][i])

            varAdd = RooAddition(wksp.obj("var{}{}".format(numKnots+2, knot_slug)))
            print "[INFO] Load %s with value %0.3lf"%(varAdd.GetName(),varAdd.getValV())

            tacc_list["def"].add(varAdd)
        
    elif "ResolutionAcceptance" in myconfigfile.keys():
        #Create acceptance
        var = []
        tacc_list = RooArgList()
        numKnots = myconfigfile["ResolutionAcceptance"]["Signal"]["Acceptance"]["KnotPositions"].__len__()
        print "[INFO] Number of knots: "+str(numKnots)
        if decay == "Bd2DPi":
            v = 9
        else:
            v = 6

        for i in range(0,numKnots+1):
            if i!=v:
                varName = "Acceptance_SplineAccCoeff%d"%(int(i))
                var.append(wksp.obj(varName))
                print "[INFO] Load %s with value %0.3lf"%(var[i].GetName(),var[i].getValV())
            else:
                var.append( RooConstVar("one","one",1.0) )
                print "[INFO] Load one as coefficient no. %d"%(v)
                            
            tacc_list.add(var[i])

        varName = "Acceptance_SplineAccCoeff%d"%(int(numKnots+1))
        var.append(wksp.obj(varName))
        print "[INFO] Load %s with value %0.3lf"%(var[numKnots+1].GetName(),var[numKnots+1].getValV())
        tacc_list.add(var[numKnots+1])

        #Create binning
        binning = RooBinning(time.getMin(), time.getMax(), 'splineBinning')
        for kn in myconfigfile["ResolutionAcceptance"]["Signal"]["Acceptance"]["KnotPositions"]:
            binning.addBoundary(kn)
        binning.removeBoundary(time.getMin())
        binning.removeBoundary(time.getMax())
        binning.removeBoundary(time.getMin())
        binning.removeBoundary(time.getMax())
        oldBinning, lo, hi = time.getBinning(), time.getMin(), time.getMax()
        time.setBinning(binning, 'splineBinning')
        time.setBinning(oldBinning)
        time.setRange(lo, hi)

    spl = {} 
    if sim: 
        for c in cat:
            spl[c] = {} 
            tacc_list[c].Print("v") 
            spl[c] = RooCubicSplineFun("splinePdf"+str(c), "splinePdf"+str(c), time, "splineBinning", tacc_list[c])
    else:
        spl = RooCubicSplineFun("splinePdf", "splinePdf", time, "splineBinning", tacc_list["def"])

    if logscale:
        from math import log 
        rel = log(frame.GetMaximum())*4.0 
    else:
        rel = frame.GetMaximum()/4.0

    if sim: 
        color= {"20152016":kRed, "2017":kGreen+3, "2018":kOrange} 
        i = 0 
        for c in cat: 
            fr = spl[c].plotOn(frame, RooFit.LineColor(color[c]),  RooFit.Normalization(rel, RooAbsReal.Relative),RooFit.Name("sPline"+c))
            i = i+1
        
        if len(cat) == 1:
            fr = model.plotOn(frame,
                              RooFit.ProjWData(RooArgSet(category),data),
                              RooFit.Slice(category,comp),
                              RooFit.LineColor(kBlue+3),RooFit.Name("FullPdf"))
        else:
            fr = model.plotOn(frame,
                              RooFit.ProjWData(RooArgSet(category),data),
                              RooFit.LineColor(kBlue+3),RooFit.Name("FullPdf"))

    else:
        fr = spl.plotOn(frame, RooFit.LineColor(kRed),  RooFit.Normalization(rel, RooAbsReal.Relative),RooFit.Name("sPline"))
        fr = model.plotOn(frame,
                          RooFit.LineColor(kBlue+3), RooFit.Name("FullPdf"))
            
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------

def getDescription(decay):

    happystar = "#lower[-0.95]{#scale[0.5]{(}}#lower[-0.8]{#scale[0.5]{*}}#lower[-0.95]{#scale[0.5]{)}}"
    happystar2 = "#lower[-0.65]{#scale[0.6]{*}}"
    happypm   = "#lower[-0.95]{#scale[0.6]{#pm}}"
    happymp   = "#lower[-0.95]{#scale[0.6]{#mp}}"
    happystarpm = "#lower[-0.95]{#scale[0.6]{*#pm}}"
    happyplus = "#lower[-0.95]{#scale[0.6]{+}}"
    happymin  = "#lower[-1.15]{#scale[0.7]{-}}"
    happy0   = "#lower[-0.85]{#scale[0.6]{0}}"

    from B2DXFitters import TLatexUtils    
    if decay == "Bs2DsPi":
        desc = "B_{s}#kern[-0.7]{"+happy0+"}#rightarrow D_{s}#kern[-0.3]{"+happymin+"}#kern[0.1]{#pi"+happyplus+"}"
    elif decay == "Bs2DsK":
        desc = "B_{s}#kern[-0.7]{"+happy0+"} #rightarrow D_{s}#kern[-0.3]{"+happymp+"}#kern[0.1]{K"+happypm+"}"
    elif decay == "Bs2DsstPi":
        desc = "B_{s}#kern[-0.7]{"+happy0+"}#rightarrow D_{s}#kern[-0.3]{"+happystar+happymin+"}#kern[0.1]{#pi"+happyplus+"}"
    elif decay == "Bs2DsstK":
        desc = "B_{s}#kern[-0.7]{"+happy0+"} #rightarrow D_{s}#kern[-0.3]{"+happystar+happymp+"}#kern[0.1]{K"+happypm+"}"
    elif decay == "Bd2DPi":
        desc = "B_{d}#kern[-0.7]{"+happy0+"} #rightarrow D#kern[-0.3]{"+happymp+"}#kern[0.1]{#pi"+happypm+"}"
    else:
        desc = "Signal" 

    return desc

#------------------------------------------------------------------------------
def legends(model, frame):
    stat = frame.findObject('data_statBox')
    prefix = 'Sig' if signalModelOnly else 'Tot'
    if not stat: stat = frame.findObject('%sEPDF_tData_statBox' % prefix)
    if stat :
        stat.SetTextSize(0.025)
    pt = frame.findObject('%sEPDF_t_paramBox' % prefix)
    if pt :
        pt.SetTextSize(0.02)
    # Legend of EPDF components
    leg = TLegend(0.56, 0.42, 0.87, 0.62)
    leg.SetFillColor(0)
    leg.SetTextSize(0.02)
    comps = model.getComponents()

    if signalModelOnly :
        pdfName = 'SigEPDF_t'
        pdf = comps.find(pdfName)
        curve = frame.findObject(pdfName + '_Norm[time]')
        if curve : leg.AddEntry(curve, pdf.GetTitle(), 'l')
        return leg, curve
    else :
        pdf1 = comps.find('SigEPDF_t')
        pdfName = 'TotEPDF_t_Norm[time]_Comp[%s]' % 'SigEPDF_t'
        curve1 = frame.findObject(pdfName)
        if curve1 : leg.AddEntry(curve1, pdf1.GetTitle(), 'l')
        pdf = comps.find('Bd2DKEPDF_t')
        pdfName = 'TotEPDF_t_Norm[time]_Comp[%s]' % 'Bd2DKEPDF_t'
        curve2 = frame.findObject(pdfName)
        if curve2 : leg.AddEntry(curve2, pdf.GetTitle(), 'l')
        pdf = comps.find('CombBkgEPDF_t')
        pdfName = 'TotEPDF_t_Norm[time]_Comp[%s]' % 'CombBkgEPDF_t'
        curve3 = frame.findObject(pdfName)
        if curve3 : leg.AddEntry(curve3, pdf.GetTitle(), 'l')
        pdfName = 'TotEPDF_t_Norm[time]_Comp[%s]' % 'CombBkgEPDF_t,Bd2DKEPDF_t'
        curve4 = frame.findObject(pdfName)
        if curve4 :
            leg.AddEntry(curve4, 'All but %s' % pdf1.GetTitle(), 'f')
            curve4.SetLineColor(0)
        pdfName = 'TotEPDF_t_Norm[time]'
        pdf = comps.find('TotEPDF_t')
        curve5 = frame.findObject(pdfName)
        #if curve5 : leg.AddEntry(curve5, pdf.GetTitle(), 'l')
        if curve5 : leg.AddEntry(curve5, 'Model (signal & background) EPDF', 'l')
        return leg, curve4

#------------------------------------------------------------------------------
_usage = '%prog [options] <filename>'

import argparse
parser = argparse.ArgumentParser()


parser.add_argument( 'FILENAME',
                     help = 'Input Filename'
                   )

parser.add_argument('-w', '--workspace',
                   dest = 'wsname',
                   metavar = 'WSNAME',
                   default = 'workspace',
                   help = 'RooWorkspace name as stored in ROOT file'
)

parser.add_argument( '-s', '--suffix',
                   dest = 'sufix',
                   metavar = 'SUFIX',
                   default = '',
                   help = 'Add sufix to output'
                   )

parser.add_argument( '--plotLabel',
                   dest = 'plotLabel',
                   metavar = 'PLOTLABEL',
                   default = 'LHCb',
                   help = 'Plot label'
                   )

parser.add_argument( '--configName',
                    dest = 'configName',
                    default = 'Bs2DsPiConfigForNominalDMSFit')

parser.add_argument( '--bin',
                   dest = 'bin',
                   default = 148,
                   help = 'set number of bins'
                   )

parser.add_argument( '--logscale', '--log',
                   dest = 'log',
                   action = 'store_true',
                   default = False,
                   help = 'log scale of plot'
                   )
parser.add_argument( '--legend',
                   dest = 'legend',
                   action = 'store_true',
                   default = False,
                   help = 'plot legend on the plot'
                   )

parser.add_argument( '-v', '--variable', '--var',
                   dest = 'var',
                   default = 'BeautyTime',
                   help = 'set observable '
                   )

parser.add_argument( '--outdir',
                     dest = 'outdir',
                     default = '',
                     help = 'directory to save plots'
                   )

parser.add_argument( '--dataSetToPlot',
                   dest = 'dataSetToPlot',
                   default = 'combData',
                   help = 'name of RooDataSet to plot'
                   )

parser.add_argument( '--pdfToPlot',
                   dest = 'pdfToPlot',
                   default = 'time_signaldefault',
                   help = 'name of pdf to plot'
                   )

parser.add_argument( '--debug',
                   dest = 'debug',
                   default = False,
                   help = 'verbose output'
                   )

parser.add_argument( '--sim','--simfit',
                     dest = 'sim',
                     action = 'store_true',
                     default = False,
                     help = 'simultaneous fit'
                   )

parser.add_argument( '--cat',
                     dest = 'cat',
                     nargs='+',
                     default = "",
                     help = 'category to plot, for example 20152016 2017'
                     )

#------------------------------------------------------------------------------

if __name__ == '__main__' :

    options = parser.parse_args()

    if not exists( options.FILENAME ) :
        parser.error( 'ROOT file "%s" not found! Nothing plotted.' % FILENAME )
        parser.print_help()

    print "=========================================================="
    print "USING  THE FOLLOWING CONFIGURATION OPTIONS"
    print vars(options)
    print "=========================================================="

    RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-13)
    RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-13)
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooAdaptiveGaussKronrodIntegrator1D').setCatLabel('method','21Points')
    RooAbsReal.defaultIntegratorConfig().getConfigSection('RooAdaptiveGaussKronrodIntegrator1D').setRealValue('maxSeg', 1000)
    # since we have finite ranges, the RooIntegrator1D is best suited to the job
    RooAbsReal.defaultIntegratorConfig().method1D().setLabel('RooAdaptiveGaussKronrodIntegrator1D')
    
    from ROOT import kYellow, kMagenta, kOrange, kCyan, kGreen, kRed, kBlue, kDashed, kBlack
    from ROOT import RooRealVar, RooStringVar, RooFormulaVar, RooProduct
    from ROOT import RooCategory, RooMappedCategory, RooConstVar
    from ROOT import RooArgSet, RooArgList, RooGaussian, RooTruthModel, RooDecay
    from ROOT import RooAddPdf, RooProdPdf, RooExtendPdf, RooGenericPdf, RooAbsReal
    from ROOT import RooFit, FitMeTool, TGraph, TPad, gStyle
    from ROOT import CombBkgPTPdf
    from ROOT import RooBlindTools

    gROOT.SetStyle('Plain')
    #gROOT.SetBatch(False)

    bin = int(options.bin)
    log = options.log
    leg = options.legend
    plotLabel = options.plotLabel
    v = options.var
    varTS = TString(v)

    config = options.configName
    last = config.rfind("/")
    directory = config[:last+1]
    configName = config[last+1:]
    p = configName.rfind(".")
    configName = configName[:p]

    import sys
    sys.path.append(directory)

    myconfigfilegrabber = __import__(configName,fromlist=['getconfig']).getconfig
    myconfigfile = myconfigfilegrabber()

    print "=========================================================="
    print "PREPARING WORKSPACE IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS"
    for option in myconfigfile :
        if option == "constParams" :
            for param in myconfigfile[option] :
                print param, "is constant in the fit"
        else :
            print option, " = ", myconfigfile[option]
    print "=========================================================="

    decay = myconfigfile["Decay"]
    decayTS = TString(decay)

    sfx = options.sufix
    out = options.outdir

    sufixTS = TString(sfx)
    outTS = TString(out)

    f = TFile(options.FILENAME)

    w = f.Get(options.wsname)
    if not w :
        parser.error('Workspace "%s" not found in file "%s"! Nothing plotted.' %\
                      (options.wsname, options.FILENAME))
    else:
        print "Workspace content:"
        w.Print("v")

    f.Close()
    time = w.var(varTS.Data())
    time.setBins(bin) 
    #time.setRange(0.4,15)
    timeDown = time.getMin()
    timeUp = time.getMax()

    cat = options.cat
    category = w.obj('category')

    #time.setRange(timeDown,timeUp)
    sim = options.sim
    if sim:
        print category.Print('v')
        namePDF = {}
        modelPDF = RooSimultaneous("time_signal", "time_signal", category)
        for c in ["20152016","2017", "2018"]:
            namePDF[c] = {}
            namePDF[c] = "time_signal"+c
            tmpPDF =  w.obj(namePDF[c])
            if not tmpPDF:
                exit(0)
            modelPDF.addPdf(tmpPDF,c)
    else:
        modelPDF = w.obj(options.pdfToPlot)
        namePDF = modelPDF.GetName()

    if modelPDF:
        print modelPDF.GetName() 

    dataset  = w.data(options.dataSetToPlot)
    if not dataset: 
        dataset = w.data("dataSet_time_weighted_default") 
    if dataset:
        print dataset.GetName()

    if not (modelPDF and dataset) :
        w.Print('v')
        exit(1)

    canvas = TCanvas("canvas", "canvas", 1200, 1000)
    canvas.cd()

    time = w.var(varTS.Data())
    frame_t = time.frame()
    frame_t.SetTitle('')
    frame_p = time.frame(RooFit.Title("pull_frame"))
 

    frame_t.GetXaxis().SetLabelFont( 132 )
    frame_t.GetYaxis().SetLabelFont( 132 )
    frame_t.GetXaxis().SetLabelOffset( 0.006 )
    frame_t.GetYaxis().SetLabelOffset( 0.006 )
    frame_t.GetXaxis().SetLabelColor( kWhite)
    
    frame_t.GetXaxis().SetTitleSize( 0.06 )
    frame_t.GetYaxis().SetTitleSize( 0.06 )
    frame_t.GetYaxis().SetNdivisions(512)
    
    if decay != "Bd2DPi":
        frame_t.GetXaxis().SetTitleOffset( 1.00 )
        frame_t.GetYaxis().SetTitleOffset( 1.00 )
        frame_t.GetXaxis().SetLabelSize( 0.06 )
        frame_t.GetYaxis().SetLabelSize( 0.06 )
    else: 
        frame_t.GetXaxis().SetTitleOffset( 1.25 )
        frame_t.GetYaxis().SetTitleOffset( 1.2 )
        frame_t.GetXaxis().SetLabelSize( 0.03 )
        frame_t.GetYaxis().SetLabelSize( 0.05 )

    unit = "ps"
    #frame_t.GetXaxis().SetTitle('#font[132]{#tau (B_{s} #rightarrow D_{s} #pi) [ps]}')
    frame_t.GetYaxis().SetTitle((TString.Format("#font[132]{Candidates / ( " +
                                                    "{0:0.2f}".format(time.getBinWidth(1))+" "+
                                                    unit+")}") ).Data())
    
    if not log and dataset.numEntries() > 10000 or decay == "Bd2DPi":
        frame_t.GetYaxis().SetNdivisions(508)
        frame_t.GetYaxis().SetTitleOffset( 1.35 )
        if decay == "Bd2DPi":
            frame_t.GetYaxis().SetTitleOffset( 1.2 )
        frame_t.GetYaxis().SetRangeUser(1.5,frame_t.GetMaximum()*1.1)

    if plotData:
        datacut = plotDataSet(dataset, frame_t, bin, options.sim, options.cat)
    

    print '##### modelPDF is'
    print modelPDF
    if plotModel :
        plotFitModel(modelPDF, frame_t, w, myconfigfile, log, decay, options.debug, namePDF, options.sim, category, cat, dataset, time)
    #exit(0) 
    doPulls = True #plotData and plotModel

    if log:
        gStyle.SetOptLogy(1)
        frame_t.GetYaxis().SetTitleOffset( 1.10 )
        frame_t.GetYaxis().SetRangeUser(1.5,frame_t.GetMaximum()*1.5)

    if decay == "Bd2DPi":
        legend = TLegend( 0.52, 0.65, 0.75, 0.93 )
    else:
        legend = TLegend( 0.60, 0.65, 0.80, 0.93 )

    legend.SetTextSize(0.06)
    legend.SetTextFont(12)
    legend.SetFillColor(0)
    legend.SetShadowColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(132)
    legend.SetHeader(plotLabel) # L_{int}=1.0 fb^{-1}")

    gr = TGraphErrors(1);
    gr.SetName("gr");
    gr.SetLineColor(kBlack);
    gr.SetLineWidth(2);
    gr.SetMarkerStyle(20);
    gr.SetMarkerSize(1.3);
    gr.SetMarkerColor(kBlack);
    #gr.Draw("P");
    legend.AddEntry(gr,"Data","lep");

    myconfigfilegrabber = __import__(configName,fromlist=['getconfig']).getconfig
    myconfigfile = myconfigfilegrabber()


    #decay = TString(myconfigfile["Decay"])
    descTS = TString(getDescription(myconfigfile["Decay"]))
    l1 = TLine()
    l1 = TLine()
    l1.SetLineColor(kBlue+3)
    l1.SetLineWidth(4)
    legend.AddEntry(l1, descTS.Data(), "L")
    l2 = TLine()
    l2.SetLineColor(kRed)
    l2.SetLineWidth(4)
    legend.AddEntry(l2, "Acceptance", "L")
    
    #pad1 = TPad("upperPad", "upperPad", .050, .22, 1.0, 1.0)
    pad1 = TPad("upperPad", "upperPad", .005, .05, 1.0, 1.0)
    pad1.SetBorderMode(0)
    pad1.SetBorderSize(-1)
    pad1.SetFillStyle(0)
    pad1.SetTickx(0);
    pad1.SetBottomMargin(0.30)
    pad1.SetLeftMargin(0.17)
    pad1.SetTopMargin(0.05)
    pad1.SetRightMargin(0.05)
    pad1.Draw()
    pad1.cd()
               
    frame_t.Draw()
    if leg:
        legend.Draw("same")
    
    pad1.Update()
    
    canvas.cd()
    #pad2 = TPad("lowerPad", "lowerPad", .050, .005, 1.0, .3275)
    canvas.cd()
    pad2 = TPad("lowerPad", "lowerPad", .005, .005, 1.0, .37)
    pad2.SetBorderMode(0)
    pad2.SetBorderSize(-1)
    pad2.SetFillStyle(0)
    pad2.SetBottomMargin(0.35)
    pad2.SetBottomMargin(0.40)
    pad2.SetLeftMargin(0.17)
    pad2.SetRightMargin(0.05)

    pad2.SetTickx(0);
    pad2.Draw()
    pad2.SetLogy(0)
    pad2.cd()
    
    gStyle.SetOptLogy(0)

    

    #frame_p = time.frame(RooFit.Title("pull_frame"))
    frame_p.Print("v")
    frame_p.SetTitle("")
    frame_p.GetYaxis().SetTitle("")
    frame_p.GetYaxis().SetTitleSize(0.08)
    frame_p.GetYaxis().SetTitleOffset(0.26)
    frame_p.GetYaxis().SetTitleFont(132)
    frame_p.GetYaxis().SetNdivisions(106)
    if decay != "Bd2DPi":
        frame_p.GetYaxis().SetLabelSize(0.18)
        frame_p.GetXaxis().SetLabelSize(0.18)
    else: 
        frame_p.GetYaxis().SetLabelSize(0.12)
        frame_p.GetXaxis().SetLabelSize(0.12)

    frame_p.GetYaxis().SetLabelOffset(0.006)
    frame_p.GetXaxis().SetTitleSize(0.20)
    frame_p.GetXaxis().SetTitleFont(132)
    frame_p.GetXaxis().SetTitleOffset(0.85)
    frame_p.GetXaxis().SetNdivisions(5)
    frame_p.GetYaxis().SetNdivisions(5)
    frame_p.GetXaxis().SetLabelFont( 132 )
    frame_p.GetYaxis().SetLabelFont( 132 )
    
    frame_p.GetYaxis().SetRangeUser(-3.0, 3.0)
    frame_p.GetXaxis().SetTitle('#font[132]{#tau('+descTS.Data()+') [ps]}')

    frame_p.Draw()
    
    if doPulls:
    
        pullHist = frame_t.pullHist()
        pullHist.SetName("pullHist")
        #pullHist.SetMaximum(3.0)
        #pullHist.SetMinimum(-3.0)
        pullHist.setYAxisLimits(-3.0, 3.0)

        frame_p.addPlotable(pullHist,"P")

        axisX = pullHist.GetXaxis()
        axisY = pullHist.GetYaxis()
        
        axisX.Set(100, timeDown, timeUp )
        axisX.SetTitle('#font[132]{#tau('+descTS.Data()+') [ps]}')   
        axisX.SetTitleSize(0.150)
        axisX.SetTitleFont(132)
        if decay == "Bd2DPi":
            axisX.SetLabelSize(0.130)
        else:
            axisX.SetLabelSize(0.150)
        axisX.SetLabelFont(132)
        maxX = axisX.GetXmax()
        minX = axisX.GetXmin()  
        
        max = axisY.GetXmax()
        min = axisY.GetXmin()
        axisY.SetLabelSize(0.150)
        axisY.SetLabelFont(132)
        axisY.SetNdivisions(5)        

        max = 3.0
        min = -3.0
        axisY.SetRangeUser(min, max)
        
        range = max-min
        zero = max/range
        print "max: %s, min: %s, range: %s, zero:%s"%(max,min,range,zero)
        print "maxX: %s, minX: %s"%(maxX,minX)
        print "time range: (%lf, %lf)"%(timeDown,timeUp) 
        
        graph = TGraph(2)
        graph.SetMaximum(max)
        graph.SetMinimum(min)
        graph.SetName("graph1")
        graph.SetTitle("")
        graph.SetPoint(1,timeDown,0)
        graph.SetPoint(2,timeUp,0)
        
        graph2 = TGraph(2)
        graph2.SetMaximum(max)
        graph2.SetMinimum(min)
        graph2.SetName("graph2")
        graph2.SetTitle("")
        graph2.SetPoint(1,timeDown,-3)
        graph2.SetPoint(2,timeUp,-3)
        graph2.SetLineColor(kRed)
        
        graph3 = TGraph(2)
        graph3.SetMaximum(max)
        graph3.SetMinimum(min)
        graph3.SetName("graph3")
        graph3.SetTitle("")
        graph3.SetPoint(1,timeDown,3)
        graph3.SetPoint(2,timeUp,3)
        graph3.SetLineColor(kRed)
        
        pullHist.SetTitle("");
        pullHist.Draw("AP")
        pullHist.GetYaxis().SetRangeUser(min, max)
        graph.Draw("SAME")
        graph2.Draw("SAME")
        graph3.Draw("SAME")

    pad2.Update()
    canvas.Update()


    if sim: 
        print "-----------------------------------------------"
        print "[WARNING] Simfit plotting is a work in progress"
        print "-----------------------------------------------"

    if doPulls:
        chi2 = frame_t.chiSquare() 
        chi22 = frame_t.chiSquare(1)
        
        print "chi2: %f"%(chi2)
        print "chi22: %f"%(chi22)
    
    sufixTS = TString(sfx)
    if sufixTS != "":
        sufixTS = TString("_")+sufixTS

    #outTS = TString(out)
    head = TString("time_")
    if log:
        head = head+TString("log_")

 
    exten = [".pdf",".png",".root",".C",".eps"] 
    for ext in exten:
        nameSave = head+decayTS+sufixTS+TString(ext) 
        canvas.Print(nameSave.Data())



#------------------------------------------------------------------------------
