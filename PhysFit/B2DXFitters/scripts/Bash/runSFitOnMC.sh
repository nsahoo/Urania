#!/bin/bash

#Prevent core dump
ulimit -c 0

#Bd2DPi
export nickname="MCSSbarAccAsymmFTFloatingDMGammaConstr2Taggers"
export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnMC.py"
export pyscriptpath="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/scripts/"
export runpath="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/"

#All, true tagging
export inputfile="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/Workspace/Nominal/work_dpi_mcfiltered_forTimeFits.root"
export outputdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sFit/${nickname}/AllData/"
export outputfile=${outputdir}"workResults.root"
export pol="both"
export mode="kpipi"
export year="run1"
export hypo="Bd2DPi"
export preselection="TagDecOS!=0||TagDecSS!=0"
rm -rf $outputdir
mkdir -p $outputdir
export Start=`date`
echo "==> Start fitting at ${Start}"
${runpath}run python ${pyscriptpath}runSFit_Bd.py --debug --outputdir $outputdir --fileName $inputfile --save $outputfile --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge both --pereventmistag --HFAG --MC --workMC "workspace" --noweight --NCPU 8 | tee ${outputdir}logfile.txt
export Stop=`date`
echo "==> Stop fitting at ${Stop}"