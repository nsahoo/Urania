#!/bin/bash

#Prevent core dump
ulimit -c 0

#Get options
export seed=$1
export stop=$2
export input=$3
export eosoutput=$4
export nickname=$5
export timefitdescr=$6
export config=$7
export pol=$8
export mode=$9
export year=${10}
export hypo=${11}
export workspace=${12}
export dataset=${13}
export pyscriptpath=${14}
export runpath=${15}

while (( $seed < $stop )); do 
    
    cd $pyscriptpath

    ${runpath}run python ${pyscriptpath}runSFit_Bd.py --NCPU 16 --pereventmistag --HFAG --sampleConstr --MC --inputdata $dataset --workMC $workspace --debug --fileName ${input}BootstrapMC_${nickname}_${seed}.root --save ${eosoutput}TimeFitBootstrapResult_${nickname}_${timefitdescr}_${seed}.root --fileNamePull ${eosoutput}PullTreeTimeFit_${nickname}_${timefitdescr}_${seed}.root --outputdir $eosoutput --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge both --noweight --seed $seed >& ${eosoutput}log_${nickname}_${timefitdescr}_${seed}.txt

    seed=$(($seed + 1))

done