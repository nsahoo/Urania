#!/bin/bash

#Prevent core dump
ulimit -c 0

#Common options
export rundir="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/"
export hypo="Bd2DPi"
export pyscriptpath="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/scripts/"
export mode="kpipi"

#------------All data---------------------------------
export pol="both"
export year="run1"
export nickname="SSbarAccAsymmFTFloatingDMGammaConstrAllSamplesBlinded_SSrlogit_pidk0"
#export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_RunIdata_OSCombined_SSCombined_B2DXNaming.root"
#export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_RunIdata_largeWindow_OSCombined_SSCombined.root"
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_RunIdata_pidk0_OSCombined_SSCombined.root"
#export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_AllData_OScombined_splinePreCalib_Nov2016.root"
export outputdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sFit/${nickname}/"
export outputfile=${outputdir}"workResults.root"
export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnData.py"
#export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnData_Nov2016.py"
export blindingString="TD_Dpi_3fb"
#export blindingString="TD_Dpi_3fb_Nov2016"
rm -rf $outputdir
mkdir -p $outputdir
export Start=`date`
echo "==> Start fitting at ${Start}"
${rundir}run python ${pyscriptpath}runSFit_Bd.py --debug --pereventmistag --fileName $inputfile --save $outputfile --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge both --NCPU 8 --HFAG --blindingString $blindingString --UniformBlinding --outputdir $outputdir | tee ${outputdir}logfile.txt
export Stop=`date`
echo "==> Stop fitting at ${Stop}"

#------------Pol/Year splits---------------------------------

exit

#2011
export pol="both"
export year="2011"
export nickname="SSbarAccAsymmFTFloatingDMGammaConstrBlinded_SSrlogit_2011"
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_2011data_OSCombined_SSCombined.root"
export outputdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sFit/${nickname}/"
export outputfile=${outputdir}"workResults.root"
export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnData.py"
export blindingString="TD_Dpi_3fb"
rm -rf $outputdir
mkdir -p $outputdir
export Start=`date`
echo "==> Start fitting at ${Start}"
${rundir}run python ${pyscriptpath}runSFit_Bd.py --debug --pereventmistag --fileName $inputfile --save $outputfile --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge pol --NCPU 8 --HFAG --blindingString $blindingString --UniformBlinding --outputdir $outputdir | tee ${outputdir}logfile.txt
export Stop=`date`
echo "==> Stop fitting at ${Stop}"

#2012
export pol="both"
export year="2012"
export nickname="SSbarAccAsymmFTFloatingDMGammaConstrBlinded_SSrlogit_2012"
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_2012data_OSCombined_SSCombined.root"
export outputdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sFit/${nickname}/"
export outputfile=${outputdir}"workResults.root"
export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnData.py"
export blindingString="TD_Dpi_3fb"
rm -rf $outputdir
mkdir -p $outputdir
export Start=`date`
echo "==> Start fitting at ${Start}"
${rundir}run python ${pyscriptpath}runSFit_Bd.py --debug --pereventmistag --fileName $inputfile --save $outputfile --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge pol --NCPU 8 --HFAG --blindingString $blindingString --UniformBlinding --outputdir $outputdir | tee ${outputdir}logfile.txt
export Stop=`date`
echo "==> Stop fitting at ${Stop}"

#MU
export pol="up"
export year="run1"
export nickname="SSbarAccAsymmFTFloatingDMGammaConstrBlinded_SSrlogit_MU"
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_MUdata_OSCombined_SSCombined.root"
export outputdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sFit/${nickname}/"
export outputfile=${outputdir}"workResults.root"
export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnData.py"
export blindingString="TD_Dpi_3fb"
rm -rf $outputdir
mkdir -p $outputdir
export Start=`date`
echo "==> Start fitting at ${Start}"
${rundir}run python ${pyscriptpath}runSFit_Bd.py --debug --pereventmistag --fileName $inputfile --save $outputfile --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge year --NCPU 8 --HFAG --blindingString $blindingString --UniformBlinding --outputdir $outputdir | tee ${outputdir}logfile.txt
export Stop=`date`
echo "==> Stop fitting at ${Stop}"

#MD
export pol="down"
export year="run1"
export nickname="SSbarAccAsymmFTFloatingDMGammaConstrBlinded_SSrlogit_MD"
export inputfile="/eos/lhcb/wg/b2oc/TD_DPi_3fb/sWeightedData_Bd/sWeights_MDdata_OSCombined_SSCombined.root"
export outputdir="/afs/cern.ch/work/v/vibattis/public/B2DX/Bd2DPi/sFit/${nickname}/"
export outputfile=${outputdir}"workResults.root"
export config="/afs/cern.ch/user/v/vibattis/cmtuser/UraniaDev_v6r2p1/PhysFit/B2DXFitters/data/Bd2DPi_3fbCPV/Bd2DPi/Bd2DPiConfigForSFitOnData.py"
export blindingString="TD_Dpi_3fb"
rm -rf $outputdir
mkdir -p $outputdir
export Start=`date`
echo "==> Start fitting at ${Start}"
${rundir}run python ${pyscriptpath}runSFit_Bd.py --debug --pereventmistag --fileName $inputfile --save $outputfile --configName $config --pol $pol --mode $mode --year $year --hypo $hypo --merge year --NCPU 8 --HFAG --blindingString $blindingString --UniformBlinding --outputdir $outputdir | tee ${outputdir}logfile.txt
export Stop=`date`
echo "==> Stop fitting at ${Stop}"