
def getconfig() :

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["YearOfDataTaking"] = {"2015","2016","2017","2018"}
    configdict["CharmModes"] = {"KstK","NonRes","PhiPi","KPiPi","PiPiPi"}

    # basic variables                                                                                        
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5300,    5800    ], "InputName" : "lab0_MassFitConsD_M"}
    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [1930,    2015    ], "InputName" : "lab2_MM"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     15.0    ], "InputName" : "lab0_LifetimeFitConsD_ctau"}
    configdict["BasicVariables"]["BacP"]          = { "Range" : [3000.0,  650000.0], "InputName" : "lab1_P"}
    configdict["BasicVariables"]["BacPT"]         = { "Range" : [400.0,   45000.0 ], "InputName" : "lab1_PT"}
    configdict["BasicVariables"]["BacPIDK"]       = { "Range" : [-7.0,    5.0     ], "InputName" : "lab1_PIDK"}
    configdict["BasicVariables"]["nTracks"]       = { "Range" : [15.0,   1000.0   ], "InputName" : "nTracks"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1     ], "InputName" : "lab0_LifetimeFitConsD_ctauErr"}
    configdict["BasicVariables"]["BacCharge"]     = { "Range" : [-1000.0, 1000.0  ], "InputName" : "lab1_ID"}
    configdict["BasicVariables"]["BDTG"]          = { "Range" : [0.475,   1.0     ], "InputName" : "BDTGResponse_XGB_1"}

    configdict["dataName"]   = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi_SignalNoVetoNoPID.txt"

    configdict["AdditionalVariables"] = {}

    #the values ARE NOT CORRECT 
    configdict["GlobalWeight"] = {"2015": {"NonRes" : {"Down":0.776869, "Up":0.564633},
                                           "PhiPi"  : {"Down":0.789176, "Up":0.572093},
                                           "KstK"   : {"Down":0.749362, "Up":0.548939},
                                           "KPiPi"  : {"Down":0.874073, "Up":0.162605},
                                           "PiPiPi" : {"Down":0.741961, "Up":0.548880}},
                                  "2016": {"NonRes" : {"Down":0.918942, "Up":0.879671},
                                           "PhiPi"  : {"Down":0.868050, "Up":0.835603},
                                           "KstK"   : {"Down":0.885762, "Up":0.859023},
                                           "KPiPi"  : {"Down":0.838399, "Up":0.413624},
                                           "PiPiPi" : {"Down":0.931354, "Up":1.000000}},
                                  "2017": {"NonRes" : {"Down":0.918942, "Up":0.879671},
                                           "PhiPi"  : {"Down":0.868050, "Up":0.835603},
                                           "KstK"   : {"Down":0.885762, "Up":0.859023},
                                           "KPiPi"  : {"Down":0.838399, "Up":0.413624},
                                           "PiPiPi" : {"Down":0.931354, "Up":1.000000}},
                                  "2018": {"NonRes" : {"Down":0.918942, "Up":0.879671},
                                           "PhiPi"  : {"Down":0.868050, "Up":0.835603},
                                           "KstK"   : {"Down":0.885762, "Up":0.859023},
                                           "KPiPi"  : {"Down":0.838399, "Up":0.413624},
                                           "PiPiPi" : {"Down":0.931354, "Up":1.000000}}}                                      


    #weighting templates by PID eff/misID                                                                                                                                                 
    configdict["WeightingMassTemplates"] = { 
                                             "RatioDataMC":      { "2015":{"FileLabel":"#DataMC 2015",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                   "2016":{"FileLabel":"#DataMC 2016",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                   "2017":{"FileLabel":"#DataMC 2017",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                   "2018":{"FileLabel":"#DataMC 2018",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                 },
                                             "Shift":            { "2011":{"BeautyMass": 0.0,   "CharmMass": 0.0},
                                                                   "2012":{"BeautyMass": 0.0,   "CharmMass": 0.0},
                                                                   "2015":{"BeautyMass": -1.0,  "CharmMass": 0.0},
                                                                   "2016":{"BeautyMass": -1.1,  "CharmMass": 0.0},
                                                                   "2017":{"BeautyMass": -0.95, "CharmMass": 0.0},
                                                                   "2018":{"BeautyMass": -1.65, "CharmMass": 0.0},
                                                                   }
                                             }


    return configdict
