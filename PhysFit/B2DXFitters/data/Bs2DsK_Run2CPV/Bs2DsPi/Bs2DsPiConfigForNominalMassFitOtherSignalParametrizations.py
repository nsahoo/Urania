def getconfig() :

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["SignalParametrization"] = {
                                          "Sample":"PIDVETO",
                                          #"Sample":"NOPIDNOVETO",
                                          "BeautyMass":"IpatiaJohnsonSU",     
                                          "CharmMass":"DoubleCrystalBallGaussianWithWidthRatio"}
                                          # "CharmMass":"DoubleCrystalBallWithWidthRatio"}


    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    
    # Bs signal shapes                     
    configdict["SignalShape"] = {} 
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"]    = {"Run2": {"All":5367.51}, "Fixed":False}

    if configdict["SignalParametrization"]["Sample"] == "PIDVETO":

        if configdict["SignalParametrization"]["BeautyMass"] == "IpatiaJohnsonSU":
            
            configdict["SignalShape"]["BeautyMass"]["type"]    = "IpatiaJohnsonSUWithWidthRatio"
            configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 19.2748},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 13.7854},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 1.30000},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 1.75031},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 4.01155},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 3.12317},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -4.0000},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.139381}, "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.361125},  "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"All" : 0.263997},  "Fixed" : True}


        else:
            configdict["SignalShape"]["BeautyMass"]["sigma1"]  = {"Run2": {"All" : 17.432},    "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["sigma2"]  = {"Run2": {"All" : 11.210},    "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["alpha1"]  = {"Run2": {"All" : -2.1016},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["alpha2"]  = {"Run2": {"All" : 2.3520},    "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 2.7904},    "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 0.61148},   "Fixed" : True}
            configdict["SignalShape"]["BeautyMass"]["frac"]    = {"Run2": {"All" : 0.50878},   "Fixed":True}
    
    else:
        configdict["SignalShape"]["BeautyMass"]["sigma1"]  = {"Run2": {"All" : 17.850},    "Fixed" : True}
        configdict["SignalShape"]["BeautyMass"]["sigma2"]  = {"Run2": {"All" : 11.279},    "Fixed" : True}
        configdict["SignalShape"]["BeautyMass"]["alpha1"]  = {"Run2": {"All" : -2.0550},   "Fixed" : True}
        configdict["SignalShape"]["BeautyMass"]["alpha2"]  = {"Run2": {"All" : 2.4151},    "Fixed" : True}
        configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 2.8440},    "Fixed" : True}
        configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 0.41598},   "Fixed" : True}
        configdict["SignalShape"]["BeautyMass"]["frac"]    = {"Run2": {"All" : 0.52647},   "Fixed":True}

    configdict["SignalShape"]["BeautyMass"]["R"]       = {"Run2": {"NonRes" : 1.00,   "PhiPi" : 1.0,      "Kstk" : 1.0,       "KPiPi" : 1.0,      "PiPiPi" : 1.0}, "Fixed":False}

    #Ds signal shapes                                                                                                                                       
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"]    = "DoubleCrystalBallGaussianWithWidthRatio"
    configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All":1968.49}, "Fixed":False} 

    if configdict["SignalParametrization"]["Sample"] == "PIDVETO":

        if configdict["SignalParametrization"]["CharmMass"] == "DoubleCrystalBallGaussianWithWidthRatio":
            configdict["SignalShape"]["CharmMass"]["sigma1"]  = {"Run2": {"NonRes" : 5.1056,   "PhiPi" : 4.8059,   "KstK" : 5.1199,    "KPiPi" : 6.1103,    "PiPiPi" : 8.3505},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["sigma2"]  = {"Run2": {"NonRes" : 5.1137,   "PhiPi" : 5.0748,   "KstK" : 5.1211,    "KPiPi" : 6.1864,    "PiPiPi" : 8.6486},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["alpha1"]  = {"Run2": {"NonRes" : -1.2395,  "PhiPi" : -1.1604,  "KstK" : -0.80649,  "KPiPi" : -0.98995,  "PiPiPi" : -0.95056}, "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["alpha2"]  = {"Run2": {"NonRes" : 1.2590,   "PhiPi" : 0.88887,  "KstK" : 1.1884,    "KPiPi" : 0.72425,   "PiPiPi" : 0.98889}, "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"NonRes" : 5.0425,   "PhiPi" : 5.2147,   "KstK" : 29.570,    "KPiPi" : 9.3488,    "PiPiPi" : 46.775},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"NonRes" : 2.9825,   "PhiPi" : 5.7816,   "KstK" : 5.8789,    "KPiPi" : 20.831,    "PiPiPi" : 49.985},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["sigmaG"]  = {"Run2": {"NonRes" : 7.5365,   "PhiPi" : 7.3484,   "KstK" : 6.9739,    "KPiPi" : 8.1432,    "PiPiPi" : 4.2005},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["frac1"]   = {"Run2": {"NonRes" : 0.41739,  "PhiPi" : 0.43356,  "KstK" : 0.27581,   "KPiPi" : 0.34634,   "PiPiPi" : 0.42889}, "Fixed":True}
            configdict["SignalShape"]["CharmMass"]["frac2"]   = {"Run2": {"NonRes" : 0.55033,  "PhiPi" : 0.43527,  "KstK" : 0.55003,   "KPiPi" : 0.40461,   "PiPiPi" : 0.94956}, "Fixed":True}
            configdict["SignalShape"]["CharmMass"]["R"]       = {"Run2": {"NonRes" : 1.00,     "PhiPi" : 1.00,     "KstK" : 1.00,      "KPiPi" : 1.00,      "PiPiPi" : 1.00}, "Fixed":False}
        elif configdict["SignalParametrization"]["CharmMass"] == "Ipatia":

            configdict["SignalShape"]["CharmMass"]["type"]    = "Ipatia"
            configdict["SignalShape"]["CharmMass"]["sigma"]   = {"Run2": {"NonRes" : 7.10812,   "PhiPi" : 7.4167,    "KstK" : 6.92678,    "KPiPi" : 8.8034,    "PiPiPi" : 11.606},    "Fixed" : False}
            configdict["SignalShape"]["CharmMass"]["a1"]      = {"Run2": {"NonRes" : 2.57097,   "PhiPi" : 2.52351,   "KstK" : 2.36694,    "KPiPi" : 2.19192,   "PiPiPi" : 6.51079},   "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["a2"]      = {"Run2": {"NonRes" : 2.48165,   "PhiPi" : 3.00548,   "KstK" : 1.96227,    "KPiPi" : 3.70117,   "PiPiPi" : 2.18278},   "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"NonRes" : 1.71756,   "PhiPi" : 2.29022,   "KstK" : 3.49652,    "KPiPi" : 2.92311,   "PiPiPi" : 10.0},      "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"NonRes" : 3.30446,   "PhiPi" : 2.37738,   "KstK" : 5.75142,    "KPiPi" : 0.494884,  "PiPiPi" : 10.0},      "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["l"]       = {"Run2": {"NonRes" : -2.62134,  "PhiPi" : -2.33104,  "KstK" : -3.29529,   "KPiPi" : -2.81874,  "PiPiPi" : -2.12864},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["fb"]      = {"Run2": {"All" : 0.00}, "Fixed":True}
            configdict["SignalShape"]["CharmMass"]["zeta"]    = {"Run2": {"All" : 0.00}, "Fixed":True}
        else:
            configdict["SignalShape"]["CharmMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
            configdict["SignalShape"]["CharmMass"]["sigma1"]  = {"Run2": {"NonRes" : 5.4666,   "PhiPi" : 5.2882,   "KstK" : 5.7843,    "KPiPi" : 6.8026,    "PiPiPi" : 8.0508},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["sigma2"]  = {"Run2": {"NonRes" : 5.7647,   "PhiPi" : 6.0419,   "KstK" : 5.6803,    "KPiPi" : 7.4045,    "PiPiPi" : 8.4088},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["alpha1"]  = {"Run2": {"NonRes" : -1.1877,  "PhiPi" : -1.1485,  "KstK" : -1.1245,   "KPiPi" : -1.2520,   "PiPiPi" : -0.92527}, "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["alpha2"]  = {"Run2": {"NonRes" : 1.1331,   "PhiPi" : 1.0455,   "KstK" : 1.1562,    "KPiPi" : 0.99111,   "PiPiPi" : 0.95463}, "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"NonRes" : 8.9789,   "PhiPi" : 8.3638,   "KstK" : 18.621,    "KPiPi" : 9.4377,    "PiPiPi" : 50.0},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"NonRes" : 7.0061,   "PhiPi" : 14.819,   "KstK" : 12.264,    "KPiPi" : 41.111,    "PiPiPi" : 50.0},  "Fixed" : True}
            configdict["SignalShape"]["CharmMass"]["frac"]    = {"Run2": {"NonRes" : 0.55959,  "PhiPi" : 0.55803,  "KstK" : 0.51514,   "KPiPi" : 0.56261,   "PiPiPi" : 0.45339}, "Fixed":True}
            configdict["SignalShape"]["CharmMass"]["R"]       = {"Run2": {"NonRes" : 1.00,     "PhiPi" : 1.00,     "KstK" : 1.00,      "KPiPi" : 1.00,      "PiPiPi" : 1.00}, "Fixed":False}
                
    else:

        configdict["SignalShape"]["CharmMass"]["sigma1"]  = {"Run2": {"NonRes" : 5.3225,   "PhiPi" : 5.0067,   "KstK" : 5.4815,    "KPiPi" : 6.4020,    "PiPiPi" : 8.3804},   "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["sigma2"]  = {"Run2": {"NonRes" : 5.1474,   "PhiPi" : 4.9690,   "KstK" : 5.5113,    "KPiPi" : 6.4982,    "PiPiPi" : 8.6096},   "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["alpha1"]  = {"Run2": {"NonRes" : -0.81071, "PhiPi" : -1.0785,  "KstK" : -1.1360,   "KPiPi" : -0.93755,  "PiPiPi" : -0.95284}, "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["alpha2"]  = {"Run2": {"NonRes" : 1.3049,   "PhiPi" : 0.94775,  "KstK" : 1.1901,    "KPiPi" : 0.85966,   "PiPiPi" : 0.97957},  "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"NonRes" : 13.990,   "PhiPi" : 5.9451,   "KstK" : 6.3220,    "KPiPi" : 9.0148,    "PiPiPi" : 43.372},   "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"NonRes" : 3.1341,   "PhiPi" : 5.0670,   "KstK" : 4.7445,    "KPiPi" : 8.0321,    "PiPiPi" : 49.996},   "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["sigmaG"]  = {"Run2": {"NonRes" : 7.7900,   "PhiPi" : 7.6435,   "KstK" : 7.9940,    "KPiPi" : 9.0954,    "PiPiPi" : 4.1422},   "Fixed" : True}
        configdict["SignalShape"]["CharmMass"]["frac1"]   = {"Run2": {"NonRes" : 0.30186,  "PhiPi" : 0.43461,  "KstK" : 0.40787,   "KPiPi" : 0.37338,   "PiPiPi" : 0.43606},  "Fixed":True}
        configdict["SignalShape"]["CharmMass"]["frac2"]   = {"Run2": {"NonRes" : 0.63200,  "PhiPi" : 0.51389,  "KstK" : 0.59695,   "KPiPi" : 0.50757,   "PiPiPi" : 0.95814},  "Fixed":True}
        configdict["SignalShape"]["CharmMass"]["R"]       = {"Run2": {"NonRes" : 1.00,     "PhiPi" : 1.00,     "KstK" : 1.00,      "KPiPi" : 1.00,      "PiPiPi" : 1.00},     "Fixed":False}


    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalParametrization"]["BeautyMass"] == "IpatiaJohnsonSU":
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"]    = "ShiftedSignalIpatiaJohnsonSU"
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"]    = "ShiftedSignal"
    configdict["Bd2DsPiShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": -86.8}, "Fixed":True}
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"]  = {"Run2": {"All": 1.00808721452}, "Fixed":True}
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"]  = {"Run2": {"All": 1.03868673310}, "Fixed":True}


    return configdict
