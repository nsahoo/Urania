def getconfig() :

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    from math import pi

    # PHYSICAL PARAMETERS
    configdict["Gammas"]        =  0.6643 #0.0020   # in ps^{-1}
    configdict["DeltaGammas"]   =  -0.083 #-0.006
    configdict["DeltaMs"]       = 17.757   # in ps^{-1}
    configdict["TagEffSig"]     = 0.403
    configdict["TagOmegaSig"]   = 0.391
    configdict["StrongPhase"]   = 20. / 180. * pi
    configdict["WeakPhase"]     = 70./180.*pi
    configdict["ModLf"]         = 0.372
    configdict["CPlimit"]       = {"upper":4.0, "lower":-4.0}
    configdict["Labels"] = ["20152016","2017", "2018"]

    configdict["Asymmetries"] = {"Detection":1.0/100.0,
                                 "Production":1.1/100.0}
    #    configdict["Asymmetries"] = {"Detection":0.0,
    #                                 "Production":0.0}


    configdict["FixAcceptance"] = False
    configdict["ConstrainsForTaggingCalib"] = False

    configdict["Resolution"] = { "20152016": {"scaleFactor":{"p0":0.012963396890204528, "p1":1.0078118390740174, "p2":0.0},
                                              "meanBias":0.0},
                                 "2017": {"scaleFactor":{"p0":0.010580008378118407, "p1":1.0159541446819076, "p2":0.0},
                                          "meanBias":0.0},
                                 "2018": {"scaleFactor":{"p0":0.010580008378118407, "p1":1.0159541446819076, "p2":0.0},
                                          "meanBias":0.0}
                                }

    configdict["TaggingCalibration"] = {}
    configdict["TaggingCalibration"]["SS"] = {
        "20152016": {
            "p0": 0.011 + 0.469436,
            "dp0": 0.0,
            "p1": -0.21 + 1,
            "dp1": 0.0,
            "cov": [
                [2.903e-05, 0.0, 0.0001613, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0001613, 0.0, 0.006101, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ],
            "average": 0.469436,
            "tagEff":0.689641,
            "aTagEff":0.0,
            "use":True,
        },
        "2017": {
            "p0": 0.015 + 0.468859,
            "dp0": 0.0,
            "p1": -0.23 + 1,
            "dp1": 0.0,
            "cov": [
                [2.903e-05, 0.0, 0.0001613, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0001613, 0.0, 0.006101, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ],
            "average": 0.468859,
            "tagEff":0.697280,
            "aTagEff":0.0,
            "use":True,
        },
        "2018": {
            "p0": 0.015 + 46.6790 / 100,
            "dp0": 0.0,
            "p1": -0.23 + 1,
            "dp1": 0.0,
            "cov": [
                [2.903e-05, 0.0, 0.0001613, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0001613, 0.0, 0.006101, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ],
            "average": 46.6790 / 100,
            "tagEff":0.697280,
            "aTagEff":0.0,
            "use":True,
        },
    }

    configdict["TaggingCalibration"]["OS"] = {
        "20152016": {
            "p0": 0.032 + 0.443185,
            "dp0": 0.0,
            "p1": -0.03 + 1,
            "dp1": 0.0,
            "cov": [
                [5.212e-05, 0.0, 0.0002286, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0002286, 0.0, 0.006685, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ],
            "average": 0.443185,
            "tagEff": 0.381689,
            "aTagEff": 0.0,
            "use": True,
        },
        "2017": {
            "p0": 0.023 + 0.439283,
            "dp0": 0.0,
            "p1": -0.11 + 1,
            "dp1": 0.0,
            "cov": [
                [5.212e-05, 0.0, 0.0002286, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0002286, 0.0, 0.006685, 0.0],
                [0.0, 0.0, 0.0, 1.0],
            ],
            "average": 43.7625 / 100,
            "tagEff": 0.403752,
            "aTagEff": 0.0,
            "use": True,
        },
        "2018": {
            "p0": 0.023 + 0.439283,
            "dp0": 0.0,
            "p1": -0.11 + 1,
            "dp1": 0.0,
            "cov": [
                [5.212e-05, 0.0, 0.0002286, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0002286, 0.0, 0.006685, 0.0],
                [0.0, 0.0, 0.0, 1.0],
            ],
            "average": 0.439283,
            "tagEff": 0.403752,
            "aTagEff": 0.0,
            "use": True,
        },
    }


    configdict["Acceptance"] = { "knots": [0.50, 1.0,  1.5, 2.0, 3.0, 12.0],
                                 "20152016": { "values": [3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00, 1.1872e+00] },
                                 "2017":     { "values": [3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00, 1.1872e+00] },
                                 "2018":     { "values": [3.774e-01, 5.793e-01, 7.752e-01, 1.0043e+00, 1.0937e+00, 1.1872e+00] },
                            }

    #configdict["Acceptance"] = {"knots": [0.50, 1.0,  1.5, 2.0, 3.0, 12.0],
    #                            "values": [3.774e-01,5.793e-01,7.752e-01,1.0043e+00,1.0937e+00,1.1872e+00] }


    configdict["constParams"] = []
    configdict["constParams"].append('Gammas_Bs2DsPi')
    configdict["constParams"].append('deltaGammas_Bs2DsPi')
    configdict["constParams"].append('C_Bs2DsPi')
    configdict["constParams"].append('Cbar_Bs2DsPi')
    configdict["constParams"].append('S_Bs2DsPi')
    configdict["constParams"].append('Sbar_Bs2DsPi')
    configdict["constParams"].append('D_Bs2DsPi')
    configdict["constParams"].append('Dbar_Bs2DsPi')
    configdict["constParams"].append('tagEff_OS')
    configdict["constParams"].append('tagEff_SS')
    configdict["constParams"].append('aTagEff_OS')
    configdict["constParams"].append('aTagEff_SS')

    if configdict["FixAcceptance"] == True:
        configdict["constParams"].append('var1')
        configdict["constParams"].append('var2')
        configdict["constParams"].append('var3')
        configdict["constParams"].append('var4')
        configdict["constParams"].append('var5')
        configdict["constParams"].append('var6')
        configdict["constParams"].append('var7')
    #if configdict["ConstrainsForTaggingCalib"] == False:
    #    configdict["constParams"].append('p0_OS')
    #    configdict["constParams"].append('p0_SS')
    #    configdict["constParams"].append('p1_OS')
    #    configdict["constParams"].append('p1_SS')
    configdict["constParams"].append('dp0_OS')
    configdict["constParams"].append('dp0_SS')
    configdict["constParams"].append('dp1_OS')
    configdict["constParams"].append('dp1_SS')
    configdict["constParams"].append('p0_mean_OS')
    configdict["constParams"].append('p0_mean_SS')
    configdict["constParams"].append('p1_mean_OS')
    configdict["constParams"].append('p1_mean_SS')
    configdict["constParams"].append('dp0_mean_OS')
    configdict["constParams"].append('dp0_mean_SS')
    configdict["constParams"].append('dp1_mean_OS')
    configdict["constParams"].append('dp1_mean_SS')
    configdict["constParams"].append('average_OS')
    configdict["constParams"].append('average_SS')

    constPar = configdict["constParams"]

    configdict["addConstParams"] = []
    for const in constPar:
        for lab in configdict["Labels"]:
            nameVar = const + "_"+lab
            configdict["addConstParams"].append(nameVar)
    configdict["constParams"] = configdict["constParams"] + configdict["addConstParams"]

    return configdict
