def getconfig() :

    configdict = {}
    
    from math import pi
    from math import log

    # considered decay mode                                                       
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = {"KstK","NonRes","PhiPi","KPiPi","PiPiPi"}
    configdict["Backgrounds"] = ["Bd2DPi","Lb2LcPi","Bs2DsRho","Bs2DsstPi","Bd2DsPi","Bs2DsK"]
    # year of data taking                                                                                                                          
    configdict["YearOfDataTaking"] = {"2015", "2016","2017","2018"}
    # stripping (necessary in case of PIDK shapes)                                                                                              
    configdict["Stripping"] = {"2012":"21", "2011":"21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)                                                                 
    configdict["IntegratedLuminosity"] = {#"2011": {"Down": 0.56,   "Up": 0.42}, 
                                          #"2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
                                          }
    # file name with paths to MC/data samples                                                                                                       
    configdict["dataName"]   = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi.txt"
    #settings for control plots                                                                                                                                  
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = { "Directory": "PlotBs2DsPi_Nominal", "Extension":"pdf"}

    configdict["MoreVariables"] = True

    # basic variables                                                                                        
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5300,    5800    ], "InputName" : "lab0_MassFitConsD_M"}
    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [1930,    2015    ], "InputName" : "lab2_MM"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     15.0    ], "InputName" : "lab0_LifetimeFitConsD_ctau"}
    configdict["BasicVariables"]["BacP"]          = { "Range" : [3000.0,  650000.0], "InputName" : "lab1_P"}
    configdict["BasicVariables"]["BacPT"]         = { "Range" : [400.0,   45000.0 ], "InputName" : "lab1_PT"}
    configdict["BasicVariables"]["BacPIDK"]       = { "Range" : [-7.0,    5.0     ], "InputName" : "lab1_PIDK"}
    configdict["BasicVariables"]["nTracks"]       = { "Range" : [15.0,   1000.0   ], "InputName" : "nTracks"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1     ], "InputName" : "lab0_LifetimeFitConsD_ctauErr"}
    configdict["BasicVariables"]["BacCharge"]     = { "Range" : [-1000.0, 1000.0  ], "InputName" : "lab1_ID"}
    configdict["BasicVariables"]["BDTG"]          = { "Range" : [0.475,   1.0     ], "InputName" : "BDTGResponse_XGB_1"}
    configdict["BasicVariables"]["TagDecOS"]      = { "Range" : [-1.0,    1.0     ], "InputName" : "OS_Combination_DEC"}
    configdict["BasicVariables"]["TagDecSS"]      = { "Range" : [-1.0,    1.0     ], "InputName" : "lab0_SSKaonLatest_TAGDEC"}
    configdict["BasicVariables"]["MistagOS"]      = { "Range" : [-3.0,    1.0     ], "InputName" : "OS_Combination_ETA"}
    configdict["BasicVariables"]["MistagSS"]      = { "Range" : [-3.0,    1.0     ], "InputName" : "lab0_SSKaonLatest_TAGETA"}

    # tagging calibration                                                                                               
    #configdict["TaggingCalibration"] = {}
    #configdict["TaggingCalibration"]["OS"] = {"p0": 0.375,  "p1": 0.982, "average": 0.3688}
    #configdict["TaggingCalibration"]["SS"] = {"p0": 0.4429, "p1": 0.977, "average": 0.4377}

    HLTcut = "&&(lab0_Hlt1TrackAllL0Decision_TOS && (lab0_Hlt2IncPhiDecision_TOS ==1 || lab0_Hlt2Topo2BodyBBDTDecision_TOS == 1 || lab0_Hlt2Topo3BodyBBDTDecision_TOS == 1))"
    # additional cuts applied to data sets                                                                                    
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"]    = { "Data": "lab1_isMuon==0",
                                               "MC"  : "lab1_M<200&&lab1_PIDK !=-1000.0&&lab2_FD_ORIVX > 0.&&(lab0_LifetimeFit_Dplus_ctau[0]>0)",
                                               "MCID":True, "MCTRUEID":True, "BKGCAT":True, "DsHypo":True}

    configdict["AdditionalCuts"]["KKPi"]   = { "Data": "lab2_FDCHI2_ORIVX > 2", "MC" : "lab2_FDCHI2_ORIVX > 2"}
    configdict["AdditionalCuts"]["KPiPi"]  = { "Data": "lab2_FDCHI2_ORIVX > 9", "MC" : "lab2_FDCHI2_ORIVX > 9"}
    configdict["AdditionalCuts"]["PiPiPi"] = { "Data": "lab2_FDCHI2_ORIVX > 9", "MC" : "lab2_FDCHI2_ORIVX > 9"}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts                                                                                                              
    # order of particles: KKPi, KPiPi, PiPiPi                                                                                                         
    configdict["DsChildrenPrefix"] = {"Child1":"lab3","Child2":"lab4","Child3": "lab5"}

    # additional variables in data sets
    if configdict["MoreVariables"] == True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"]             =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSCharm_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"]    =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSElectronLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"]        =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSKaonLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"]        =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSMuonLatest_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"]             =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_OSVtxCh_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"]              =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_SSPion_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"]            =  { "Range" : [ -2.0, 2.0 ], "InputName" : "lab0_SSProton_TAGDEC"}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"]             =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSCharm_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"]    =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSElectronLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"]        =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSKaonLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"]        =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSMuonLatest_TAGETA"}
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"]             =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_OSVtxCh_TAGETA"}
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"]              =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_SSPion_TAGETA"}
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"]            =  { "Range" : [ -3.0, 1.0 ], "InputName" : "lab0_SSProton_TAGETA"}
        configdict["AdditionalVariables"]["lab0_P"]                          =  { "Range" : [ 0.0, 1600000.0 ],     "InputName" : "lab0_P"}
        configdict["AdditionalVariables"]["lab0_PT"]                         =  { "Range" : [ 0.0, 40000.0 ],       "InputName" : "lab0_PT"}
        configdict["AdditionalVariables"]["lab0_ETA"]                        =  { "Range" : [ 0.0, 6.0 ],           "InputName" : "lab0_ETA"}
        configdict["AdditionalVariables"]["runNumber"]                       =  { "Range" : [ 0.0, 1000000.0 ],     "InputName" : "runNumber"}
        configdict["AdditionalVariables"]["eventNumber"]                     =  { "Range" : [ 0.0, 10000000000.0 ], "InputName" : "eventNumber"}
        configdict["AdditionalVariables"]["BDTGResponse_3"]                  =  { "Range" : [ -1.0, 1.0 ],          "InputName" : "BDTGResponse_3"}
        configdict["AdditionalVariables"]["nSPDHits"]                        =  { "Range" : [ 0.0, 1400.0 ],        "InputName" : "nSPDHits"}
        configdict["AdditionalVariables"]["nTracksLinear"]                   =  { "Range" : [15.0,    1000.0  ],    "InputName" : "clone_nTracks"}


    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    
    # Bs signal shapes                     
    configdict["SignalShape"] = {} 
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"]    = "IpatiaJohnsonSUWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"]    = {"Run2": {"All":5367.51}, "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {"Run2": {"All" : 19.2748},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {"Run2": {"All" : 13.7854},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["a1"]      = {"Run2": {"All" : 1.30000},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["a2"]      = {"Run2": {"All" : 1.75031},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All" : 4.01155},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All" : 3.12317},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["fb"]      = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["zeta"]    = {"Run2": {"All" : 0.00000},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["l"]       = {"Run2": {"All" : -4.0000},   "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["nu"]      = {"Run2": {"All" : -0.139381}, "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["tau"]     = {"Run2": {"All" : 0.361125},  "Fixed" : True}
    configdict["SignalShape"]["BeautyMass"]["fracI"]   = {"Run2": {"All" : 0.263997},  "Fixed" : True}

    configdict["SignalShape"]["BeautyMass"]["R"]       = {"Run2": {"NonRes" : 1.00,   "PhiPi" : 1.0,      "Kstk" : 1.0,       "KPiPi" : 1.0,      "PiPiPi" : 1.0}, "Fixed":False}


    #Ds signal shapes                                                                                                                                       
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"]    = "Ipatia"
    configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All":1968.49}, "Fixed":False}
    configdict["SignalShape"]["CharmMass"]["sigma"]   = {"Run2": {"NonRes" : 7.10812,   "PhiPi" : 7.4167,    "KstK" : 6.92678,    "KPiPi" : 8.8034,    "PiPiPi" : 11.606},    "Fixed" : False}
    configdict["SignalShape"]["CharmMass"]["a1"]      = {"Run2": {"NonRes" : 2.57097,   "PhiPi" : 2.52351,   "KstK" : 2.36694,    "KPiPi" : 2.19192,   "PiPiPi" : 6.51079},   "Fixed" : True}
    configdict["SignalShape"]["CharmMass"]["a2"]      = {"Run2": {"NonRes" : 2.48165,   "PhiPi" : 3.00548,   "KstK" : 1.96227,    "KPiPi" : 3.70117,   "PiPiPi" : 2.18278},   "Fixed" : True}
    configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"NonRes" : 1.71756,   "PhiPi" : 2.29022,   "KstK" : 3.49652,    "KPiPi" : 2.92311,   "PiPiPi" : 10.0},      "Fixed" : True}
    configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"NonRes" : 3.30446,   "PhiPi" : 2.37738,   "KstK" : 5.75142,    "KPiPi" : 0.494884,  "PiPiPi" : 10.0},      "Fixed" : True}
    configdict["SignalShape"]["CharmMass"]["l"]       = {"Run2": {"NonRes" : -2.62134,  "PhiPi" : -2.33104,  "KstK" : -3.29529,   "KPiPi" : -2.81874,  "PiPiPi" : -2.12864},  "Fixed" : True}
    configdict["SignalShape"]["CharmMass"]["fb"]      = {"Run2": {"All" : 0.00}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["zeta"]    = {"Run2": {"All" : 0.00}, "Fixed":True}
    
    # combinatorial background                                                                                                                              
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"]  = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"]   = {"Run2": {"NonRes":-3.5211e-02,  "PhiPi":-3.0873e-02,  "KstK":-2.3392e-02, "KPiPi":-1.0361e-02, "PiPiPi":-1.5277e-02},"Fixed": False}
    #configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = {"Run2": {"NonRes":-3.5211e-02,  "PhiPi":-3.0873e-02,  "KstK":-2.3392e-02, "KPiPi":-1.0361e-02, "PiPiPi":-1.5277e-02},"Fixed": False}
    configdict["CombBkgShape"]["BeautyMass"]["cB2"]   = {"Run2": {"NonRes":0.0,       "PhiPi":0.0,       "KstK":0.0,      "KPiPi":0.0,         "PiPiPi":0.0}, 
                                                         "Fixed":True }
    configdict["CombBkgShape"]["BeautyMass"]["frac"]  = {"Run2": {"NonRes":4.3067e-01,   "PhiPi":6.5400e-01,   "KstK":3.7409e-01,  "KPiPi":0.5,         "PiPiPi":0.5}, "Fixed":False}
    

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"]  = "ExponentialPlusSignal" #"ExponentialPlusDoubleCrystalBallWithWidthRatioSharedMean"
    configdict["CombBkgShape"]["CharmMass"]["cD"]      = {"Run2": {"NonRes":-5.7520e-03,  "PhiPi":-5.7273e-03, "KstK":-8.3967e-03, "KPiPi":-4.9193e-03, "PiPiPi":-4.5455e-02},"Fixed":False}
    configdict["CombBkgShape"]["CharmMass"]["fracD"]   = {"Run2": {"NonRes":0.88620,      "PhiPi":0.37379,     "KstK":0.59093,     "KPiPi":0.5,         "PiPiPi":0.5},"Fixed":False}


    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "FixedWithKaonPion"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = { "Kaon":True, "Pion":True, "Proton":False }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"]   = { "Run2":{"NonRes":0.9, "PhiPi":0.9, "KstK":0.9, "KPiPi":0.8, "PiPiPi":0.8 }, "Fixed":False }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK2"]   = { "Run2":{"NonRes":0.9, "PhiPi":0.9, "KstK":0.9, "KPiPi":0.8, "PiPiPi":0.8 }, "Fixed":False }


    #Bd2Dsh background
    #shape for BeautyMass, for CharmMass as well as BacPIDK taken by default the same as signal 
    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"]["type"]   == "IpatiaJohnsonSUWithWidthRatio":
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"]    = "ShiftedSignalIpatiaJohnsonSU"
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"]    = "ShiftedSignal"
    configdict["Bd2DsPiShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": -86.8}, "Fixed":True}
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"]  = {"Run2": {"All": 1.00808721452}, "Fixed":True}
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"]  = {"Run2": {"All": 1.03868673310}, "Fixed":True}


    #
    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {"Run2":{"All":{"Both":{"CentralValue":0.5, "Range":[0.0,1.0]}}}, "Fixed":False}
    configdict["AdditionalParameters"]["g1_f2_frac"] = {"Run2":{"All":{"Both":{"CentralValue":0.5, "Range":[0.0,1.0]}}}, "Fixed":False}
 
    #expected yields                                                                                                                                                       
    configdict["Yields"] = {}
    #configdict["Yields"]["Bd2DPi"]          = {"2011": { "NonRes":502.0,   "PhiPi":25.5,    "KstK":366.0,   "KPiPi":0.0,     "PiPiPi":0.0},
    #                                           "2012": { "NonRes":1116.0,  "PhiPi":61.0,    "KstK":903.0,   "KPiPi":0.0,     "PiPiPi":0.0},  "Fixed":True}
    configdict["Yields"]["Bd2DPi"]          = {
                                               "2015": { "NonRes":196.60,    "PhiPi":3.82,    "KstK":57.47,   "KPiPi":0.74,      "PiPiPi":0.0},
                                               "2016": { "NonRes":1077.43,   "PhiPi":25.74,   "KstK":314.35,  "KPiPi":4.21,      "PiPiPi":0.0},
                                               "2017": { "NonRes":982.239,   "PhiPi":24.99,   "KstK":285.59,  "KPiPi":3.66,      "PiPiPi":0.0},
                                               "2018": { "NonRes":1275.19,   "PhiPi":31.58,   "KstK":368.34,  "KPiPi":4.25,      "PiPiPi":0.0},
                                               "Fixed":True,
                                               }
    #configdict["Yields"]["Lb2LcPi"]         = {"2011": { "NonRes":215.6,   "PhiPi":39.0,    "KstK":63.0,    "KPiPi":1.4,     "PiPiPi":0.0},
    #                                           "2012": { "NonRes":560.0,   "PhiPi":92.0,    "KstK":164.0,   "KPiPi":4.6,     "PiPiPi":0.0},  "Fixed":True}
    configdict["Yields"]["Lb2LcPi"]         = {
                                               "2015": { "NonRes":66.22,   "PhiPi":6.68,    "KstK":14.25,   "KPiPi":0.36,     "PiPiPi":0.0},
                                               "2016": { "NonRes":318.5,   "PhiPi":26.41,   "KstK":70.6,    "KPiPi":4.45,     "PiPiPi":0.0},
                                               "2017": { "NonRes":384.9,   "PhiPi":36.84,   "KstK":82.8,    "KPiPi":4.65,     "PiPiPi":0.0},
                                               "2018": { "NonRes":771.5,   "PhiPi":72.7,    "KstK":141.5,   "KPiPi":4.40,     "PiPiPi": 0.0},
                                               "Fixed":True}
    
    configdict["Yields"]["Bs2DsK"]          = {"2015": { "NonRes":23.20,     "PhiPi":41.27,     "KstK":37.53,     "KPiPi":9.11,       "PiPiPi":23.20},
                                               "2016": { "NonRes":139.20,    "PhiPi":300.85,    "KstK":220.11,    "KPiPi":53.41,      "PiPiPi":141.55},
                                               "2017": { "NonRes":133.56,    "PhiPi":287.56,    "KstK":210.45,    "KPiPi":50.98,      "PiPiPi":133.81},
                                               "2018": { "NonRes":156.5,     "PhiPi":340.104,   "KstK":250.2,     "KPiPi":60.63,      "PiPiPi":156.00},
                                               "Fixed":True}

    #configdict["Yields"]["Bs2DsK"]          = {"2011": { "NonRes":46.2,    "PhiPi":69.8,    "KstK":67.6,    "KPiPi":33.3,    "PiPiPi":61.6},
    #                                           "2012": { "NonRes":118.7,   "PhiPi":171.1,   "KstK":167.0,   "KPiPi":87.8,    "PiPiPi":156.0}, "Fixed":True}
    configdict["Yields"]["Bs2DsDsstPiRho"]  = {"2015": { "NonRes":1000.0,   "PhiPi":1000.0,   "KstK":1000.0,   "KPiPi":200.0,   "PiPiPi":200.0},
                                               "2016": { "NonRes":2000.0,   "PhiPi":2000.0,   "KstK":2000.0,   "KPiPi":500.0,   "PiPiPi":500.0}, 
                                               "2017": { "NonRes":2000.0,   "PhiPi":2000.0,   "KstK":2000.0,   "KPiPi":500.0,   "PiPiPi":500.0},
                                               "2018": { "NonRes":2000.0,   "PhiPi":2000.0,   "KstK":2000.0,   "KPiPi":500.0,   "PiPiPi":500.0},
                                               "Fixed":False}
    configdict["Yields"]["CombBkg"]         = {"2015": { "NonRes":10000.0, "PhiPi":20000.0, "KstK":10000.0, "KPiPi":10000.0, "PiPiPi":10000.0},
                                               "2016": { "NonRes":20000.0, "PhiPi":30000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                               "2017": { "NonRes":20000.0, "PhiPi":30000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                               "2018": { "NonRes":20000.0, "PhiPi":30000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                               "Fixed":False}
    configdict["Yields"]["Signal"]          = {"2015": { "NonRes":10000.0, "PhiPi":20000.0, "KstK":10000.0, "KPiPi":10000.0, "PiPiPi":10000.0},
                                               "2016": { "NonRes":20000.0, "PhiPi":40000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                               "2017": { "NonRes":20000.0, "PhiPi":40000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                               "2018": { "NonRes":20000.0, "PhiPi":40000.0, "KstK":20000.0, "KPiPi":20000.0, "PiPiPi":20000.0},
                                               "Fixed":False}


    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#    
    ###                                                               MDfit plotting settings                                                             
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#               
    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bd2DPi", "Lb2LcPi", "Bs2DsDsstPiRho", "Bs2DsK"] 
    configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kRed, kBlue-10, kGreen+3]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.35,0.9], "ScaleYSize":2.5}
    configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.8,0.66],
                                                  "ScaleYSize":1.7, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"]    = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.20,0.9], "ScaleYSize":1.2}

    return configdict
