def getconfig() :

    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["dataName"]   = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi_ForTagging.txt"
    del configdict["BasicVariables"]["TagDecOS"]
    del configdict["BasicVariables"]["TagDecSS"]
    del configdict["BasicVariables"]["MistagOS"]
    del configdict["BasicVariables"]["MistagSS"]
    configdict["AdditionalVariables"]["lab0_SSKaonLatest_TAGDEC"] = {"Range": [ -2.0, 2.0 ], "InputName": "lab0_SSKaonLatest_TAGDEC"}
    configdict["AdditionalVariables"]["lab0_SSKaonLatest_TAGETA"] = {"Range": [ -3.0, 1.0 ], "InputName": "lab0_SSKaonLatest_TAGETA"}

    return configdict
