#Bs2DsK NonRes 2015
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2015/
B2DX_2015_Down_Bs2DsK_NonRes.root
B2DX_2015_Up_Bs2DsK_NonRes.root
DecayTree
DecayTree
###

#Bs2DsK PhiPi 2015
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2015/
B2DX_2015_Down_Bs2DsK_PhiPi.root
B2DX_2015_Up_Bs2DsK_PhiPi.root
DecayTree
DecayTree
###

#Bs2DsK KstK 2015
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2015/
B2DX_2015_Down_Bs2DsK_KstK.root
B2DX_2015_Up_Bs2DsK_KstK.root
DecayTree
DecayTree
###

#Bs2DsK KPiPi 2015
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2015/
B2DX_2015_Down_Bs2DsK_KPiPi.root
B2DX_2015_Up_Bs2DsK_KPiPi.root
DecayTree
DecayTree
###

#Bs2DsK PiPiPi 2015
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2015/
B2DX_2015_Down_Bs2DsK_PiPiPi.root
B2DX_2015_Up_Bs2DsK_PiPiPi.root
DecayTree
DecayTree
###

#Bs2DsK NonRes 2016
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2016/
B2DX_2016_Down_Bs2DsK_NonRes.root
B2DX_2016_Up_Bs2DsK_NonRes.root
DecayTree
DecayTree
###

#Bs2DsK PhiPi 2016
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2016/
B2DX_2016_Down_Bs2DsK_PhiPi.root
B2DX_2016_Up_Bs2DsK_PhiPi.root
DecayTree
DecayTree
###

#Bs2DsK KstK 2016
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2016/
B2DX_2016_Down_Bs2DsK_KstK.root
B2DX_2016_Up_Bs2DsK_KstK.root
DecayTree
DecayTree
###

#Bs2DsK KPiPi 2016
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2016/
B2DX_2016_Down_Bs2DsK_KPiPi.root
B2DX_2016_Up_Bs2DsK_KPiPi.root
DecayTree
DecayTree
###

#Bs2DsK PiPiPi 2016
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/DsK_Run2/MERGED_withCNAF/B2DX_2016/
B2DX_2016_Down_Bs2DsK_PiPiPi.root
B2DX_2016_Up_Bs2DsK_PiPiPi.root
DecayTree
DecayTree
###


#MC FileName KKPi MD 2015
{"Mode":"Bd2DK",     
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-K+_2015_Down/Bd_D-K+_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dspi_KKpi_2015_Down/Bs_Dspi_KKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"} 
{"Mode":"Bs2DsRho",  
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsrho_KKpi_2015_Down/Bs_Dsrho_KKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi", 
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsstpi_KKpi_2015_Down/Bs_Dsstpi_KKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsp",    
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsp_K+K-pi-_2015_Down/Lb_Dsp_K+K-pi-_2015_Down_Bs.root", 
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsstp",  
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsstp_K+K-pi-_2015_Down/Lb_Dsstp_K+K-pi-_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcK",    
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_LcK_pKpi_2015_Down/Lb_LcK_pKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",   
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Lcpi_pKpi_2015_Down/Lb_Lcpi_pKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bd2DPi",    
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-pi_2015_Down/Bd_D-pi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2015
{"Mode":"Bd2DK",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-K+_2015_Up/Bd_D-K+_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dspi_KKpi_2015_Up/Bs_Dspi_KKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsrho_KKpi_2015_Up/Bs_Dsrho_KKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsstpi_KKpi_2015_Up/Bs_Dsstpi_KKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsp",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsp_K+K-pi-_2015_Up/Lb_Dsp_K+K-pi-_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsstp",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsstp_K+K-pi-_2015_Up/Lb_Dsstp_K+K-pi-_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcK",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_LcK_pKpi_2015_Up/Lb_LcK_pKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Lcpi_pKpi_2015_Up/Lb_Lcpi_pKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-pi_2015_Up/Bd_D-pi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MD 2016
{"Mode":"Bd2DK",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-K+_2016_Down/Bd_D-K+_2016_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dspi_KKpi_2016_Down/Bs_Dspi_KKpi_2016_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsrho_KKpi_2016_Down/Bs_Dsrho_KKpi_2016_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsstpi_KKpi_2015_Down/Bs_Dsstpi_KKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsp",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsp_K+K-pi-_2015_Down/Lb_Dsp_K+K-pi-_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsstp",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsstp_K+K-pi-_2015_Down/Lb_Dsstp_K+K-pi-_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcK",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_LcK_pKpi_2015_Down/Lb_LcK_pKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Lcpi_pKpi_2015_Down/Lb_Lcpi_pKpi_2015_Down_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-pi_2016_Down/Bd_D-pi_2016_Down_Bs.root",
 "TreeName":"DecayTree"}
###

#MC FileName KKPi MU 2016
{"Mode":"Bd2DK",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-K+_2016_Up/Bd_D-K+_2016_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dspi_KKpi_2016_Up/Bs_Dspi_KKpi_2016_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsRho",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsrho_KKpi_2016_Up/Bs_Dsrho_KKpi_2016_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bs2DsstPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bs_Dsstpi_KKpi_2015_Up/Bs_Dsstpi_KKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsp",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsp_K+K-pi-_2015_Up/Lb_Dsp_K+K-pi-_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2Dsstp",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Dsstp_K+K-pi-_2015_Up/Lb_Dsstp_K+K-pi-_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcK",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_LcK_pKpi_2015_Up/Lb_LcK_pKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Lb2LcPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Lb_Lcpi_pKpi_2015_Up/Lb_Lcpi_pKpi_2015_Up_Bs.root",
 "TreeName":"DecayTree"}
{"Mode":"Bd2DPi",
 "FileName":"/eos/lhcb/wg/b2oc/DsK_Run2/MERGED_BDTG_noCNAF/Bd_D-pi_2016_Up/Bd_D-pi_2016_Up_Bs.root",
 "TreeName":"DecayTree"}
###



#Signal Bs2DsK KstK 2011
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_KKpi_Dn_OFFLINE_Bs_DsK_KstK.root
B2DX_MC_Bs_DsK_KKpi_Up_OFFLINE_Bs_DsK_KstK.root
DecayTree
DecayTree
###

#Signal Bs2DsK PhiPi 2011
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_KKpi_Dn_OFFLINE_Bs_DsK_phipi.root
B2DX_MC_Bs_DsK_KKpi_Up_OFFLINE_Bs_DsK_phipi.root
DecayTree
DecayTree
###

#Signal Bs2DsK NonRes 2011
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_KKpi_Dn_OFFLINE_Bs_DsK_nonres.root
B2DX_MC_Bs_DsK_KKpi_Up_OFFLINE_Bs_DsK_nonres.root
DecayTree
DecayTree
###


#Signal Bs2DsK KPiPi 2011
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_Kpipi_Dn_OFFLINE_Bs_DsK_Kpipi.root
B2DX_MC_Bs_DsK_Kpipi_Up_OFFLINE_Bs_DsK_Kpipi.root
DecayTree
DecayTree
###


#Signal Bs2DsK PiPiPi 2011
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_pipipi_Dn_OFFLINE_Bs_DsK_pipipi.root
B2DX_MC_Bs_DsK_pipipi_Up_OFFLINE_Bs_DsK_pipipi.root
DecayTree
DecayTree
###

#Signal Bs2DsK KstK 2012
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_KKpi_Dn_OFFLINE_Bs_DsK_KstK.root
B2DX_MC_Bs_DsK_KKpi_Up_OFFLINE_Bs_DsK_KstK.root
DecayTree
DecayTree
###

#Signal Bs2DsK PhiPi 2012
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_KKpi_Dn_OFFLINE_Bs_DsK_phipi.root
B2DX_MC_Bs_DsK_KKpi_Up_OFFLINE_Bs_DsK_phipi.root
DecayTree
DecayTree
###

#Signal Bs2DsK NonRes 2012
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_KKpi_Dn_OFFLINE_Bs_DsK_nonres.root
B2DX_MC_Bs_DsK_KKpi_Up_OFFLINE_Bs_DsK_nonres.root
DecayTree
DecayTree
###


#Signal Bs2DsK KPiPi 2012
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_Kpipi_Dn_OFFLINE_Bs_DsK_Kpipi.root
B2DX_MC_Bs_DsK_Kpipi_Up_OFFLINE_Bs_DsK_Kpipi.root
DecayTree
DecayTree
###

#Signal Bs2DsK PiPiPi 2012
root://eoslhcb.cern.ch//eos/lhcb/wg/b2oc/TD_DsK_3fb/TD_DsK3fb_MC/FINAL_WITH_BDTG3/
B2DX_MC_Bs_DsK_pipipi_Dn_OFFLINE_Bs_DsK_pipipi.root
B2DX_MC_Bs_DsK_pipipi_Up_OFFLINE_Bs_DsK_pipipi.root
DecayTree
DecayTree
###




#PIDK Kaon 2015
/eos/lhcb/wg/b2oc/DsK_Run2/PIDHist/
PerfHists_K_Turbo15_MagDown_fsfd_nTracks_Brunel_P_FAKE.root
PerfHists_K_Turbo15_MagUp_fsfd_nTracks_Brunel_P.root
###

#PIDK Kaon 2016
/eos/lhcb/wg/b2oc/DsK_Run2/PIDHist/
PerfHists_K_Turbo16_MagDown_fsfd_nTracks_Brunel_P.root
PerfHists_K_Turbo16_MagUp_fsfd_nTracks_Brunel_P.root
###

#PIDK Kaon 2015 2
/eos/lhcb/wg/b2oc/Lb2Dsp/PerfHists/
PerfHists_K_Turbo15_MagDown_DsK_Brunel_P_nTracks_Brunel.root
PerfHists_K_Turbo15_MagUp_DsK_Brunel_P_nTracks_Brunel.root
###

#PIDK Kaon 2016 2
/eos/lhcb/wg/b2oc/Lb2Dsp/PerfHists/
PerfHists_K_Turbo16_MagDown_DsK_Brunel_P_nTracks_Brunel.root
PerfHists_K_Turbo16_MagUp_DsK_Brunel_P_nTracks_Brunel.root
###

#PIDK Pion 2015 2
/eos/lhcb/wg/b2oc/Lb2Dsp/PerfHists/
PerfHists_Pi_Turbo15_MagDown_DsK_Brunel_P_nTracks_Brunel.root
PerfHists_Pi_Turbo15_MagUp_DsK_Brunel_P_nTracks_Brunel.root
###

#PIDK Pion 2016 2
/eos/lhcb/wg/b2oc/Lb2Dsp/PerfHists/
PerfHists_Pi_Turbo16_MagDown_DsK_Brunel_P_nTracks_Brunel.root
PerfHists_Pi_Turbo16_MagUp_DsK_Brunel_P_nTracks_Brunel.root
###

#PIDK Pion 2015
/eos/lhcb/wg/b2oc/DsK_Run2/PIDHist/
PerfHists_Pi_Turbo15_MagDown_fsfd_nTracks_Brunel_P_FAKE.root
PerfHists_Pi_Turbo15_MagUp_fsfd_nTracks_Brunel_P.root
###

#PIDK Pion 2016
/eos/lhcb/wg/b2oc/DsK_Run2/PIDHist/
PerfHists_Pi_Turbo16_MagDown_fsfd_nTracks_Brunel_P.root
PerfHists_Pi_Turbo16_MagUp_fsfd_nTracks_Brunel_P.root
###

#PIDK Proton 2015
/eos/lhcb/wg/b2oc/DsK_Run2/PIDHist/
PerfHists_P_Turbo15_MagUp_fsfd_nTracks_Brunel_P.root
PerfHists_P_Turbo15_MagDown_fsfd_nTracks_Brunel_P_FAKE.root
###

#PIDK Proton 2016
/eos/lhcb/wg/b2oc/DsK_Run2/PIDHist/
PerfHists_P_Turbo16_MagDown_fsfd_nTracks_Brunel_P.root
PerfHists_P_Turbo16_MagUp_fsfd_nTracks_Brunel_P.root
###

#DataMC 2015
/afs/cern.ch/user/a/abertoli/public/Bd2DPi/data2MC_weights/Run2/p_nTracks/
weights_DPi_2015_dw.root
weights_DPi_2015_up.root
###

#DataMC 2016
/afs/cern.ch/user/a/abertoli/public/Bd2DPi/data2MC_weights/Run2/p_nTracks/
weights_DPi_2016_dw.root
weights_DPi_2016_up.root
###


