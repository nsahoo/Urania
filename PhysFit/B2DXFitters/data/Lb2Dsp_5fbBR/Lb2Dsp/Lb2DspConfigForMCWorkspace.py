def getconfig() :

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts                                                                                                                                                             
    # order of particles: KKPi, KPiPi, PiPiPi                                                                                                                                                                           
    configdict["DsChildrenPrefix"] = {"Child1":"lab3","Child2":"lab4","Child3": "lab5"}


    ### Weighting MC samples ###                                                                                                                                                                                        
    configdict["WeightingMassTemplates"] = { "PIDBachEff":      { "2011":{"FileLabel":"#PIDK Proton 2011", "Var":["lab1_P","nTracks"], "HistName":"P_MC12TuneV2_ProbNNp>0.7_All"},
                                                                  "2012":{"FileLabel":"#PIDK Proton 2012", "Var":["lab1_P","nTracks"], "HistName":"P_MC12TuneV2_ProbNNp>0.7_All"},
                                                                  "2015":{"FileLabel":"#PIDK Proton 2015", "Var":["lab1_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2016":{"FileLabel":"#PIDK Proton 2016", "Var":["lab1_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2017":{"FileLabel":"#PIDK Proton 2016", "Var":["lab1_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2018":{"FileLabel":"#PIDK Proton 2016", "Var":["lab1_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.9_All"},
                                                              },
                                             "PIDBachPion":     { "2011":{"FileLabel":"#PIDK Pion 2011", "Var":["lab1_P","nTracks"], "HistName":"Pi_MC12TuneV2_ProbNNp>0.7_All"},
                                                                  "2012":{"FileLabel":"#PIDK Pion 2012", "Var":["lab1_P","nTracks"], "HistName":"Pi_MC12TuneV2_ProbNNp>0.7_All"},
                                                                  "2015":{"FileLabel":"#PIDK Pion 2015", "Var":["lab1_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2016":{"FileLabel":"#PIDK Pion 2016", "Var":["lab1_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2017":{"FileLabel":"#PIDK Pion 2016", "Var":["lab1_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2018":{"FileLabel":"#PIDK Pion 2016", "Var":["lab1_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.9_All"},
                                                              },

                                             "PIDBachKaon":     { "2011":{"FileLabel":"#PIDK Kaon 2011", "Var":["lab1_P","nTracks"], "HistName":"K_MC12TuneV2_ProbNNp>0.7_All"},
                                                                  "2012":{"FileLabel":"#PIDK Kaon 2012", "Var":["lab1_P","nTracks"], "HistName":"K_MC12TuneV2_ProbNNp>0.7_All"},
                                                                  "2015":{"FileLabel":"#PIDK Kaon 2015", "Var":["lab1_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2016":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab1_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2017":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab1_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.9_All"},
                                                                  "2018":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab1_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.9_All"},
                                                              },

                                             "RatioDataMC":     { "2015":{"FileLabel":"#DataMC 2015",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                  "2016":{"FileLabel":"#DataMC 2016",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                  "2017":{"FileLabel":"#DataMC 2017",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                                  "2018":{"FileLabel":"#DataMC 2018",   "Var":["lab1_P","nTracks"], "HistName":"histRatio"},
                                                              },
                                             "Shift":           { "2011":{"BeautyMass": 0.0,   "CharmMass": 0.0},
                                                                  "2012":{"BeautyMass": 0.0,   "CharmMass": 0.0},
                                                                  "2015":{"BeautyMass": -1.0,  "CharmMass": 0.0},
                                                                  "2016":{"BeautyMass": -1.1,  "CharmMass": 0.0},
                                                                  "2017":{"BeautyMass": -0.95, "CharmMass": 0.0},
                                                                  "2018":{"BeautyMass": -1.65, "CharmMass": 0.0},
                                                              }

                                             }




    return configdict
