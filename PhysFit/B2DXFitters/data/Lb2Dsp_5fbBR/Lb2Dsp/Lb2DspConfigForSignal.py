def getconfig() :

    from Lb2DspConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5100,    5800    ], "InputName" : "lab0_MassHypo_Dsp"}

    return configdict
