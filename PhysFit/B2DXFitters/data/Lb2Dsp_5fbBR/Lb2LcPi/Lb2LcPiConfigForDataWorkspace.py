def getconfig() :

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BDTG"]   = { "Range" : [-1.0,    1.0     ], "InputName" : "BDTGResponse_3"}

    return configdict
