def getconfig() :

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts                                                                                                                                           
    # order of particles: KKPi, KPiPi, PiPiPi                                                                                                                                                         
    configdict["DsChildrenPrefix"] = {"Child1":"lab3","Child2":"lab4","Child3": "lab5"}

    ### Weighting MC samples ###                                                                                                                                                                  
    configdict["WeightingMassTemplates"] = {  "PIDBachEff":        { "2011":{"FileLabel":"#PIDK Pion 2011",       "Var":["lab1_P","nTracks"], "HistName":"Pi_DLLK<10_All;"},
                                                                     "2012":{"FileLabel":"#PIDK Pion 2012",       "Var":["lab1_P","nTracks"], "HistName":"Pi_DLLK<10_All;"},
                                                                     "2015":{"FileLabel":"#PIDK Pion DsK 2015",   "Var":["nTracks","lab1_P"], "HistName":"Pi_DLLK < 0 && IsMuon ==0.0_All;"}, 
                                                                     "2016":{"FileLabel":"#PIDK Pion DsK 2016",   "Var":["nTracks","lab1_P"], "HistName":"Pi_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2017":{"FileLabel":"#PIDK Pion DsK 2017",   "Var":["nTracks","lab1_P"], "HistName":"Pi_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2018":{"FileLabel":"#PIDK Pion DsK 2018",   "Var":["nTracks","lab1_P"], "HistName":"Pi_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                 },
                                             "PIDBachMisID":       { 
                                                                     "2011":{"FileLabel":"#PIDK Kaon DsK 2015",   "Var":["nTracks","lab1_P"], "HistName":"K_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2012":{"FileLabel":"#PIDK Kaon DsK 2015",   "Var":["nTracks","lab1_P"], "HistName":"K_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2015":{"FileLabel":"#PIDK Kaon DsK 2015",   "Var":["nTracks","lab1_P"], "HistName":"K_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2016":{"FileLabel":"#PIDK Kaon DsK 2016",   "Var":["nTracks","lab1_P"], "HistName":"K_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2017":{"FileLabel":"#PIDK Kaon DsK 2017",   "Var":["nTracks","lab1_P"], "HistName":"K_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                     "2018":{"FileLabel":"#PIDK Kaon DsK 2018",   "Var":["nTracks","lab1_P"], "HistName":"K_DLLK < 0 && IsMuon ==0.0_All;"},
                                                                 },


                                               "PIDChild1Eff":      {"2011":{"FileLabel":"#PIDK Proton 2011", "Var":["lab3_P","nTracks"], "HistName":"P_MC12TuneV2_ProbNNp>0.6_All"},
                                                                     "2012":{"FileLabel":"#PIDK Proton 2012", "Var":["lab3_P","nTracks"], "HistName":"P_MC12TuneV2_ProbNNp>0.6_All"},
                                                                     "2015":{"FileLabel":"#PIDK Proton 2015", "Var":["lab3_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                     "2016":{"FileLabel":"#PIDK Proton 2016", "Var":["lab3_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                     "2017":{"FileLabel":"#PIDK Proton 2016", "Var":["lab3_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                     "2018":{"FileLabel":"#PIDK Proton 2016", "Var":["lab3_P","nTracks"], "HistName":"P_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                   },
                                              "PIDChild2Eff":      { "2011":{"FileLabel":"#PIDK Kaon 2011", "Var":["lab4_P","nTracks"], "HistName":"K_DLLK>0_All;"},
                                                                     "2012":{"FileLabel":"#PIDK Kaon 2012", "Var":["lab4_P","nTracks"], "HistName":"K_DLLK>0_All;"},
                                                                     "2015":{"FileLabel":"#PIDK Kaon 2015", "Var":["lab4_P","nTracks"], "HistName":"K_DLLK>0_All;"},
                                                                     "2016":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab4_P","nTracks"], "HistName":"K_DLLK>0_All;"},
                                                                     "2017":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab4_P","nTracks"], "HistName":"K_DLLK>0_All;"},
                                                                     "2018":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab4_P","nTracks"], "HistName":"K_DLLK>0_All;"},
                                                                   },
                                              "PIDChild3Eff":      { "2011":{"FileLabel":"#PIDK Pion 2011", "Var":["lab5_P","nTracks"], "HistName":"Pi_DLLK<5_All;"},
                                                                     "2012":{"FileLabel":"#PIDK Pion 2012", "Var":["lab5_P","nTracks"], "HistName":"Pi_DLLK<5_All;"},
                                                                     "2015":{"FileLabel":"#PIDK Pion 2015", "Var":["lab5_P","nTracks"], "HistName":"Pi_DLLK<5_All;"},
                                                                     "2016":{"FileLabel":"#PIDK Pion 2016", "Var":["lab5_P","nTracks"], "HistName":"Pi_DLLK<5_All;"},
                                                                     "2017":{"FileLabel":"#PIDK Pion 2016", "Var":["lab5_P","nTracks"], "HistName":"Pi_DLLK<5_All;"},
                                                                     "2018":{"FileLabel":"#PIDK Pion 2016", "Var":["lab5_P","nTracks"], "HistName":"Pi_DLLK<5_All;"},
                                                                   },
                                              "PIDChildKaonProton": { "2011":{"FileLabel":"#PIDK Kaon 2015", "Var":["lab3_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2012":{"FileLabel":"#PIDK Kaon 2015", "Var":["lab3_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2015":{"FileLabel":"#PIDK Kaon 2015", "Var":["lab3_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2016":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab3_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2017":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab3_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2018":{"FileLabel":"#PIDK Kaon 2016", "Var":["lab3_P","nTracks"], "HistName":"K_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                  },
                                              "PIDChildPionProton": { "2011":{"FileLabel":"#PIDK Pion 2015", "Var":["lab3_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2012":{"FileLabel":"#PIDK Pion 2015", "Var":["lab3_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2015":{"FileLabel":"#PIDK Pion 2015", "Var":["lab3_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2016":{"FileLabel":"#PIDK Pion 2016", "Var":["lab3_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2017":{"FileLabel":"#PIDK Pion 2016", "Var":["lab3_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                      "2018":{"FileLabel":"#PIDK Pion 2016", "Var":["lab3_P","nTracks"], "HistName":"Pi_MC15TuneV1_ProbNNp>0.6_All;"},
                                                                  },

                                             "Shift":            { "2011":{"BeautyMass": -2.0,  "CharmMass": 0.0},
                                                                   "2012":{"BeautyMass": -2.0,  "CharmMass": 0.0},
                                                                   "2015":{"BeautyMass": -1.0,  "CharmMass": 0.0},
                                                                   "2016":{"BeautyMass": -1.1,  "CharmMass": 0.0},
                                                                   "2017":{"BeautyMass": -0.95, "CharmMass": 0.0},
                                                                   "2018":{"BeautyMass": -1.65, "CharmMass": 0.0},
                                                                 }
                                    }


    return configdict
