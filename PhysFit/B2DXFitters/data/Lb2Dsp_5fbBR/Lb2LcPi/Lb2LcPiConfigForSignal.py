def getconfig() :

    from Lb2LcPiConfigForNominalMassFit import getconfig as getconfig_nominal
    configdict = getconfig_nominal()

    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5100,    5800    ], "InputName" : "lab0_MM"}

    return configdict
