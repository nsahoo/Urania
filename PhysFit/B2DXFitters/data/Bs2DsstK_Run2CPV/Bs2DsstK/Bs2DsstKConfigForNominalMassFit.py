def getconfig() :

    configdict = {}
    
    from math import pi
    from math import log

    # considered decay mode                                                       
    configdict["Decay"] = "Bs2DsstK"
    configdict["Backgrounds"] = ["Bs2DsRho","Bs2DsstRho","Bs2DsstPi","Bd2DsstK","Bs2DsKst","Bd2DsKst","Bs2DsstKst","Bd2DsstKst"]
    # configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "KPiPi", "PiPiPi"}
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK"}
    # year of data taking                                                                                 
    configdict["YearOfDataTaking"] = {"2015", "2016"}
    # stripping (necessary in case of PIDK shapes)                               
    configdict["Stripping"] = {"2012":"21", "2011":"21r1"}
    # integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {"2011": {"Down": 0.5600, "Up": 0.4200},
                                          "2012": {"Down": 0.9912, "Up": 0.9988},
                                          "2015": {"Down": 0.1540, "Up": 0.1740},
                                          "2016": {"Down": 0.8325, "Up": 0.8325}
                                          }
    # file name with paths to MC/data samples                                     
    configdict["dataName"] = "../data/Bs2DsstK_Run2CPV/Bs2DsstK/config_Bs2DsstK.txt"

    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = { "Directory": "PlotBs2DsstK", "Extension":"pdf"}

    # basic variables                                                                                        
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"]    = { "Range" : [5100,    6000    ], "InputName" : "FBs_DsstMC_M"}
    configdict["BasicVariables"]["CharmMass"]     = { "Range" : [2080,    2150    ], "InputName" : "FDsst_DeltaM_M"}
    configdict["BasicVariables"]["BeautyTime"]    = { "Range" : [0.4,     12.0    ], "InputName" : "FBs_LF_ctau"}
    configdict["BasicVariables"]["BacP"]          = { "Range" : [1000.,   650000.0], "InputName" : "FBac_P"}
    configdict["BasicVariables"]["BacPT"]         = { "Range" : [500.,    45000.0 ], "InputName" : "FBac_Ptr"}
    configdict["BasicVariables"]["BacPIDK"]       = { "Range" : [-150.0,  150     ], "InputName" : "FBac_PIDK"}
    configdict["BasicVariables"]["nTracks"]       = { "Range" : [15.,     1000.0  ], "InputName" : "FnTracks"}
    configdict["BasicVariables"]["BeautyTimeErr"] = { "Range" : [0.01,    0.1     ], "InputName" : "FBs_LF_ctauErr"}
    configdict["BasicVariables"]["BacCharge"]     = { "Range" : [-1000.0, 1000.0  ], "InputName" : "FBac_ID"}
    configdict["BasicVariables"]["BDTG"]          = { "Range" : [-1.0,    1.0     ], "InputName" : "FBDT_Var"}
    configdict["BasicVariables"]["TagDecOS"]      = { "Range" : [-1.0,    1.0     ], "InputName" : "FBs_TAGDECISION_OS"}
    configdict["BasicVariables"]["TagDecSS"]      = { "Range" : [-1.0,    1.0     ], "InputName" : "FBs_SS_nnetKaon_DEC"}
    configdict["BasicVariables"]["MistagOS"]      = { "Range" : [0.0,     0.5     ], "InputName" : "FBs_TAGOMEGA_OS"}
    configdict["BasicVariables"]["MistagSS"]      = { "Range" : [0.0,     0.5     ], "InputName" : "FBs_SS_nnetKaon_PROB"}
    '''
    # additional variables in data sets
    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["BacEta"]   = { "Range" : [-9.0,    9.0     ], "InputName" : "FBac_Eta"}
    configdict["AdditionalVariables"]["PhEta"]    = { "Range" : [-9.0,    9.0     ], "InputName" : "FPh_Eta"}
    configdict["AdditionalVariables"]["PhPT"]     = { "Range" : [ 0.0,    1.e+9   ], "InputName" : "FPh_Ptr"}
    configdict["AdditionalVariables"]["PhCL"]     = { "Range" : [ 0.0,    1.0     ], "InputName" : "FPh_CL"}
    configdict["AdditionalVariables"]["PhisNotE"] = { "Range" : [ 0.0,    1.0     ], "InputName" : "FPh_isNotE"}
    configdict["AdditionalVariables"]["BsEta"]    = { "Range" : [-9.0,    9.0     ], "InputName" : "FBs_Eta"}
    configdict["AdditionalVariables"]["BsPhi"]    = { "Range" : [ 0.0,    9.0     ], "InputName" : "FBs_Phi"}
    configdict["AdditionalVariables"]["BsPT"]     = { "Range" : [ 0.0,    1.e+9   ], "InputName" : "FBs_Ptr"}
    configdict["AdditionalVariables"]["DsDec"]    = { "Range" : [ 0.0,    6.0     ], "InputName" : "FDs_Dec"}
    configdict["AdditionalVariables"]["KpPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FKp_Ptr"}
    configdict["AdditionalVariables"]["KmPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FKm_Ptr"}
    configdict["AdditionalVariables"]["PiPT"]     = { "Range" : [100.,45000.0     ], "InputName" : "FPi_Ptr"}
    configdict["AdditionalVariables"]["PtrRel"]   = { "Range" : [ 0.0,  200.      ], "InputName" : "FPtr_Rel"}
    configdict["AdditionalVariables"]["CosTheS"]  = { "Range" : [-1.0,     1.0    ], "InputName" : "FCTS_Ds"}
    configdict["AdditionalVariables"]["BsIpChi2Own"] = { "Range" : [ 0.0,  100.    ], "InputName" : "FBs_IpChi2Own"}
    configdict["AdditionalVariables"]["BsDIRAOwn"]= { "Range" : [-1.0,     1.0    ], "InputName" : "FBs_DIRAOwn"}
    configdict["AdditionalVariables"]["BsRFD"]    = { "Range" : [ 0.0,   100.0    ], "InputName" : "FBs_RFD"}
    configdict["AdditionalVariables"]["DsDIRAOri"]= { "Range" : [-1.0,     1.0    ], "InputName" : "FDs_DIRAOri"}
    configdict["AdditionalVariables"]["nSPDHits"] = { "Range" : [ 0.0,  1000.0    ], "InputName" : "FnSPDHits"}
    '''
    
    # tagging calibration                                                                                               
    configdict["TaggingCalibration"] = {}
    configdict["TaggingCalibration"]["OS"] = {"p0": 0.3834, "p1": 0.9720, "average": 0.3813}
    configdict["TaggingCalibration"]["SS"] = {"p0": 0.4244, "p1": 1.2180, "average": 0.4097}

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"]    = {"Data": "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&(FDsst_M-FDs_M)<181.5&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01&&FBac_PIDK>5.0",
                                              "MC":   "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&(FDsst_M-FDs_M)<181.5&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01"}
    configdict["AdditionalCuts"]["NonRes"] = {"Data":"FDs_Dec==3.", "MC":"FDs_Dec==3."}
    configdict["AdditionalCuts"]["PhiPi"]  = {"Data":"FDs_Dec==1.", "MC":"FDs_Dec==1."}
    configdict["AdditionalCuts"]["KstK"]   = {"Data":"FDs_Dec==2.", "MC":"FDs_Dec==2."}
    configdict["AdditionalCuts"]["KPiPi"]  = {"Data":"FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FPi_PIDK>10.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FKst_M<1750.",
                                              "MC":  "FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FKst_M<1750."}
    configdict["AdditionalCuts"]["PiPiPi"] = {"Data":"FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FPi_PIDK<0.&&FPhi_M<1700.&&FKst_M<1700.", 
                                              "MC":  "FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FPhi_M<1700.&&FKst_M<1700."}
 
    configdict["CreateCombinatorial"] = {}
    configdict["CreateCombinatorial"]["BeautyMass"] = {} 
    configdict["CreateCombinatorial"]["BeautyMass"]["All"]    = {"Cut":"FBs_DsstMC_M>5100.&&FBs_DsstMC_M<6000.&&FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>195.&&(FDsst_M-FDs_M)<205.&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01&&FBac_P>1000.&&FBac_P<650000.&&FBac_Ptr>500.&&FBac_Ptr<45000.",
                                                                 "Rho":3.5, "Mirror":"Both"}
    configdict["CreateCombinatorial"]["BeautyMass"]["NonRes"] = {"Cut":"FDs_Dec==3."}
    configdict["CreateCombinatorial"]["BeautyMass"]["PhiPi"]  = {"Cut":"FDs_Dec==1."}
    configdict["CreateCombinatorial"]["BeautyMass"]["KstK"]   = {"Cut":"FDs_Dec==2."}
    configdict["CreateCombinatorial"]["BeautyMass"]["KPiPi"]  = {"Cut":"FDs_Dec==5.&&FDs_FDCHI2_ORIVX>9.&&FPi_PIDK>10.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FKst_M<1750."}
    configdict["CreateCombinatorial"]["BeautyMass"]["PiPiPi"] = {"Cut":"FDs_Dec==4.&&FDs_FDCHI2_ORIVX>9.&&FKp_PIDK<0.&&FKm_PIDK<0.&&FPi_PIDK<0.&&FPhi_M<1700.&&FKst_M<1700."}

    # configdict["WeightingMassTemplates"] = { "RatioDataMC":{"FileLabel":{"2011":"#RatioDataMC 2011 PNTr","2012":"#RatioDataMC 2012 PNTr"},
    #                                                         "Var":["FBac_P","FnTracks"],"HistName":"histRatio"},
    #                                          "Shift":{ "BeautyMass": 0., "CharmMass": 0.} }
    # configdict["WeightingMassTemplates"] = { "Shift":{ "BeautyMass": 0., "CharmMass": 0.} }
    configdict["WeightingMassTemplates"] = { }

    #weighting for PID templates
    # --MCPID, --MC, --SignalPID, --Signal
    configdict["ObtainPIDTemplates"] = { "Variables":["BacP","nTracks"], "Bins":[30,30] }
    # --CombPID
    # configdict["ObtainPIDTemplates"] = { "Variables":["BacPT","nTracks"], "Bins":[30,30] }

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    ###                                                               MDfit fitting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#
    
    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"]    = {"Run2": {"All": 5.3679e+03}, "Fixed":False}
    configdict["SignalShape"]["BeautyMass"]["sigma1"]  = {"Run2": {"All": 1.2346e+01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["sigma2"]  = {"Run2": {"All": 1.7520e+01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["alpha1"]  = {"Run2": {"All":-1.6541e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["alpha2"]  = {"Run2": {"All": 1.9086e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["n1"]      = {"Run2": {"All": 2.2690e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["n2"]      = {"Run2": {"All": 1.3914e+00}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["frac"]    = {"Run2": {"All": 4.2576e-01}, "Fixed":True}
    configdict["SignalShape"]["BeautyMass"]["R"]    =    {"Run2": {"All": 1.},         "Fixed":False}
    
    # Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"]    = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["CharmMass"]["mean"]    = {"Run2": {"All": 2.1127e+03}, "Fixed":False}
    configdict["SignalShape"]["CharmMass"]["sigma1"]  = {"Run2": {"All": 8.3507e+00},  "Fixed":True} 
    configdict["SignalShape"]["CharmMass"]["sigma2"]  = {"Run2": {"All": 9.9230e+00},  "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["alpha1"]  = {"Run2": {"All": 9.1471e-01},  "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["alpha2"]  = {"Run2": {"All":-5.1880e-01}, "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["n1"]      = {"Run2": {"All": 1.1845e+02},  "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["n2"]      = {"Run2": {"All": 3.0395e+01},  "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["frac"]    = {"Run2": {"All": 2.5227e-01},  "Fixed":True}
    configdict["SignalShape"]["CharmMass"]["R"]    =    {"Run2": {"All": 1.},         "Fixed":False}

    # combinatorial background              
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "RooKeysPdf"
    configdict["CombBkgShape"]["BeautyMass"]["name"] = {"2015": {"NonRes": "PhysBkgCombK_BeautyMassPdf_m_both_nonres_2015",
                                                                 "PhiPi":  "PhysBkgCombK_BeautyMassPdf_m_both_phipi_2015",
                                                                 "KstK":   "PhysBkgCombK_BeautyMassPdf_m_both_kstk_2015",
                                                                 "KPiPi":  "PhysBkgCombK_BeautyMassPdf_m_both_kpipi_2015",
                                                                 "PiPiPi": "PhysBkgCombK_BeautyMassPdf_m_both_pipipi_2015"},
                                                        "2016": {"NonRes": "PhysBkgCombK_BeautyMassPdf_m_both_nonres_2016",
                                                                 "PhiPi":  "PhysBkgCombK_BeautyMassPdf_m_both_phipi_2016",
                                                                 "KstK":   "PhysBkgCombK_BeautyMassPdf_m_both_kstk_2016",
                                                                 "KPiPi":  "PhysBkgCombK_BeautyMassPdf_m_both_kpipi_2016",
                                                                 "PiPiPi": "PhysBkgCombK_BeautyMassPdf_m_both_pipipi_2016"}}

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"]  = "ExponentialPlusSignal" 
    configdict["CombBkgShape"]["CharmMass"]["cD"]    = {"Run2": {"NonRes":0.01, "PhiPi":0.01, "KstK":0.01, "KPiPi":0.01, "PiPiPi":0.01}, "Fixed":False}
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {"Run2": {"NonRes":0.5,  "PhiPi":0.5,  "KstK":0.5,  "KPiPi":1.0,  "PiPiPi":1.0},  "Fixed":{"KPiPi":True, "PiPiPi":True}}

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "Fixed"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = { "Kaon":True, "Pion":True, "Proton":False }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"]   = { "Run2":{"All":0.7}, "Fixed":False }

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"]    = "ShiftedSignal"
    configdict["Bd2DsstKShape"]["BeautyMass"]["shift"]   = {"Run2": {"All": 86.8}, "Fixed":True}
    configdict["Bd2DsstKShape"]["BeautyMass"]["scale1"]  = {"Run2": {"All": 1.00}, "Fixed":True}
    configdict["Bd2DsstKShape"]["BeautyMass"]["scale2"]  = {"Run2": {"All": 1.00}, "Fixed":True}

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {"Run2":{"All":{"Both":{ "CentralValue":0.41, "Range":[0.0,1.0]}}}, "Fixed":True}
    configdict["AdditionalParameters"]["g1_f2_frac"] = {"Run2":{"All":{"Both":{ "CentralValue":0.41, "Range":[0.0,1.0]}}}, "Fixed":True}
    configdict["AdditionalParameters"]["g2_f1_frac"] = {"Run2":{"All":{"Both":{ "CentralValue":0.50, "Range":[0.0,1.0]}}}, "Fixed":False}
    '''
    configdict["AdditionalParameters"]["g3_f1_frac"] = {"Run2":{"All":{"Both":{ "CentralValue":0.7,  "Range":[0.0,1.0]}}}, "Fixed":False}
    configdict["AdditionalParameters"]["g3_f2_frac"] = {"Run2":{"NonRes":{"Both":{ "CentralValue":0.6,  "Range":[0.0,1.0]}},
                                                                "PhiPi":{"Both":{ "CentralValue":0.6,  "Range":[0.0,1.0]}},
                                                                "KstK":{"Both":{ "CentralValue":0.6,  "Range":[0.0,1.0]}},
                                                                "KPiPi":{"Both":{ "CentralValue":0.6,  "Range":[0.0,1.0]}},
                                                                "PiPiPi":{"Both":{ "CentralValue":0.6,  "Range":[0.0,1.0]}}}, "Fixed":False}
    configdict["AdditionalParameters"]["g4_f1_frac"] = {"Run2":{"NonRes":{"Both":{ "CentralValue":0.8, "Range":[0.5,1.0]}}, 
                                                                "PhiPi":{"Both":{ "CentralValue":0.8, "Range":[0.5,1.0]}},
                                                                "KstK":{"Both":{ "CentralValue":0.8, "Range":[0.5,1.0]}},
                                                                "KPiPi":{"Both":{ "CentralValue":0.8, "Range":[0.5,1.0]}},
                                                                "PiPiPi":{"Both":{ "CentralValue":0.8, "Range":[0.5,1.0]}}}, "Fixed":False}
    '''

    #expected yields                                                                                                                                                       
    configdict["Yields"] = {}
    
    configdict["Yields"]["Bs2DsDsstRho"]   = {"2015": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.},  
                                              "2016": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.}, "Fixed":True}
    configdict["Yields"]["Bd2DsstK"]       = {"2015": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.},
                                              "2016": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.}, "Fixed":True}
    configdict["Yields"]["Bs2DsstPi"]      = {"2015": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.},
                                              "2016": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.}, "Fixed":True}
    configdict["Yields"]["BsBd2DsKst"]     = {"2015": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.},
                                              "2016": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.}, "Fixed":True}
    configdict["Yields"]["BsBd2DsstKst"]   = {"2015": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.},
                                              "2016": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.}, "Fixed":True}
    configdict["Yields"]["CombBkg"]        = {"2015": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.},
                                              "2016": {"NonRes":0.,  "PhiPi":0.,  "KstK":0.,  "KPiPi":0.,  "PiPiPi":0.}, "Fixed":True}
    configdict["Yields"]["Signal"]         = {"2015": {"NonRes":20000., "PhiPi":20000., "KstK":20000., "KPiPi":5000., "PiPiPi":5000.},
                                              "2016": {"NonRes":20000., "PhiPi":20000., "KstK":20000., "KPiPi":5000., "PiPiPi":5000.}, "Fixed":False}

    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#    
    ###                                                               MDfit plotting settings
    #----------------------------------------------------------------------------------------------------------------------------------------------------------------#

    from ROOT import *
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bs2DsDsstRho", "BsBd2DsstKst", "Bd2DsstK"]
    configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kOrange, kMagenta-2, kBlue-10, kRed+1, kBlue+2]
    #configdict["PlotSettings"]["components"] = ["Sig", "CombBkg", "Bs2DsRho", "Bs2DsstRho"]
    #configdict["PlotSettings"]["colors"] = [kRed-7, kBlue-6, kBlue-10, kGreen+3]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {"Position":[0.53, 0.45, 0.90, 0.91], "TextSize": 0.05, "LHCbText":[0.7,0.9]}
    configdict["LegendSettings"]["CharmMass"]  = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.7,0.9],
                                                  "ScaleYSize":1.5, "SetLegendColumns":2, "LHCbTextSize":0.075 }
    configdict["LegendSettings"]["BacPIDK"]    = {"Position":[0.20, 0.69, 0.93, 0.93], "TextSize": 0.05, "LHCbText":[0.7,0.9], 
                                                  "ScaleYSize":1.5, "SetLegendColumns":2, "LHCbTextSize":0.075}

    return configdict
