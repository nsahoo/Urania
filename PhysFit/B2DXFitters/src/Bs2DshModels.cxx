//---------------------------------------------------------------------------//
//                                                                           //
//  RooFit models for Bs -> Ds h                                             //
//                                                                           //
//  Source file                                                              //
//                                                                           //
//  Authors: Agnieszka Dziurda                                               //
//  Date   : 28 / 01 / 2018                                                  //
//                                                                           //
//---------------------------------------------------------------------------//

// STL includes


// ROOT and RooFit includes
#include "TFile.h"
#include "RooArgList.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooExtendPdf.h"
#include "RooExponential.h"
#include "RooWorkspace.h"
#include "RooAbsReal.h" 
#include "TString.h" 
#include "RooKeysPdf.h" 

// B2DXFitters includes
#include "B2DXFitters/Bs2DshModels.h"
#include "B2DXFitters/Bs2Dsh2011TDAnaModels.h"
#include "B2DXFitters/BasicMDFitPdf.h"
#include "B2DXFitters/Bd2DhModels.h"
#include "B2DXFitters/GeneralUtils.h"
#include "B2DXFitters/RooBinned1DQuinticBase.h"
#include "B2DXFitters/RooIpatia2.h"
#include "B2DXFitters/RooApollonios.h"

using namespace std;
using namespace GeneralUtils;
using namespace BasicMDFitPdf;
using namespace Bs2Dsh2011TDAnaModels; 

namespace Bs2DshModels {
  
  //=============================================================================== 
  // Background model for Bs->DsPi MDFit. 
  //=============================================================================== 
  RooAbsPdf* build_Bs2DsPi_BKG_MDFitter( RooWorkspace* work,
					 RooWorkspace* workInt,
					 std::vector <RooAbsReal*> obs,
					 std::vector <TString> types,
					 TString &samplemode,
					 std::vector<TString> merge,
					 bool debug)
  {
    
    if (debug == true)
      {
	cout<<"[INFO] =====> Build background model Bs->DsPi --------------"<<endl;
      }
    RooArgList* list = new RooArgList();
    RooRealVar* getObs = (RooRealVar*)work->var("BeautyMass");
    Double_t lowMass = getObs->getMin();

    TString nBs2DsDsstPiRhoName = "nBs2DsDsstPiRho_"+samplemode+"_Evts";
    RooRealVar* nBs2DsDsstPiRhoEvts = tryVar(nBs2DsDsstPiRhoName, workInt,debug);
    Double_t valBs2DsDsstPiRho = nBs2DsDsstPiRhoEvts->getValV();

    TString nBd2DsPiName;
    RooRealVar* nBd2DsPiEvts = NULL;
    Double_t valBd2DsPi = 1.0;

    if ( lowMass < 5250.0 )
      {
	nBd2DsPiName = "nBd2DsPi_"+samplemode+"_Evts";
	nBd2DsPiEvts = tryVar(nBd2DsPiName, workInt,debug);
	valBd2DsPi = nBd2DsPiEvts->getValV();
      }


    TString g1_f1_Name = "g1_f1_frac_"+samplemode;
    RooRealVar* g1_f1 = tryVar(g1_f1_Name, workInt,debug);

    TString g1_f2_Name;
    RooRealVar* g1_f2 = NULL;

    if (lowMass < 5150.0)
      {
        g1_f2_Name = "g1_f2_frac_"+samplemode;
        g1_f2 = tryVar(g1_f2_Name, workInt,debug);
      }

    TString mode = CheckDMode(samplemode,debug);
    if ( mode == "" ) { mode = CheckKKPiMode(samplemode, debug); }

    TString name="";
    TString m = "";

    RooProdPdf* pdf_Bd2DsPi_Tot = NULL;
    RooExtendPdf* epdf_Bd2DsPi = NULL;

    if ( valBs2DsDsstPiRho != 0.0 )
      {
	pdf_Bd2DsPi_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bd2DsPi", "", merge, debug);
      }
    if (lowMass < 5250.0 && valBd2DsPi !=0.0)
      {
	name = "Bd2DsPiEPDF_m_"+samplemode;
	epdf_Bd2DsPi = new RooExtendPdf( name.Data() , pdf_Bd2DsPi_Tot-> GetTitle(), *pdf_Bd2DsPi_Tot  , *nBd2DsPiEvts);
	CheckPDF(epdf_Bd2DsPi, debug);
	list = AddEPDF(list, epdf_Bd2DsPi, nBd2DsPiEvts, debug);
      }

    // --------------------------------- Read PDFs from Workspace -------------------------------------------------//                                                         
    if (debug == true) cout<<endl;
    if (debug == true) cout<<"---------------  Read PDF's from the workspace -----------------"<<endl;

    RooExtendPdf* epdf_Bd2DPi = NULL;
    epdf_Bd2DPi = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Bd2DPi", "", merge, debug);
    Double_t valBd2DPi = CheckEvts(workInt, samplemode, "Bd2DPi",debug);
    list = AddEPDF(list, epdf_Bd2DPi, valBd2DPi, debug);

    //-----------------------------------------//                                                                                                                            

    RooExtendPdf* epdf_Lb2LcPi = NULL;
    epdf_Lb2LcPi = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Lb2LcPi", "", merge, debug);
    Double_t valLb2LcPi = CheckEvts(workInt, samplemode, "Lb2LcPi",debug);
    list = AddEPDF(list, epdf_Lb2LcPi, valLb2LcPi, debug);

    //-----------------------------------------//                                                                                                                             
    RooExtendPdf* epdf_Bs2DsK = NULL;
    epdf_Bs2DsK = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Bs2DsK", "", merge, debug);
    Double_t valBs2DsK = CheckEvts(workInt, samplemode, "Bs2DsK",debug);
    list = AddEPDF(list, epdf_Bs2DsK, valBs2DsK, debug);

    // --------------------------------- Create RooAddPdf -------------------------------------------------//                                                                  
    RooProdPdf* pdf_Bs2DsstPi_Tot = NULL;
    RooProdPdf* pdf_Bs2DsRho_Tot = NULL;
    RooProdPdf* pdf_Bs2DsstRho_Tot = NULL;
    RooAddPdf* pdf_Bs2DsDsstPiRho_Tot = NULL;
    RooExtendPdf* epdf_Bs2DsDsstPiRho   = NULL;

    if ( valBs2DsDsstPiRho!= 0)
      {

	if ( lowMass > 5250.0 )
	  {
	    if ( debug )
	      {
		std::cout<<"[INFO] The Bs2DsstPiRho background is composed of Bs2DsstPi and Bd2DsPi"<<std::endl;
	      }

	    for( TString type: types ) 
	      { 
		if ( type.Contains("Bs2DsstPi") && type.Contains("BacPIDK") ) { type = "Bs2DsstPi_BacPIDK_Signal"; } 
	      }
	    pdf_Bs2DsstPi_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstPi", "", merge, debug);

	    name="PhysBkgBs2DsDsstPiPdf_m_"+samplemode+"_Tot";
	    pdf_Bs2DsDsstPiRho_Tot = new RooAddPdf( name.Data(),
						    name.Data(),
						    RooArgList(*pdf_Bs2DsstPi_Tot, *pdf_Bd2DsPi_Tot),
						    RooArgList(*g1_f1)
						    );
	    CheckPDF(pdf_Bs2DsDsstPiRho_Tot, debug);
	  }
	else
	  {
	    pdf_Bs2DsRho_Tot  = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsRho", "", merge, debug);
	    pdf_Bs2DsstPi_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstPi", "", merge, debug);
	    
	    name="PhysBkgBs2DsDsstPiPdf_m_"+samplemode+"_Tot";
	    if ( lowMass > 5150.0 )
	      {
		if ( debug )
		  {
		    std::cout<<"[INFO] The Bs2DsstPiRho background is composed of Bs2DsstPi and Bs2DsRho"<<std::endl;
		  }
		pdf_Bs2DsDsstPiRho_Tot = new RooAddPdf( name.Data(), name.Data(),
							RooArgList(*pdf_Bs2DsstPi_Tot, *pdf_Bs2DsRho_Tot),
							RooArgList(*g1_f1)
							);
	      }
	    else
	      {
		if ( debug )
		  {
		    std::cout<<"[INFO] The Bs2DsstPiRho background is composed of Bs2DsstPi, Bs2DsRho and Bs2DsstRho"<<std::endl;
		  }
		pdf_Bs2DsstRho_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstRho", "", merge, debug);
		pdf_Bs2DsDsstPiRho_Tot = new RooAddPdf( name.Data(), name.Data(),
							RooArgList(*pdf_Bs2DsstPi_Tot, *pdf_Bs2DsRho_Tot, *pdf_Bs2DsstRho_Tot),
							RooArgList(*g1_f1,*g1_f2), true                                                                                 
							);
	      }
	    CheckPDF(pdf_Bs2DsDsstPiRho_Tot, debug);

	  }
	name = "Bs2DsDsstPiRhoEPDF_m_"+samplemode;
	epdf_Bs2DsDsstPiRho = new RooExtendPdf( name.Data() , pdf_Bs2DsDsstPiRho_Tot-> GetTitle(), *pdf_Bs2DsDsstPiRho_Tot  , *nBs2DsDsstPiRhoEvts);
	CheckPDF(epdf_Bs2DsDsstPiRho, debug);
	list = AddEPDF(list, epdf_Bs2DsDsstPiRho, nBs2DsDsstPiRhoEvts, debug);
      }
    
    RooAbsPdf* pdf_totBkg = NULL;
    name = "BkgEPDF_m_"+samplemode;
    pdf_totBkg = new RooAddPdf( name.Data(), name.Data(),*list);

    if (debug == true)
      {
	cout<<endl;
	if( pdf_totBkg != NULL )
	  {
	    cout<<" ------------- CREATED TOTAL BACKGROUND PDF: SUCCESFULL------------"<<endl;
	    std::cout<<"Name: "<<pdf_totBkg->GetName();
	    pdf_totBkg->Print("v");
	  }
	else { cout<<" ---------- CREATED TOTAL BACKGROUND PDF: FAILED ----------------"<<endl;}
      }

    return pdf_totBkg;

  }

  //=============================================================================== 
  // Background mode for Bs->DsK MDFit. 
  //===============================================================================
  
  
  RooAbsPdf* build_Bs2DsK_BKG_MDFitter(RooWorkspace* work,
                                       RooWorkspace* workInt,
				       std::vector <RooAbsReal*> obs,
				       std::vector <TString> types,
                                       TString &samplemode,
				       std::vector<TString> merge,
                                       bool debug)
  {

    if (debug == true)
      {
	cout<<"--------------------------------------------------------"<<endl;
	cout<<"=====> Build background model BsDsK for simultaneous fit"<<endl;
	cout<<"--------------------------------------------------------"<<endl;
      }

    RooArgList* list = new RooArgList();
    //TString charmVarName = "CharmMass"; //massDs.GetName();
    TString mode = CheckDMode(samplemode,debug);
    if ( mode == "" ) { mode = CheckKKPiMode(samplemode, debug); }
    RooRealVar* getObs = (RooRealVar*)work->var("BeautyMass");
    Double_t lowMass = getObs->getMin();

    TString nBsLb2DsDsstPPiRhoName = "nBsLb2DsDsstPPiRho_"+samplemode+"_Evts";
    RooRealVar* nBsLb2DsDsstPPiRhoEvts = tryVar(nBsLb2DsDsstPPiRhoName, workInt,debug); 
    Double_t valBsLb2DsDsstPPiRho = nBsLb2DsDsstPPiRhoEvts->getValV();
    TString nBs2DsDssKKstName = "nBs2DsDsstKKst_"+samplemode+"_Evts";
    RooRealVar* nBs2DsDssKKstEvts = tryVar(nBs2DsDssKKstName, workInt,debug); 
    Double_t valBs2DsDssKKst = nBs2DsDssKKstEvts->getValV();

    TString g1_f1_Name;
    RooRealVar* g1_f1 =NULL;
    TString g1_f2_Name;
    RooRealVar* g1_f2 =NULL;
    TString g1_f3_Name;
    RooRealVar* g1_f3 =NULL;
    
    if ( lowMass < 5300.0)
      {
	g1_f1_Name = "g1_f1_frac_"+samplemode;
        g1_f1 = tryVar(g1_f1_Name, workInt,debug);
      }
    if ( lowMass < 5250.0)
      {
	g1_f2_Name = "g1_f2_frac_"+samplemode;
        g1_f2 = tryVar(g1_f2_Name, workInt,debug);
      }
    if ( lowMass < 5150.0)
      { 
	g1_f3_Name = "g1_f3_frac_"+samplemode;
        g1_f3 = tryVar(g1_f3_Name, workInt,debug);
      } 

    TString g2_f1_Name = "g2_f1_frac_"+samplemode;
    RooRealVar* g2_f1 = tryVar(g2_f1_Name, workInt,debug); 

    TString g2_f2_Name = "g2_f2_frac_"+samplemode;
    RooRealVar* g2_f2 = tryVar(g2_f2_Name, workInt,debug); 

    TString g2_f3_Name; 
    RooRealVar* g2_f3 =NULL; 

    if (lowMass < 5200.0)
      {
        g2_f3_Name = "g2_f3_frac_"+samplemode;
        g2_f3 = tryVar(g2_f3_Name, workInt,debug);
      }

    TString g3_f1_Name = "g3_f1_frac_"+samplemode;
    RooRealVar* g3_f1 = tryVar(g3_f1_Name, workInt,debug); 

    TString g5_f1_Name = "g5_f1_frac_"+samplemode;
    RooRealVar* g5_f1 = tryVar(g5_f1_Name, workInt,debug); 

    // --------------------------------- Read PDFs from Workspace -------------------------------------------------//                                                    

    RooExtendPdf* epdf_Bd2DK = NULL;
    epdf_Bd2DK = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Bd2DK", "", merge, debug);
    Double_t valBd2DK = CheckEvts(workInt, samplemode, "Bd2DK",debug);
    list = AddEPDF(list, epdf_Bd2DK, valBd2DK, debug);

    RooExtendPdf* epdf_Bd2DPi = NULL;
    epdf_Bd2DPi = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Bd2DPi", "", merge, debug);
    Double_t valBd2DPi = CheckEvts(workInt, samplemode, "Bd2DPi",debug);
    list = AddEPDF(list, epdf_Bd2DPi, valBd2DPi, debug);

    RooExtendPdf* epdf_Lb2LcK = NULL;
    epdf_Lb2LcK = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Lb2LcK", "", merge, debug);
    Double_t valLb2LcK = CheckEvts(workInt, samplemode, "Lb2LcK",debug);
    list = AddEPDF(list, epdf_Lb2LcK, valLb2LcK, debug);

    RooExtendPdf* epdf_Lb2LcPi = NULL;
    epdf_Lb2LcPi = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Lb2LcPi", "", merge, debug);
    Double_t valLb2LcPi = CheckEvts(workInt, samplemode, "Lb2LcPi",debug);
    list = AddEPDF(list, epdf_Lb2LcPi, valLb2LcPi, debug);

    
    if (debug == true){
      cout<<"---------------  Group 1 -----------------"<<endl;
      cout<<"Bd->DsK"<<endl;
      if ( lowMass < 5300.0) { cout<<"Bs->DsstK"<<endl; } 
      if ( lowMass < 5250.0) { cout<<"Bs->DsKst"<<endl; }
      if ( lowMass < 5150.0) { cout<<"Bs->DsstKst"<<endl; }
    }

    TString name="";
    RooProdPdf* pdf_Bd2DsK_Tot = NULL;
    RooProdPdf* pdf_Bs2DsKst_Tot = NULL;
    RooProdPdf* pdf_Bs2DsstK_Tot = NULL;
    RooProdPdf* pdf_Bs2DsstKst_Tot = NULL;
    RooAddPdf*  pdf_Bs2DsDsstKKst_Tot = NULL;
    RooArgList* list_g1_bkg = new RooArgList();
    RooArgList* list_g1_frac = new RooArgList();
    RooExtendPdf* epdf_Bs2DsDsstKKst = NULL; 

    if ( valBs2DsDssKKst != 0.0 )
      {
        pdf_Bd2DsK_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bd2DsK", "", merge, debug);
	list_g1_bkg->add(*pdf_Bd2DsK_Tot);
	if ( debug ) { std::cout<<"[INFO] Adding pdf: "<<pdf_Bd2DsK_Tot->GetName()<<" to a list of pdfs for g1."<<std::endl; } 
	if ( lowMass < 5300.0) 
	  { 
	    pdf_Bs2DsstK_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstK", "", merge, debug);
	    list_g1_bkg->add(*pdf_Bs2DsstK_Tot);
	    list_g1_frac->add(*g1_f1); 
	    if ( debug )
	      {
		std::cout<<"[INFO] Adding pdf: "<<pdf_Bs2DsstK_Tot->GetName()<<" to a list of pdfs for g1."<<std::endl;
		std::cout<<"[INFO] Adding frac: "<<g1_f1->GetName()<<" to a list of frac for g1."<<std::endl;
	      }
	  }
	if ( lowMass < 5250.0) 
	  { 
	    pdf_Bs2DsKst_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsKst", "", merge, debug);
	    list_g1_bkg->add(*pdf_Bs2DsKst_Tot);
            list_g1_frac->add(*g1_f2);
	    if ( debug )
              {
		std::cout<<"[INFO] Adding pdf: "<<pdf_Bs2DsKst_Tot->GetName()<<" to a list of pdfs for g1."<<std::endl;
		std::cout<<"[INFO] Adding frac: "<<g1_f2->GetName()<<" to a list of frac for g1."<<std::endl;
              }
	  }
	if ( lowMass < 5150.0) 
	  { 
	    pdf_Bs2DsstKst_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstKst", "", merge, debug);
	    list_g1_bkg->add(*pdf_Bs2DsstKst_Tot);
            list_g1_frac->add(*g1_f3);
	    std::cout<<"[INFO] Adding pdf: "<<pdf_Bs2DsstKst_Tot->GetName()<<" to a list of pdfs for g1."<<std::endl;
	    std::cout<<"[INFO] Adding frac: "<<g1_f3->GetName()<<" to a list of frac for g1."<<std::endl;
	  }
	
	if ( lowMass < 5300.0 )
	  {
	    name="PhysBkgBs2DsDsstKKstPdf_m_"+samplemode+"_Tot";
	    pdf_Bs2DsDsstKKst_Tot = new RooAddPdf(name.Data(), name.Data(), *list_g1_bkg, *list_g1_frac, true); 

	    name = "Bs2DsDsstKKstEPDF_m_"+samplemode;
            epdf_Bs2DsDsstKKst = new RooExtendPdf( name.Data() , pdf_Bs2DsDsstKKst_Tot->GetTitle(), *pdf_Bs2DsDsstKKst_Tot, *nBs2DsDssKKstEvts   );
            CheckPDF( epdf_Bs2DsDsstKKst, debug );
            list = AddEPDF(list, epdf_Bs2DsDsstKKst, nBs2DsDssKKstEvts, debug);
	  }
	else
	  {
	    name = "Bs2DsDsstKKstEPDF_m_"+samplemode;
	    epdf_Bs2DsDsstKKst = new RooExtendPdf( name.Data() , pdf_Bd2DsK_Tot->GetTitle(), *pdf_Bd2DsK_Tot, *nBs2DsDssKKstEvts   );
	    CheckPDF( epdf_Bs2DsDsstKKst, debug );
	    list = AddEPDF(list, epdf_Bs2DsDsstKKst, nBs2DsDssKKstEvts, debug);
	  }
      }

    if (debug == true){
      cout<<"---------------  Group 2 -----------------"<<endl;
      cout<<"Bs->DsPi"<<endl;
      cout<<"Bs->DsstPi"<<endl;
      cout<<"Bs->DsRho"<<endl;
      if ( lowMass < 5200.0) { cout<<"Bs->DsstRho"<<endl; }
    }

    RooProdPdf* pdf_Bs2DsRho_Tot = NULL;
    RooProdPdf* pdf_Bs2DsstRho_Tot = NULL;
    RooProdPdf* pdf_Bs2DsstPi_Tot = NULL;
    RooProdPdf* pdf_Bs2DsPi_Tot = NULL;
    RooAddPdf*  pdf_Bs2DsDsstPiRho_Tot = NULL;

    if ( valBsLb2DsDsstPPiRho != 0.0 )
      {
	pdf_Bs2DsRho_Tot  = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsRho",  "", merge, debug);
	pdf_Bs2DsstPi_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstPi", "", merge, debug);
	pdf_Bs2DsPi_Tot   = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsPi",   "", merge, debug);

	if (lowMass < 5200.0)
	  {
	    pdf_Bs2DsstRho_Tot  = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstRho",  "", merge, debug);
	  }

	name="PhysBkgBs2DsDsstPiRhoPdf_m_"+samplemode+"_Tot";

	if ( lowMass < 5200.0)
	  {
	    pdf_Bs2DsDsstPiRho_Tot = new RooAddPdf(name.Data(), name.Data(),
                                                   RooArgList(*pdf_Bs2DsPi_Tot,*pdf_Bs2DsstPi_Tot, *pdf_Bs2DsRho_Tot,*pdf_Bs2DsstRho_Tot),
                                                   RooArgList(*g2_f1,*g2_f2,*g2_f3), true);
	  }
	else
	  {
	    pdf_Bs2DsDsstPiRho_Tot = new RooAddPdf(name.Data(), name.Data(),
						   RooArgList(*pdf_Bs2DsPi_Tot,*pdf_Bs2DsstPi_Tot, *pdf_Bs2DsRho_Tot),
						   RooArgList(*g2_f1,*g2_f2), true);
	  }
	CheckPDF(pdf_Bs2DsDsstPiRho_Tot, debug);
      }

    RooProdPdf* pdf_Lb2Dsp_Tot = NULL;
    RooProdPdf* pdf_Lb2Dsstp_Tot = NULL;
    RooAddPdf* pdf_Lb2DsDsstP_Tot = NULL;
    RooAddPdf* pdf_BsLb2DsDsstPPiRho_Tot = NULL;
    RooExtendPdf* epdf_BsLb2DsDsstPPiRho   = NULL;

    if ( valBsLb2DsDsstPPiRho != 0.0 )
      {

	pdf_Lb2Dsp_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Lb2Dsp",  "", merge, debug);
        pdf_Lb2Dsstp_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Lb2Dsstp",  "", merge, debug);
	
	name="PhysBkgLb2DsDsstPPdf_m_"+samplemode+"_Tot";
	pdf_Lb2DsDsstP_Tot = new RooAddPdf(name.Data(), name.Data(), *pdf_Lb2Dsp_Tot, *pdf_Lb2Dsstp_Tot, *g3_f1);
	CheckPDF(pdf_Lb2DsDsstP_Tot, debug);

	name="PhysBkgBsLb2DsDsstPPiRhoPdf_m_"+samplemode+"_Tot";
	pdf_BsLb2DsDsstPPiRho_Tot = new RooAddPdf(name.Data(), name.Data(),
						  RooArgList(*pdf_Bs2DsDsstPiRho_Tot, *pdf_Lb2DsDsstP_Tot),
						  RooArgList(*g5_f1));
	CheckPDF(pdf_BsLb2DsDsstPPiRho_Tot, debug);

	name = "BsLb2DsDsstPPiRhoEPDF_m_"+samplemode;
	epdf_BsLb2DsDsstPPiRho = new RooExtendPdf( name.Data() , pdf_BsLb2DsDsstPPiRho_Tot-> GetTitle(),
						   *pdf_BsLb2DsDsstPPiRho_Tot  , *nBsLb2DsDsstPPiRhoEvts   );
	CheckPDF( epdf_BsLb2DsDsstPPiRho, debug );
	list = AddEPDF(list, epdf_BsLb2DsDsstPPiRho, nBsLb2DsDsstPPiRhoEvts, debug);
      }

    //--------------------------------- FULL PDF --------------------------//                                                                                                  
    RooAbsPdf* pdf_totBkg = NULL;
    name = "BkgEPDF_m_"+samplemode;
    pdf_totBkg = new RooAddPdf( name.Data(), name.Data(), *list);
    if (debug == true)
      {
	cout<<endl;
	if( pdf_totBkg != NULL ){ cout<<" ------------- CREATED TOTAL BACKGROUND PDF: SUCCESFULL------------"<<endl; }
	else { cout<<" ---------- CREATED TOTAL BACKGROUND PDF: FAILED ----------------"<<endl;}
      }
    return pdf_totBkg;
    

  }


} // end of namespace

//=============================================================================
