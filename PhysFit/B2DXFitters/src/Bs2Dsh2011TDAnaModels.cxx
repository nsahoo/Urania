// ROOT and RooFit includes
#include "RooFormulaVar.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooExtendPdf.h"
#include "RooEffProd.h"
#include "RooGaussian.h"
#include "RooDecay.h"
#include "RooBDecay.h"
#include "RooCBShape.h"
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooProdPdf.h"
#include "RooGenericPdf.h" 
#include "TFile.h"
#include "TTree.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooHistPdf.h"
#include <string>
#include <vector>
#include <fstream>
#include "RooAbsReal.h"


#include "B2DXFitters/Bs2Dsh2011TDAnaModels.h"
#include "B2DXFitters/BasicMDFitPdf.h" 
#include "B2DXFitters/Bd2DhModels.h" 
#include "B2DXFitters/GeneralUtils.h"
#include "B2DXFitters/RooBinned1DQuinticBase.h"
#include "B2DXFitters/RooIpatia2.h" 
#include "B2DXFitters/RooApollonios.h"

using namespace std;
using namespace GeneralUtils;
using namespace BasicMDFitPdf; 

namespace Bs2Dsh2011TDAnaModels {


  RooAbsPdf* buildComboPIDKPDF(RooWorkspace* work, 
                               RooWorkspace* workInt, 
                               TString samplemode, 
                               TString typemode,
                               TString shapeName,
			       TString varName, 
                               bool debug)
  {
    RooArgList* listPDF = new RooArgList();
    RooArgList* listFrac = new RooArgList();

    RooAbsPdf* pdf_pidk[3];
    RooAbsPdf* pdf = NULL; 
    RooRealVar* fracPIDK1 = NULL;
    RooRealVar* fracPIDK2 = NULL;

    std::vector<bool> shapes; 
 
    Int_t num = 0; 
    
    if ( shapeName.Contains("Pion") || shapeName.Contains("pion")) 
      {num++; shapes.push_back(true); } else { shapes.push_back(false); }
    if ( shapeName.Contains("Kaon") || shapeName.Contains("kaon")) 
      {num++; shapes.push_back(true); } else { shapes.push_back(false); } 
    if ( shapeName.Contains("Proton") || shapeName.Contains("Proton")) 
      {num++; shapes.push_back(true);} else { shapes.push_back(false); } 

    if ( debug ) { 
      std::cout<<"[DEBUG] num: "<<num<<std::endl; 
      for(unsigned int i=0; i<shapes.size(); i ++ )
	{
	  std::cout<<"[DEBUG] shapes: "<<shapes[i]<<std::endl; 
	}
    }

    if (num > 1 )
    {
      TString fracPIDK1Name = typemode+"_"+varName+"_fracPIDK1_"+samplemode;
      fracPIDK1 =tryVar(fracPIDK1Name, workInt, debug);  
    }
    if ( num > 2 )
    {
      TString fracPIDK2Name = typemode+"_"+varName+"_fracPIDK2_"+samplemode;
      fracPIDK2 = tryVar(fracPIDK2Name, workInt, debug); 
    }

    TString m[] = {"CombPi","CombK","CombP"};

    TString y = CheckDataYear(samplemode,debug);
    TString sam = CheckPolarity(samplemode,debug);
    TString mode = CheckDMode(samplemode, debug);
    if ( mode == "" ) { mode == CheckKKPiMode(samplemode,debug);}
    RooRealVar lum;

    for (int i = 0; i<3; i++ )
    {
      if ( shapes[i] == true ) 
	{
	  pdf_pidk[i] =  buildPIDKShapeMDFit(work,samplemode, m[i], "",debug);
	  
	  if ( pdf_pidk[i] != NULL )
	    {
	      std::cout<<"[INFO] Adding pdf: "<<pdf_pidk[i]->GetName()<<" to PIDK PDFs"<<std::endl;
	      listPDF->add(*pdf_pidk[i]);
	    }
	}
    }

    if ( num > 1 ) { listFrac->add(*fracPIDK1); if (debug == true ) { std::cout<<"[INFO] Adding fraction: "<<fracPIDK1->GetName()<<std::endl;} }
    if ( num > 2 ) { listFrac->add(*fracPIDK2); if (debug == true ) { std::cout<<"[INFO] Adding fraction: "<<fracPIDK2->GetName()<<std::endl;}}

    TString pdfName = typemode+"_"+varName+"_PIDKShape_"+samplemode;
    if ( num == 2 )
    {
      pdf = new RooAddPdf( pdfName.Data(), pdfName.Data(), *listPDF, *listFrac);
    }
    else if ( num == 3 )
    {
      pdf = new RooAddPdf( pdfName.Data(), pdfName.Data(), *listPDF, *listFrac, true);
    }
    else
    {
      pdf = pdf_pidk[0]; 
    }
    CheckPDF(pdf, debug);

    return pdf; 
  }

  




  RooAbsPdf* buildShiftedDoubleCrystalBallPDF(RooAbsReal& obs, RooWorkspace* workInt,
                                              TString samplemode, TString typemode, bool debug)
  {

    if ( debug == true ) { std::cout<<"[INFO] --------- build Shifted double Crystal Ball -------- "<<std::endl; }
    
    RooRealVar* mean = NULL;
    RooRealVar* alpha1Var = NULL;
    RooRealVar* alpha2Var =NULL;
    RooRealVar* n1Var = NULL;
    RooRealVar* n2Var =NULL;
    RooRealVar* sigma1Var =NULL;
    RooRealVar* sigma2Var = NULL;
    RooRealVar* fracVar = NULL;
    RooRealVar* shift = NULL; 
    RooRealVar* scale1 = NULL;
    RooRealVar* scale2 = NULL;
    RooFormulaVar* meanShift= NULL;
    RooFormulaVar* sigma1For = NULL;
    RooFormulaVar* sigma2For = NULL;


    TString varName = obs.GetName();

    TString meanName = "Signal_"+varName+"_mean_"+samplemode;
    mean = tryVar(meanName, workInt, debug);
    TString alpha1Name = "Signal_"+varName+"_alpha1_"+samplemode;
    alpha1Var = tryVar(alpha1Name, workInt, debug);
    TString alpha2Name = "Signal_"+varName+"_alpha2_"+samplemode;
    alpha2Var = tryVar(alpha2Name, workInt, debug);
    TString n1Name = "Signal_"+varName+"_n1_"+samplemode;
    n1Var = tryVar(n1Name, workInt, debug);
    TString n2Name = "Signal_"+varName+"_n2_"+samplemode;
    n2Var = tryVar(n2Name, workInt, debug);
    TString sigma1Name = "Signal_"+varName+"_sigma1_"+samplemode;
    sigma1Var = tryVar(sigma1Name, workInt, debug);
    TString sigma2Name = "Signal_"+varName+"_sigma2_"+samplemode;
    sigma2Var = tryVar(sigma2Name, workInt, debug);
    TString fracName = "Signal_"+varName+"_frac_"+samplemode;
    fracVar = tryVar(fracName, workInt, debug);

    TString shiftName = typemode+"_"+varName+"_shift_"+samplemode;
    shift = tryVar(shiftName, workInt, debug);
    TString scale1Name = typemode+"_"+varName+"_scale1_"+samplemode;
    scale1 = tryVar(scale1Name, workInt, debug);
    TString scale2Name = typemode+"_"+varName+"_scale2_"+samplemode;
    scale2 = tryVar(scale2Name, workInt, debug);

    TString meanShiftName = typemode+"_"+varName+"_meanShift_"+samplemode;
    meanShift = new RooFormulaVar(meanShiftName.Data() , meanShiftName.Data(),"@0-@1", RooArgList(*mean,*shift));
    TString sigma1ForName = typemode+"_"+varName+"_sigma1For_"+samplemode;
    sigma1For = new RooFormulaVar(sigma1ForName.Data(), sigma1ForName.Data(),"@0*@1", RooArgList(*sigma1Var,*scale1));
    TString sigma2ForName = typemode+"_"+varName+"_sigma2For_"+samplemode;
    sigma2For = new RooFormulaVar(sigma2ForName.Data(), sigma2ForName.Data(),"@0*@1", RooArgList(*sigma2Var,*scale2));

    RooAbsPdf* pdf = NULL;

    RooCBShape* pdf1 = NULL;
    TString pdf1Name = typemode+"_"+varName+"_shiftedCrystalBall1_"+samplemode;
    RooCBShape* pdf2 = NULL;
    TString pdf2Name = typemode+"_"+varName+"_shiftedCrystalBall2_"+samplemode;

    pdf1 = new RooCBShape( pdf1Name.Data(), pdf1Name.Data(), obs, *meanShift, *sigma1For, *alpha1Var, *n1Var);
    pdf2 = new RooCBShape( pdf2Name.Data(), pdf2Name.Data(), obs, *meanShift, *sigma2For, *alpha2Var, *n2Var);
   
    CheckPDF( pdf1, debug );
    CheckPDF( pdf2, debug);

    TString pdfName = typemode+"_"+varName+"_shiftedCrystalBall_"+samplemode;
    pdf = new RooAddPdf( pdfName.Data(), pdfName.Data(), *pdf1, *pdf2, *fracVar);
    CheckPDF( pdf, debug );

    return pdf; 

  }


  RooAbsPdf* buildGeneralPdfMDFit(RooWorkspace* work, RooWorkspace* workInt,
				  std::vector <TString> bkgs,
				  std::vector <RooAbsReal*> obs,
				  std::vector <TString> types,
				  TString samplemode, 
				  std::vector<TString> merge, 
				  bool debug)
  {
    RooAbsPdf* pdf_totBkg = NULL;
    RooArgList* list = new RooArgList();
    
    for( const TString bkg: bkgs)
      {
	if ( bkg == "bkgs") { continue; } 
	RooExtendPdf* epdf = NULL;
	epdf = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, bkg, "", merge, debug);
	Double_t val = CheckEvts(workInt, samplemode, bkg, debug);
	list = AddEPDF(list, epdf, val, debug);
      }

    TString name = "BkgEPDF_m_"+samplemode;
    pdf_totBkg = new RooAddPdf( name.Data(), name.Data(), *list);
    if (debug == true)
      {
        cout<<endl;
        if( pdf_totBkg != NULL ){ cout<<" ------------- CREATED TOTAL BACKGROUND PDF: SUCCESFULL------------"<<endl; }
        else { cout<<" ---------- CREATED TOTAL BACKGROUND PDF: FAILED ----------------"<<endl;}
      }
    return pdf_totBkg;

  }

  RooExtendPdf* buildExtendPdfMDFit( RooWorkspace* workInt, RooWorkspace* work,
				     std::vector <RooAbsReal*> obs,
				     std::vector <TString> types,
				     TString samplemode, TString typemode,
				     TString typemodeDs,
				     std::vector<TString> merge, 
				     bool debug)
  {
    RooExtendPdf* epdf = NULL;
    RooProdPdf* pdf_Tot = NULL;
 
    TString nName = "n"+typemode+"_"+samplemode+"_Evts";
    if ( typemode.Contains("Signal") ) { nName = "nSig_"+samplemode+"_Evts"; }
    if ( typemode.Contains("Combinatorial") || typemode.Contains("CombBkg") ) { nName = "nCombBkg_"+samplemode+"_Evts"; }
    //   std::cout<<" nName: "<<nName<<" typemode: "<<typemode<<std::endl;
    RooRealVar* nEvts = tryVar(nName, workInt, debug);
    Double_t val = nEvts->getValV();

    if ( val != 0.0 )
      {
	pdf_Tot = buildProdPdfMDFit(workInt, work, obs, types,  
				    samplemode, typemode, typemodeDs, merge, debug);

	TString epdfName = typemode+"EPDF_m_"+samplemode;
	if(typemode.Contains("Signal") ) { epdfName = "SigEPDF_"+samplemode; }
	else if ( typemode == "Combinatorial" || typemode == "CombBkg" ) { epdfName = "CombBkgEPDF_"+samplemode; }
	epdf = new RooExtendPdf(epdfName.Data() , pdf_Tot->GetTitle(), *pdf_Tot, *nEvts );
	CheckPDF(epdf, debug);
      }
    return epdf;

  }

  RooProdPdf* buildProdPdfMDFit( RooWorkspace* workInt, RooWorkspace* work,
				 std::vector <RooAbsReal*> obs,
				 std::vector <TString> types,
				 TString samplemode, TString typemode, 
				 TString typemodeDs,
				 std::vector <TString> merge, 
				 bool debug)
  {

    RooProdPdf* pdf_Tot = NULL;
    TString typetmp = typemode;
    if ( typemode.Contains("Signal") ) { typetmp ="Signal"; } 

    std::vector <TString> modeTypes =  getShapesType(types, obs, typetmp, debug);
    std::vector <RooAbsPdf*> pdfs;
 
    for (unsigned int i = 0; i < obs.size(); i++ )
      {
	std::pair <RooAbsReal*,TString> obs_shape = getObservableAndShape(modeTypes,obs,i);  
	pdfs.push_back(buildMergedPdfMDFit(workInt, work, obs_shape, samplemode, typemode, typemodeDs, merge, debug));	
      }

    RooArgList* list = new RooArgList();
    for ( const RooAbsPdf* pdf: pdfs ) { list->add(*pdf); }
    
    TString name="PhysBkg"+typetmp+"Pdf_m_"+samplemode+"_Tot";
    pdf_Tot = new RooProdPdf(name.Data(), name.Data(), *list);
    CheckPDF( pdf_Tot, debug );
   
    return pdf_Tot; 
  }

  RooAbsPdf* buildMergedPdfMDFit(RooWorkspace* workInt, RooWorkspace* work,
				 std::pair <RooAbsReal*,TString> obs_shape,
				 TString samplemode, TString typemode, 
				 TString typemodeDs,
				 std::vector<TString> merge,
				 bool debug)
  {
    if (debug == true)
      {
	cout<<"[INFO] build merged RooAbsPdf for: "<<typemode<<" "<<typemodeDs<<endl;
	cout<<"[INFO]       build as: "<<obs_shape.second<<" for variable: "<<obs_shape.first->GetName()<<std::endl; 
      }

    std::vector<RooAbsPdf*> pdf_part;
    RooAbsPdf* pdf = NULL;

    TString t = "_";
    TString mode, Mode;
    std::vector<TString> y, sam;
    mode = CheckDMode(samplemode,debug);
    if ( mode == "" ) { mode = CheckKKPiMode(samplemode, debug); }
    Mode = GetModeCapital(mode,debug);

    y = GetDataYear(samplemode, merge, debug);
    sam = GetPolarity(samplemode, merge, debug);

    for(unsigned int i = 0; i < sam.size(); i++ )
      {
	for (unsigned int j =0; j < y.size(); j++ )
	  {
	    TString smp = sam[i]+"_"+mode+"_"+y[j];
	    TString name = obs_shape.first->GetName(); 
	    if ( obs_shape.second  == "RooKeysPdf" ) 
              {
		if ( (typemode.Contains("Combinatorial") || typemode.Contains("CombBkg") ))
		  {
		    RooAbsPdf* pdfTmp = NULL;
		    TString tmp = "CombPi_"+TString(obs_shape.first->GetName());
		    pdfTmp = buildMassPdfSpecBkgMDFit(work, smp, tmp, typemodeDs, false, debug);
		    if ( pdfTmp == NULL ) 
		      { 
			tmp = "CombK_"+TString(obs_shape.first->GetName());  
			pdfTmp = buildMassPdfSpecBkgMDFit(work, smp, tmp, typemodeDs, false, debug);
		      }
		    CheckPDF( pdfTmp, debug);
		    pdf_part.push_back(pdfTmp);
		  }
		else if  ( name == "BeautyMass" )
		  {
		    pdf_part.push_back(buildMassPdfSpecBkgMDFit(work, smp, typemode, typemodeDs, false, debug));
		  }
		else if ( name == "CharmMass")
		  {
		    pdf_part.push_back(buildMassPdfSpecBkgMDFit(work, smp, typemode, typemodeDs, true, debug));
		  }
	      }
	    else if ( obs_shape.second.Contains("ShiftedSignal") )
	      {
		std::cout<<"INFO] I am here"<<std::endl;
		if ( obs_shape.second  == "ShiftedSignalIpatiaJohnsonSU" )
		  {
		    std::cout<<"INFO] I am here 2"<<std::endl;
		    pdf_part.push_back(Bd2DhModels::buildIpatiaPlusJohnsonSUPDF(*obs_shape.first, workInt, samplemode, typemode, true, false, false, true, debug));
		    //pdf_part.push_back(buildShiftedIpatiaJohnsonSUPDF(*obs_shape.first, workInt, samplemode, typemode, debug));
		  }
		else
		  {
		    pdf_part.push_back(buildShiftedDoubleCrystalBallPDF(*obs_shape.first, workInt, samplemode, typemode, debug));
		  }
	      }
	    else if ( obs_shape.second  == "Signal" )
	      {
		RooAbsPdf* pdfTmp = NULL;
		pdfTmp = trySignal(smp,obs_shape.first->GetName(),workInt, debug);
		if ( pdfTmp == NULL ) { pdfTmp = trySignal(samplemode,obs_shape.first->GetName(),workInt, debug); }
		CheckPDF( pdfTmp, debug);
		pdf_part.push_back(pdfTmp);
	      }
	    else if ( name == "BacPIDK" && (typemode == "Combinatorial" || typemode == "CombBkg") ) 
	      {
		RooAbsPdf* pdfTmp = NULL;
		pdfTmp = buildComboPIDKPDF(work, workInt,smp, typemode, obs_shape.second, obs_shape.first->GetName(), debug);
		CheckPDF( pdfTmp, debug);
                pdf_part.push_back(pdfTmp);
	      }
	    else if ( name == "BacPIDK" || obs_shape.second.Contains("PIDK") )
	      {
		RooAbsPdf* pdfTmp = NULL;
		TString tmp = typemode; 
		if ( typemode.Contains("Signal") ) {tmp = getSignalDecay(typemode); } 
                pdfTmp = buildPIDKShapeMDFit(work, smp, tmp, typemodeDs, false);
		if( pdfTmp == NULL ) { pdfTmp =  buildPIDKShapeMDFit(work, smp, tmp, mode, false); }
		if( pdfTmp == NULL ) { pdfTmp =  buildPIDKShapeMDFit(work, smp, tmp, "", false);}

		if ( pdfTmp != NULL && typemode.Contains("Signal") )
		  {
		    TString newName = "Signal_"+TString(obs_shape.first->GetName())+"_PIDKShape_"+smp; 
		    pdfTmp->SetName(newName.Data()); 
		  }
		CheckPDF( pdfTmp, debug);
		pdf_part.push_back(pdfTmp);
	      }
	    else 
	      {
		pdf_part.push_back(buildAnalyticalShape(*obs_shape.first, workInt, smp, typemode, obs_shape.second, debug));
	      }
	  }
      }
    
    if  ( CheckMerge(merge,"pol" ) ) { pdf_part = mergePdf(workInt, pdf_part, y, sam, "both", debug); }

    if  ( samplemode.Contains("run1") || CheckMerge(merge,"runs")) 
      { 
	std::vector<TString> years_to_merge = GetRunYears(y,"run1");
	pdf_part = mergePdf(workInt, pdf_part, y, years_to_merge, "run1", debug); 
      }
    if  ( samplemode.Contains("run2") || CheckMerge(merge,"runs") )  
      { 
	std::vector<TString> years_to_merge = GetRunYears(y,"run2");
	pdf_part = mergePdf(workInt, pdf_part, y, years_to_merge, "run2", debug); 
      }
    if  ( samplemode.Contains("runs") ) 
      { 
        std::vector<TString> years_to_merge{"run1","run2"};
	pdf_part = mergePdf(workInt, pdf_part, y, years_to_merge, "runs", debug); 
      }

    pdf = pdf_part[0]; 
    
    CheckPDF( pdf, debug );
    if (typemode.Contains("Signal") ) { workInt->import(*pdf); }  
    return pdf;
    
  }


  std::vector<RooAbsPdf*> mergePdf( RooWorkspace* workInt, std::vector<RooAbsPdf*> pdfs, 
				    std::vector<TString> lum, 
				    std::vector<TString> check, 
				    TString outName, bool debug)
  {
    std::vector <RooAbsPdf*> pdfOut;
    std::vector <RooAbsPdf*> init_pdf;
    TString name;
    std::vector<TString> init;
    std::vector<TString> used;
    
    if ( check.size() > 0 )
      {
        for(unsigned int i = 0; i<pdfs.size(); i++)
          {
            TString name = pdfs[i]->GetName();
            if( name.Contains(check[0]) )
              {
                init.push_back(name);
                used.push_back(name);
		init_pdf.push_back(pdfs[i]); 
              }
          }
      }
    
    if ( init.size()> 0 )
      {
	for(unsigned int i = 0; i<init.size(); i++ )
	  {
	    
	    RooArgList* listPDF = new RooArgList();
	    RooArgList* listFrac = new RooArgList();
	    
	    std::vector<TString> types;
	    std::vector<RooRealVar*> frac; 
	    TString copy = init[i];
	    TString pdfName = init[i];
	    pdfName = pdfName.ReplaceAll(check[0],outName);
	    if( debug ) std::cout<<"[INFO] Initial pdf: "<<copy<<std::endl;
	    listPDF->add(*init_pdf[i]); 
	    TString fracName = "";
	    TString checkyear = CheckDataYear(copy,false);

	    if ( check[0] == "up" ) { fracName = "lumRatio_pol_"+checkyear; }
	    else if ( check[0].Contains("20") ){ fracName = "lumRatio_run_"+checkyear; }
	    else if ( check[0].Contains("run") ) { fracName = "lumRatio_runs";}
	    frac.push_back(GetObservable(workInt, fracName, false));
	    Int_t size = frac.size(); 
	    listFrac->add(*frac[size-1]);
	    
	    for( unsigned int j = 1; j < check.size(); j++)
	      {
		TString name = copy.ReplaceAll(check[j-1],check[j]);
		for( unsigned k = 0; k <pdfs.size(); k++ )
		  {
		    TString pdfName = pdfs[k]->GetName();
		    if ( name == pdfName )
		      {
			listPDF->add(*pdfs[k]);
			used.push_back(name);

			if ( debug ) std::cout<<"[INFO]    Adding pdf: "<<name<<std::endl;
			
			if ( j < check.size()-1 )
			  {
			    TString checkyear = CheckDataYear(check[j],false);
			    if ( check[j] == "up" ) { fracName = "lumRatio_pol_"+checkyear; }
			    else if ( check[j].Contains("20") ){ fracName = "lumRatio_run_"+checkyear; }
			    else if ( check[j].Contains("run") ) { fracName = "lumRatio_runs";}
			    frac.push_back(GetObservable(workInt, fracName, false));
			    Int_t size = frac.size();
			    listFrac->add(*frac[size-1]);
			  }
	
		      }
		  }
	      }
	    if ( debug )
	      {
		std::cout<<"[INFO] Final pdf name: "<<pdfName<<std::endl;
		std::cout<<"[INFO] Fractions: "<<std::endl; 
		for(auto& f:frac){ std::cout<<"[INFO]      "<<f->getValV()<<std::endl;}
	      }
	    
	    pdfOut.push_back(new RooAddPdf( pdfName.Data(), pdfName.Data(), *listPDF, *listFrac));
	  }
      
	for(unsigned int i = 0; i<pdfs.size(); i++)
	  {
	    bool usedbool = false;
	    TString pdfName = pdfs[i]->GetName();
	    for( unsigned int j = 0; j<used.size(); j++ )
	      {
		if( pdfName == used[j] ) { usedbool = true; break; }
	      }
	    if( usedbool == false )
	      {
		pdfOut.push_back(pdfs[i]);
	      }
	  }
	    
      } 
    
    else 
      {
	if ( debug ) { std::cout<<"[INFO] Nothing to do for merging. Checking for duplications. "<<std::endl;}

	for( unsigned int i = 0; i < pdfs.size(); i++)
	  {
	    init.push_back(TString(pdfs[i]->GetName())); 
	  }
	used = init; 
	for(unsigned int i = 0; i<init.size();i++)
	  {
	    for(unsigned int j = 0; j<used.size(); j++)
	      {
		if ( i != j && init[i] == used[j] && i < j ) 
		  { 
		    used.erase(used.begin()+j); 
		  } 
	      }
	  }
	
	for( auto& name : used ) 
	  {
	    for( auto& pdf : pdfs )
	      {
		if ( pdf->GetName() == name ) { pdfOut.push_back(pdf); break; } 
	      }
	  }
	
      }
    if ( debug ) 
      {
	std::cout<<"[INFO] New pdfs: "<<std::endl;
        for( unsigned int i =0; i<pdfOut.size(); i++ )
          {
	    std::cout<<"[INFO]   "<<pdfOut[i]->GetName()<<std::endl;
          }
      }
    return pdfOut;


  }

  RooAbsPdf* buildMassPdfSpecBkgMDFit(RooWorkspace* work,
                                      TString samplemode, TString typemode, TString typemodeDs,
                                      //std::vector <TString> types,
				      bool charmShape, bool debug)
  {

    
    TString p = CheckPolarity(samplemode,false);
    TString y = CheckDataYear(samplemode,false);

    RooAbsPdf* pdf_Mass = NULL;
    TString name = "";
    TString Ds = "";
    if (charmShape == true ) { Ds = "_Ds"; }

    if ( typemodeDs != "" )
    {
      name = "PhysBkg"+typemode+"Pdf_m_"+p+"_"+typemodeDs+"_"+y+Ds;	 
    }
    else
    {
      name = "PhysBkg"+typemode+"Pdf_m_both_"+y+Ds;
    }

    pdf_Mass = tryPdf(name, work, debug);
    CheckPDF( pdf_Mass, debug);
    return pdf_Mass;
    
  }


  RooAbsPdf* buildPIDKShapeMDFit(RooWorkspace* work,
                                 TString samplemode, TString typemode, TString typemodeDs,
                                 bool debug)
  {
    TString p = CheckPolarity(samplemode,false);
    TString y = CheckDataYear(samplemode,false);

    RooAbsPdf* pdf_PIDK = NULL;
    TString name = "";

    if ( typemodeDs != "" )
    {
      name = "PIDKShape_"+typemode+"_"+p+"_"+typemodeDs+"_"+y;
    }
    else
    {
      name = "PIDKShape_"+typemode+"_"+p+"_"+y;
    }

    pdf_PIDK = tryPdf(name, work, debug);
    CheckPDF( pdf_PIDK, debug);
    return pdf_PIDK;

  }

  //===============================================================================
  // Load RooKeysPdf from workspace.
  //===============================================================================

  RooKeysPdf* GetRooKeysPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug) {

    RooKeysPdf* pdf = NULL;
    pdf = (RooKeysPdf*)work->pdf(name.Data());
    if (debug == true) {if( pdf != NULL ){ cout<<"[INFO] Read "<<pdf->GetName()<<endl;} else { cout<<"Cannot read PDF"<<endl;}}
    return pdf;

  }

  //===============================================================================
  // Load RooHistPdf from workspace.
  //===============================================================================

  RooHistPdf* GetRooHistPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug) {

    RooHistPdf* pdf = NULL;
    pdf = (RooHistPdf*)work->pdf(name.Data());
    if (debug == true) {if( pdf != NULL ){ cout<<"[INFO] Read "<<pdf->GetName()<<endl;} else { cout<<"Cannot read PDF"<<endl;}}
    return pdf;

  }

  //===============================================================================
  // Load RooAddPdf from workspace.
  //===============================================================================

  RooAddPdf* GetRooAddPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug) {

    RooAddPdf* pdf = NULL;
    pdf = (RooAddPdf*)work->pdf(name.Data());
    if (debug == true) {if( pdf != NULL ){ cout<<"[INFO] Read "<<pdf->GetName()<<endl;} else { cout<<"Cannot read PDF"<<endl;}}
    return pdf;

  }

  //===============================================================================
  // Load RooBinned1DPdf from workspace.
  //===============================================================================

  RooAbsPdf* GetRooBinned1DFromWorkspace(RooWorkspace* work, TString& name, bool debug)
  {
    RooBinned1DQuinticBase<RooAbsPdf>* pdf = NULL;
    pdf = (RooBinned1DQuinticBase<RooAbsPdf>*)work->pdf(name.Data());
    RooAbsPdf* pdf2 = pdf;
    if (debug == true) {if( pdf2 != NULL ){ cout<<"[INFO] Read "<<pdf2->GetName()<<endl;} else { cout<<"Cannot read PDF"<<endl;}}
    return pdf2;
  }

  //===============================================================================
  // Load RooAbsPdf from workspace.
  //===============================================================================

  RooAbsPdf* GetRooAbsPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug)
  {
    RooAbsPdf* pdf = NULL;
    pdf = (RooAbsPdf*)work->pdf(name.Data());
    if (debug == true) {if( pdf != NULL ){ cout<<"[INFO] Read "<<pdf->GetName()<<endl;} else { cout<<"Cannot read PDF"<<endl;}}
    return pdf;
  }

  Double_t  CheckEvts( RooWorkspace* workInt, TString samplemode, TString typemode, bool debug)
  {
 
    Double_t val = 0.0; 
    TString nName = "n"+typemode+"_"+samplemode+"_Evts";
    RooRealVar* nEvts = tryVar(nName, workInt, debug);
    val = nEvts->getValV();
   
    if ( debug == true  ) { std::cout<<"[INFO] check number of events: "<<nName<<" value: "<<val<<std::endl; } 
    return val; 
  }
    

  RooArgList* AddEPDF(RooArgList* list, RooExtendPdf* pdf, RooRealVar *numEvts, bool debug)
  {
    Double_t ev = numEvts->getValV();
    if ( ev != 0.0 )
    {
      list->add(*pdf);
      if (debug == true )
      {
        std::cout<<"[INFO] "<<pdf->GetName()<<" added to pdf list with inital number of events:"<<ev<<std::endl; 
      }
    }
    else
    {
      if (debug == true )
      {
        std::cout<<"[INFO] "<<pdf->GetName()<<" NOT added to pdf list, number of events:"<<ev<<std::endl;
      }
    }
    return list; 
  }

  RooArgList* AddEPDF(RooArgList* list, RooExtendPdf* pdf, Double_t ev, bool debug)
  {
    if ( ev != 0.0 )
    {
      list->add(*pdf);
      if (debug == true )
      {
        std::cout<<"[INFO] "<<pdf->GetName()<<" added to pdf list with inital number of events:"<<ev<<std::endl;
      }
    }
    else
    {
      if (debug == true )
      {
        std::cout<<"[INFO] PDF NOT added to pdf list, number of events:"<<ev<<std::endl;
      }
    }
    return list;
  }


  RooAbsPdf* mergePdf(RooAbsPdf* pdf1, RooAbsPdf* pdf2, TString merge, TString lum,RooWorkspace* workInt, bool debug)
  {

    if ( pdf1->GetName() == pdf2->GetName() ) 
    {
      if(debug == true ) { std::cout<<"[INFO] Pdfs the same: "<<pdf1->GetName()<<" = "<<pdf2->GetName()<<". Nothing done."<<std::endl; }
      return pdf1; 
    }
    TString lumRatioName = "lumRatio_"+lum;
    RooRealVar* lumRatio = GetObservable(workInt, lumRatioName, false);
    RooAbsPdf* pdf = NULL; 
    
    TString name = pdf1->GetName();
    
    
    if ( merge == "pol" )
    {
      if ( name.Contains("down") ) { name.ReplaceAll("down","both"); }
      else if ( name.Contains("up") ) { name.ReplaceAll("up","both"); }
    }
    else if ( merge == "year" ) 
    {
      if ( name.Contains("2011") ) { name.ReplaceAll("2011","run1");}
      else if ( name.Contains("2012") ){ name.ReplaceAll("2012","run1");}
    }
    name = name + "_" + merge; 
    pdf = new RooAddPdf( name.Data(), name.Data(),RooArgList(*pdf1,*pdf2), RooArgList(*lumRatio));

    if ( debug == true )
    {
      std::cout<<"[INFO] Adding "<<pdf1->GetName()<<" to "<<pdf2->GetName()<<" = "<<pdf->GetName()<<" with fraction: "<<lumRatio->getValV()<<std::endl; 
    }
    return pdf; 
  }

  RooAbsPdf* tryPdf(TString name, RooWorkspace* work, bool debug )
  {
    TString p = CheckPolarity(name,false);
    TString y = CheckDataYear(name,false);
    TString m = CheckDMode(name,false);
    if ( m == "") { m = CheckKKPiMode(name, false);}
    TString h = CheckHypo(name,false);
    TString t = "_";

    TString name_prev = name;
    TString name2 = name;

    RooAbsPdf* pdf = (RooKeysPdf*)work->pdf(name.Data()); 

    TString yy[] = {y,"run1"};
    TString mm[] = {m,"all","kkpi"};
    TString pp[] = {p,"both"};
    TString hh[] = {h,"Bd2DPi","Bd2DK","Bs2DsK","Bs2DsPi"};
    int s = 3;
    if ( m == "pipipi" || m == "kpipi" )
    {
      s = 2;
    }
    
    if ( pdf == NULL )
    {
      for( int i = 0; i<2; i++)
      {

        for (int j = 0; j<s; j++ )
        {
          for (int k =0; k<2; k++ )
          {
            for (int l=0; l<5; l++)
            {  
              TString nName = name_prev;
              //std::cout<<"prev: "<<name_prev<<std::endl;                                                                                                                          
              nName = nName.ReplaceAll(y,yy[i]);
              nName = nName.ReplaceAll(m,mm[j]);
              nName = nName.ReplaceAll(p,pp[k]);
              if(h!=""){nName = nName.ReplaceAll(h,hh[l]);}
              pdf = (RooKeysPdf*)work->pdf(nName.Data());
              //std::cout<<"name: "<<nName<<std::endl;
              if ( pdf != NULL ) { break; }
            }            
            if ( pdf != NULL ) { break; }
          }
          if ( pdf != NULL ) { break; }
        }
        if ( pdf != NULL ) { break; }
      }
    }

    if( pdf != NULL ){ if ( debug == true) {std::cout<<"[INFO] Read pdf: "<<pdf->GetName()<<std::endl;} return pdf; }
    else{ std::cout<<"[ERROR] Cannot read pdf."<<std::endl; return NULL;}

  }


  TString findRooKeysPdf(std::vector <std::vector <TString> > pdfNames, TString var, TString smp, bool debug)
  {
    TString pdfName = "";

    TString p = CheckPolarity(smp,false);
    TString y = CheckDataYear(smp,false);
    TString m = CheckDMode(smp,false);
    if ( m == "") { m = CheckKKPiMode(smp, false);}
    TString h = CheckHypo(smp,false);
    TString t = "_";

    int pdfID = -1.0; 


    for (unsigned int g = 0; g<pdfNames.size(); g++ )
    {
      if(h!="")
      {   
        if ( pdfNames[g][1].Contains(m)  == true  && pdfNames[g][1].Contains(y) == true     
             && pdfNames[g][1].Contains(p)  == true && pdfNames[g][1].Contains(var)== true && pdfNames[g][1].Contains(h) == true)
        {
          pdfID =g; 
          break;
        }
      }
      else
      {
        if ( pdfNames[g][1].Contains(m)  == true  && pdfNames[g][1].Contains(y) == true
             && pdfNames[g][1].Contains(p)  == true && pdfNames[g][1].Contains(var)== true)
        {
          pdfID =g;
          break; 
        } 
      }
    }
    
    if ( pdfID == -1 )
    {
      TString yy[] = {y,"run1"};
      TString mm[] = {m,"all","kkpi"};
      TString pp[] = {p,"both"};
      TString hh[] = {h,"Bd2DPi","Bd2DK","Bs2DsK","Bs2DsPi"};
      int s = 3;
      if ( m == "pipipi" || m == "kpipi" )
      {
        s = 2;
      }

      for (unsigned int g = 0; g<pdfNames.size(); g++ )
      {
        for( int i = 0; i<2; i++)
	      {
          for (int j = 0; j<s; j++ )
          {
            for (int k =0; k<2; k++ )
            {
              for (int l =0; l<5; l++ )
              { 
                if(h!="")
                { 
                  if ( pdfNames[g][1].Contains(mm[j])  == true  && pdfNames[g][1].Contains(yy[i]) == true
                       && pdfNames[g][1].Contains(pp[k])  == true && pdfNames[g][1].Contains(var)  == true && pdfNames[g][1].Contains(hh[l]) == true)
                  {
                    pdfID =g;
                    break;
                  }
                }
                else
                {
                  if ( pdfNames[g][1].Contains(mm[j])  == true  && pdfNames[g][1].Contains(yy[i]) == true
                       && pdfNames[g][1].Contains(pp[k])  == true && pdfNames[g][1].Contains(var)  == true)
                  {
                    pdfID =g;
                    break; 
                  }
                }
                if ( pdfID>-1 ) { break; }
              }
              if ( pdfID>-1 ) { break; }
            }
            if ( pdfID>-1 ) { break; }
          }
          if ( pdfID>-1 ) { break; }
        }
      }
    }

    if ( pdfID > -1) 
    {
      if ( debug == true ) { std::cout<<"[INFO] Found PDF with pdfId: "<<pdfID<<" and name: "<<pdfNames[pdfID][0]<<std::endl;}
      pdfName = pdfNames[pdfID][0];
    }
    
    return pdfName; 
  }
  
  //---------------------------------------
  // Get Shape type, default is RooKeysPdf
  //---------------------------------------
  TString getShapeType(std::vector <TString> types, const TString var, const TString typemode)
  {
    TString type = "RooKeysPdf"; 
    TString find = typemode+"_"+var; 

    for (const TString& name : types)
      {
	if ( name.Contains(typemode) )
	  {
	    if ( name.Contains("ShiftedSignal") && typemode.Contains("Signal") ){ continue; }  
	    if ( name.Contains("PlusSignal") && typemode.Contains("Signal") ){ continue; }
	    if ( name.Contains(var))
	      {
		type = name; 
		break;
	      }
	    else if ( var.Contains("PIDK") ) 
	      {
		type = "PIDKShape"; 
	      }
	    else
	      {
		type = "RooKeysPdf"; 
	      }
	  }
      }
	  
    find = find+"_";
    type.ReplaceAll(find,""); 
    
    return type; 
  }
  //---------------------------------------
  // Get list of shapes for all dimensions
  //---------------------------------------
  std::vector<TString> getShapesType(std::vector <TString> types, std::vector <TString> vars, const TString typemode, bool debug )
  {
    std::vector<TString> outTypes;

    for ( const TString& var: vars )
      {
	outTypes.push_back( getShapeType(types, var, typemode) ); 
      }

    if (debug)
      {
	std::cout<<"[INFO] For the mode: "<<typemode<<" following PDFs are chosen: "<<std::endl; 
	for ( unsigned int i = 0; i < vars.size(); i++) 
	  {
	    std::cout<<"[INFO]      "<<vars[i]<<" PDF model: "<<outTypes[i]<<std::endl; 
	  }
      }
    return outTypes; 
  }
  
  //---------------------------------------                                                                                                                      
  // Get list of shapes for all dimensions                                                                                                                       
  //---------------------------------------                                                                                                                             
  std::vector<TString> getShapesType(std::vector <TString> types, std::vector <RooAbsReal*> vars, const TString typemode, bool debug )
  {
    std::vector<TString> obsName; 
    for ( const  RooAbsReal* var: vars )
      {
	obsName.push_back(var->GetName());
      }
    
    std::vector <TString> outNames = getShapesType(types, obsName, typemode, debug );
    
    return outNames; 
  }

  std::pair <RooAbsReal*, TString> getObservableAndShape(std::vector <TString> types, std::vector <RooAbsReal*> vars,Int_t i)
  {
    std::pair <RooAbsReal*, TString> pair; 
    
    pair.first = vars[i];
    pair.second = types[i]; 

    return pair; 
  }
}
