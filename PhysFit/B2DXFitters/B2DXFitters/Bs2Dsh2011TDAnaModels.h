#ifndef Bs2Dsh2011TDAnaModels_H
#define Bs2Dsh2011TDAnaModels_H 1


#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooResolutionModel.h"
#include "RooWorkspace.h"
#include "RooAddPdf.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooAbsReal.h"

#include <vector> 

namespace Bs2Dsh2011TDAnaModels {
  
  RooAbsPdf* buildShiftedDoubleCrystalBallPDF(RooAbsReal& mass, RooWorkspace* workInt,
					      TString samplemode, TString typemode, bool debug = false);


  RooAbsPdf* buildGeneralPdfMDFit(RooWorkspace* workInt, RooWorkspace* work,
				  std::vector <TString> bkg, 
				  std::vector <RooAbsReal*> obs,
				  std::vector <TString> types,
				  TString samplemode, 
				  std::vector<TString> merge,
				  bool debug =false);


  RooExtendPdf* buildExtendPdfMDFit( RooWorkspace* workInt, RooWorkspace* work,
				     std::vector <RooAbsReal*> obs,
                                     std::vector <TString> types,
				     TString samplemode, TString typemode, 
				     TString typemodeDs,
				     std::vector<TString> merge,
                                     bool debug =false);

  RooProdPdf* buildProdPdfMDFit( RooWorkspace* workInt, RooWorkspace* work,
				 std::vector <RooAbsReal*> obs,
				 std::vector <TString> types,
				 TString samplemode, TString typemode, 
				 TString typemodeDs,
                                 std::vector<TString> merge,
				 bool debug=false);

  RooAbsPdf* buildMergedPdfMDFit(RooWorkspace* workInt, RooWorkspace* work,
                                 std::pair <RooAbsReal*,TString> obs_shape,
                                 TString samplemode, TString typemode,  
				 TString typemodeDs,
				 std::vector<TString> merge,
				 bool debug);

  RooAbsPdf* buildMassPdfSpecBkgMDFit(RooWorkspace* work,
				      TString samplemode, TString typemode, TString typemodeDs = "",
				      bool charmShape = false, bool debug = false);

  RooAbsPdf* buildPIDKShapeMDFit(RooWorkspace* work,
				 TString samplemode, TString typemode, TString typemodeDs = "",
				 bool debug = false);

  RooAbsPdf* buildComboPIDKPDF(RooWorkspace* work, RooWorkspace* workInt,
                               TString samplemode, TString typemode, TString shapes, TString varName, bool debug = false);

  //===============================================================================
  // Load RooKeysPdf from workspace.
  //===============================================================================
  RooKeysPdf* GetRooKeysPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug = false);
  
  //===============================================================================
  // Load RooHistPdf from workspace.
  //===============================================================================
  RooHistPdf* GetRooHistPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug = false);

  //===============================================================================
  // Load RooAddPdf from workspace.
  //===============================================================================
  RooAddPdf* GetRooAddPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug=false);

  //===============================================================================
  // Load RooBinned1DPdf from workspace.
  //===============================================================================
  RooAbsPdf* GetRooBinned1DFromWorkspace(RooWorkspace* work, TString& name, bool  debug = false);
  
  //===============================================================================
  // Load RooAbsPdf from workspace.
  //===============================================================================
  RooAbsPdf* GetRooAbsPdfFromWorkspace(RooWorkspace* work, TString& name, bool debug  = false );

  Double_t  CheckEvts( RooWorkspace* workInt, TString samplemode, TString typemode, bool debug = false);

  RooArgList* AddEPDF(RooArgList* list, RooExtendPdf* pdf, RooRealVar *numEvts, bool debug = false); 
  RooArgList* AddEPDF(RooArgList* list, RooExtendPdf* pdf, Double_t ev, bool debug = false);

  
  RooAbsPdf* mergePdf(RooAbsPdf* pdf1, RooAbsPdf* pdf2, TString merge, TString lum,RooWorkspace* workInt, bool debug = false);
  std::vector<RooAbsPdf*> mergePdf( RooWorkspace* workInt, std::vector<RooAbsPdf*> pdfs,
                                    std::vector<TString> lum,
                                    std::vector<TString> check,
                                    TString outName, bool debug);
  


  RooAbsPdf* tryPdf(TString name, RooWorkspace* workInt, bool debug );

  TString findRooKeysPdf(std::vector <std::vector <TString> > pdfNames, TString var, TString smp, bool debug);
  TString getShapeType(std::vector <TString> types, const TString var, const TString typemode); 
  std::vector<TString> getShapesType(std::vector <TString> types, std::vector <TString> vars, const TString typemode, bool debug = false);
  std::vector<TString> getShapesType(std::vector <TString> types, std::vector <RooAbsReal*> vars, const TString typemode, bool debug );
  std::pair <RooAbsReal*, TString> getObservableAndShape(std::vector <TString> types, std::vector <RooAbsReal*> vars,Int_t i);

}

#endif
