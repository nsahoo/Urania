# When preparing a release of Urania:
  - ```cd Math```
  - ```git pull```
# Math/SomeUtils is submoduled from lhcb/Math in this way:
  - ```git submodule add ssh://git@gitlab.cern.ch:7999/lhcb/Math.git```

# Old instructions (to be updated):
  - Instructions for maintainers
  - Carlos Vazquez Sierra (Mar, 2017)
  - Fetch PhysFit/P2VV from lhcb-b2cc/P2VV:
    + Replace CMakeLists.txt, cmt/requirements and doc/release.notes,
    + Update versions.

