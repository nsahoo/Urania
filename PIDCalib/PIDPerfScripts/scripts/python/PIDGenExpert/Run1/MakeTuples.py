import os, sys, math
from ROOT import TFile, TNtuple

from Config import *

def convert_single_file(infile, f2, nt2, treename, pidvar, ptvar, etavar, ntracksvar, transform, limits, selection = None, pol_cut = "") :
  f1 = TFile(infile)
  nt1 = f1.Get(treename)
  f2.cd()
  nentries = nt1.GetEntries()
  print "tuple %s: %d entries" % (treename, nentries )
  n = 0
  x_code = compile("i.%s" % pidvar, '<string>', 'eval')
  pid_code = compile(transform, '<string>', 'eval')
  pt_code = compile("math.log(i.%s)" % ptvar, '<string>', 'eval')
  eta_code = compile("i.%s" % etavar, '<string>', 'eval')
  if selection : 
    selection_code = compile(selection, '<string>', 'eval')
  if ntracksvar : 
    ntracks_code = compile("math.log(i.%s)" % ntracksvar, '<string>', 'eval')
  if pol_cut : 
    pol_code = compile("%s" % pol_cut, '<string>', 'eval')
  for i in nt1 :
      n += 1
      if (n % 10000 == 0) : 
        print "  event %d/%d" % (n, nentries)
      x = eval(x_code)
      if x < limits[0] or x > limits[1] : continue
      if selection : 
        if not eval(selection_code) : continue
      if pol_cut :
        if not eval(pol_code) : continue
#      if pol ==  1 and i.Charge_idx == 1 : continue  # Charge_idx = 0 for positive and 1 for negative tracks
#      if pol == -1 and i.Charge_idx == 0 : continue
      pid = eval(pid_code)
      pt  = eval(pt_code)
      eta = eval(eta_code)
      ntracks = eval(ntracks_code)
      nt2.Fill(pid, pt, eta, ntracks, i.Charge_idx, i.nsig_sw)
  f1.Close()

def convert_calo_file(infile, f2, nt2, treename, pidvar, ptvar, etavar, transform) :
  f1 = TFile(infile)
  nt1 = f1.Get(treename)
  f2.cd()
  nentries = nt1.GetEntries()
  print "tuple %s: %d entries" % (treename, nentries )
  n = 0
  x_code = compile("i.%s" % pidvar, '<string>', 'eval')
  pid_code = compile(transform, '<string>', 'eval')
  pt_code = compile("math.log(i.%s)" % ptvar, '<string>', 'eval')
  eta_code = compile("i.%s" % etavar, '<string>', 'eval')
  for i in nt1 :
      n += 1
      if (n % 10000 == 0) : 
        print "  event %d/%d" % (n, nentries)
      x = eval(x_code)
      pid = eval(pid_code)
      pt  = eval(pt_code)
      eta = eval(eta_code)
      nt2.Fill(pid, pt, eta, i.nkstgWeight)
  f1.Close()

if len(sys.argv)>1 : 
  # config name, e.g. "p_ProbNNp"
  configname = sys.argv[1]
else : 
  print "Usage: MakeTuples [config] [option1:option2:...] [dataset1:dataset2:]"
  print "  configs are: "
  for i in sorted(configs.keys()) : 
    print "    ", i
  print "  options are: "
  print "    test     - run for just a single PIDCalib file rather than whole dataset"
  print "    polarity - create separate datasets for positive and negative track polarities"
  print "    brem     - create separate datasets for HasBremAdded=0 and 1 electrons"
  print "  datasets can be, e.g. "
  print "    MagDown_2012:MadUp_2011"
  print "    or leave empty to process all available datasets"
  sys.exit(0)

config = configs[configname]
# sample name, e.g. "p" for proton
samplename = config['sample']
sample = samples[samplename]
datasets = sample['datasets']
dslist = datasets.keys()
reco = sample['reco']
options = ""
if len(sys.argv)>2 : options = sys.argv[2].split(":")
if len(sys.argv)>3 : dslist = sys.argv[3].split(":")

# resampled variable name
var = configname

# other variable names
pidvar = config['var']
ptvar = sample['pt']
etavar = sample['eta']
ntracksvar = sample['ntracks']
treename = sample['tree']
transform = config['transform_forward']

cut = None
if "cut" in config.keys() : cut = config["cut"]

limits = (0., 1.)
if 'limits' in config : 
  limits = config['limits']

# Create dictionary with the lists of PIDCalib datasets for each year and magnet polarity
dsdict = {}
for ds in dslist : 
  if not isinstance(datasets[ds], tuple) : 
    dsdict[ds] = [ datasets[ds] ] 
  else : 
    dsdict[ds] = []
    n = datasets[ds][1]
    if "test" in options : n = 1
    for i in range(0, n) : 
      dsdict[ds] += [ datasets[ds][0] % i ]
    if "test" in options : break

os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select mkdir -p %s/%s" % (eosdir, configname))

# Loop over PIDCalib datasets
for pol,dss in dsdict.iteritems() : 

  if "polarity" in options : 
    calibfile_p = pol + "_P.root"
    f2p = TFile.Open(calibfile_p,"RECREATE")
    nt2p = TNtuple("pid","pid", "%s:Pt:Eta:Ntracks:Charge:w" % var)
    calibfile_m = pol + "_M.root"
    f2m = TFile.Open(calibfile_m,"RECREATE")
    nt2m = TNtuple("pid","pid", "%s:Pt:Eta:Ntracks:Charge:w" % var)
  elif "brem" in options : 
    calibfile_nb = pol + "_NoBrem.root"
    f2nb = TFile.Open(calibfile_nb,"RECREATE")
    nt2nb = TNtuple("pid","pid", "%s:Pt:Eta:Ntracks:Charge:w" % var)
    calibfile_b = pol + "_Brem.root"
    f2b = TFile.Open(calibfile_b,"RECREATE")
    nt2b = TNtuple("pid","pid", "%s:Pt:Eta:Ntracks:Charge:w" % var)
  else : 
    calibfile = pol + ".root"
    f2 = TFile.Open(calibfile,"RECREATE")
    if reco : 
      nt2 = TNtuple("pid","pid", "%s:Pt:Eta:Ntracks:Charge:w" % var)
    else : 
      nt2 = TNtuple("pid","pid", "%s:Pt:Eta:w" % var)

  print "Polarity " + pol

  for ds in dss : 
    if reco :   # PIDCalib samples need to be converted to TTree first
#      loc = "${CALIBDATAURLPROTOCOL}://${CALIBDATASTORE}/%s/%s/%s" % (reco, pol.split("_")[0], ds)
      loc = "/${CALIBDATASTORE}/%s/%s/%s" % (reco, pol.split("_")[0], ds)
      out = tmpdir + "/" + ds
      cmd = "python $PIDPERFSCRIPTSROOT/scripts/python/Plots/CreateTTreeFromDataset.py %s %s" % (loc, out)
      print "  File " + out
      print "  " + cmd
      os.system(cmd)
      if "polarity" in options :
        convert_single_file(out, f2p, nt2p, treename, pidvar, ptvar, etavar, ntracksvar, transform, limits, cut, "i.Charge_idx==0")
        convert_single_file(out, f2m, nt2m, treename, pidvar, ptvar, etavar, ntracksvar, transform, limits, cut, "i.Charge_idx==1")
      elif "brem" in options :
        convert_single_file(out, f2nb, nt2nb, treename, pidvar, ptvar, etavar, ntracksvar, transform, limits, cut, "i.e_HasBremAdded==0")
        convert_single_file(out, f2b,  nt2b,  treename, pidvar, ptvar, etavar, ntracksvar, transform, limits, cut, "i.e_HasBremAdded==1")
      else :
        convert_single_file(out, f2, nt2, treename, pidvar, ptvar, etavar, ntracksvar, transform, limits, cut)
      os.remove(out)
    else : 
      # Calo samples are already trees
      convert_calo_file(ds, f2, nt2, treename, pidvar, ptvar, etavar, transform)

  if "polarity" in options : 
    f2p.cd()
    nt2p.Write()
    f2p.Close()
    f2m.cd()
    nt2m.Write()
    f2m.Close()
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile_p, eosdir, configname))
    os.remove(calibfile_p)
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile_m, eosdir, configname))
    os.remove(calibfile_m)
  elif "brem" in options : 
    f2nb.cd()
    nt2nb.Write()
    f2nb.Close()
    f2b.cd()
    nt2b.Write()
    f2b.Close()
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile_nb, eosdir, configname))
    os.remove(calibfile_nb)
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile_b, eosdir, configname))
    os.remove(calibfile_b)
  else : 
    nt2.Write()
    f2.Close()
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile, eosdir, configname))
    os.remove(calibfile)
    if "test" in options : 
      print "Now run:"
      print "root -l %s/%s/%s" % (eosrootdir, configname, calibfile)
