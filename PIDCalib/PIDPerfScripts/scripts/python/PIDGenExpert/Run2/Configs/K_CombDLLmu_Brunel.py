config = {   'bins': 100,
    'controlstat': 5000000,
    'gamma': 1.0,
    'limits': (-15.0, 14.0),
    'name': 'K_CombDLLmu_Brunel',
    'nbootstrap': 10,
    'sample': 'K_DSt_Brunel',
    'scale_default': 0.1,
    'scale_pid': 1.0,
    'scale_syst': 0.15,
    'toystat': 10000000,
    'var': 'probe_Brunel_PIDmu'}
