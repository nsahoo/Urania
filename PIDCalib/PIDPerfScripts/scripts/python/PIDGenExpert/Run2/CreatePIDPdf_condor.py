import os, sys, signal

"""
Create PID PDFs from calibration ntuples, as well as some auxiliary
PDFs (for systematic and stat. error evaluation)
"""

cwd = os.getcwd()

from Config import *

def signal_handler(signal, frame):
    print 'Exiting'
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

if len(sys.argv)>1 : 
  configname = sys.argv[1]
else : 
  print "Usage: CreatePIDPdf [config] [option1:option2:...] [dataset1:dataset2:...]"
  print "  configs are: "
  for i in sorted(configs().keys()) : 
    print "    ", i
  print "  options are: "
  print "     dry - Dry run (do not submit jobs, just print command lines)"
  print "     continue - Continue unfinished jobs (only run on the datasets where control plots are missing in eos)"
  print "     polarity - Create separate templates for positive and negative track polarities"
  print "     brem     - Create separate templates for HasBremAdded=0 and 1 electrons"
  sys.exit(0)

opt = ""
if len(sys.argv)>2 : 
  opt = sys.argv[2]
if len(sys.argv)>3 : 
  file_list = sys.argv[3].split(":")
else : 
  file_list = datasets.keys()
if "polarity" in opt.split(":") : 
  file_list2 = []
  for f in file_list : 
    file_list2 += [ f + "_M" ]
    file_list2 += [ f + "_P" ]
  file_list = file_list2
elif "brem" in opt.split(":") : 
  file_list2 = []
  for f in file_list : 
    file_list2 += [ f + "_Brem" ]
    file_list2 += [ f + "_NoBrem" ]
  file_list = file_list2

config = configs()[configname]

scale_default = config['scale_default']
scale_syst = config['scale_syst']
scale_pid = config['scale_pid']
var = "PID"
toystat = config['toystat']
controlstat = config['controlstat']
nbootstrap = config['nbootstrap']
sample = config['sample']

limits = None
if 'limits' in config : 
  limits = config['limits']

scr = "lb-run Urania/v8r0 python %s/PIDPdf.py" % (cwd)

ready_list = []
if "continue" in opt.split(":") : 
  print "ls -1 %s/%s/control/ > all.txt" % (eosdir, configname)
  os.system("ls -1 %s/%s/control/ > all.txt" % (eosdir, configname) )
  f = open("all.txt")
  for l in f : ready_list += [ "control/" + l.strip() ]
  f.close()
  print ready_list

def create_submission_scripts(command, suffix) : 
  preamble = '''#!/bin/sh
export CVMFS=/cvmfs/lhcb.cern.ch
export LCGSYS=$CVMFS/lib/lcg
export CMTCONFIG=x86_64-centos7-gcc8-opt
export MYSITEROOT=/cvmfs/lhcb.cern.ch/lib
export HOME=yourhome
export USERAREA=$HOME/cmtuser
export X509_CERT_DIR=$CVMFS/etc/grid-security/certificates
export X509_VOMS_DIR=$CVMFS/etc/grid-security/vomsdir
export X509_PROXY=$HOME/.grid_proxy
export X509_USER_PROXY=$X509_PROXY
source $MYSITEROOT/LbLogin.sh --cmtconfig=$CMTCONFIG --mysiteroot=$MYSITEROOT --userarea=$USERAREA
export URANIA=$HOME/UraniaDev_v8r0
export PERFMAPSROOT=$URANIA/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack
'''

  submission = '''executable = job_%s.sh
output = out.$(ClusterId).$(ProcId).out
error = err.$(ClusterId).$(ProcId).err
log = log.$(ClusterId).$(ProcId).log
RequestCpus = 1
+JobFlavour = "nextweek"

queue
''' % suffix

  f = open("job_" + suffix + ".sub", "w")
  f.write(submission)
  f.close()

  f = open("job_" + suffix + ".sh", "w")
  f.write(preamble + "\n" + command + "\n")
  f.close()

  os.system("condor_submit job_" + suffix + ".sub")

for filename in file_list : 

  infile = configname + "/" + filename + ".root"
  outdir = eosrootdir + "/" + configname + "/"

  nominalfile = filename + "_distrib.root"
  controlfile = "control/" + filename + "_control.root"
  figfile = "plots/" + filename + "_control.png"

  command = "%s -s %f -i %f -v %s -o %s -t %d -p %d -b %s -c %s -f %s %s/%s" % \
          (scr, scale_default, scale_pid, var, outdir, toystat, controlstat, nominalfile, controlfile, figfile, eosrootdir, infile)
  if limits : 
    command += " -l %f -u %f" % (limits[0], limits[1])

  suffix = configname + "_" + filename

  if controlfile not in ready_list : 
    print command
    if ("dry" not in opt.split(":")) : create_submission_scripts(command, suffix + "_default")

  systfile    = filename + "_syst_1.root"
  controlfile = "control/" + filename + "_syst_1_control.root"
  figfile     = "plots/" + filename + "_syst_1_control.png"

  if controlfile not in ready_list and scale_syst : 
    command = "%s -s %f -i %f -v %s -o %s -t %d -p %d -b %s -c %s -f %s %s/%s" % \
          (scr, scale_syst, scale_pid, var, outdir, toystat, controlstat, systfile, controlfile, figfile, eosrootdir, infile)
    if limits : 
      command += " -l %f -u %f" % (limits[0], limits[1])
    print command
    if ("dry" not in opt.split(":")) : create_submission_scripts(command, suffix + "_syst_1")

  for i in range(0, nbootstrap) : 

    statfile    = filename + "_stat_%d.root" % i
    tmpfile     = "%s/%s_stat_%d_tmp.root" % (tmpdir, filename, i)
    controlfile = "control/" + filename + "_stat_%d_control.root" % i
    figfile     = "plots/" + filename + "_stat_%d_control.png" % i
    command = "%s -a %s -r %d -s %f -i %f -v %s -o %s -t %d -p %d -b %s -c %s -f %s %s/%s" % \
            (scr, tmpfile, i+1, scale_default, scale_pid, var, outdir, toystat, controlstat, statfile, controlfile, figfile, eosrootdir, infile)
    if limits : 
      command += " -l %f -u %f" % (limits[0], limits[1])

#    print controlfile

    if controlfile not in ready_list : 
      print command
      if ("dry" not in opt.split(":")) : create_submission_scripts(command, suffix + "_stat_" + str(i))
