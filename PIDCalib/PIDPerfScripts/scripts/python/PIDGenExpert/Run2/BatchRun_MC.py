conf = [

#     "K_CombDLLK_Brunel", 
     "K_CombDLLmu_Brunel",
#     "K_CombDLLp_Brunel",
#     "K_MC15TuneV1_ProbNNK_Brunel",
#     "K_MC15TuneV1_ProbNNK_Brunel_Mod2",
#     "K_MC15TuneV1_ProbNNmu_Brunel",
#     "K_MC15TuneV1_ProbNNp_Brunel",
#     "K_MC15TuneV1_ProbNNpi_Brunel",

#     "pi_CombDLLK_Brunel",
     "pi_CombDLLmu_Brunel",
#     "pi_CombDLLp_Brunel",
#     "pi_MC15TuneV1_ProbNNK_Brunel",
#     "pi_MC15TuneV1_ProbNNmu_Brunel",
#     "pi_MC15TuneV1_ProbNNp_Brunel",
#     "pi_MC15TuneV1_ProbNNpi_Brunel",
#     "pi_MC15TuneV1_ProbNNpi_Brunel_Mod2",

]

dss = [ "MagUp_2017", "MagDown_2017" ] 

import os

for i in conf : 
  for ds in dss : 
#    os.system("python MakeTuples_MC.py %s - %s" % (i, ds) )
    os.system("python CreatePIDPdf_MC_condor.py %s - %s" % (i, ds) )
#    os.system("python ComparePDFs.py %s %s" % (i, ds) )
#    os.system("python CreatePIDPdf_condor.py %s continue %s" % (i, ds) )
