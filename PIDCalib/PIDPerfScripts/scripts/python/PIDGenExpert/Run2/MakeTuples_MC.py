import os, sys
from ROOT import TFile, TNtuple, gDirectory
from math import sqrt, log

import ConfigMC

def convert_single_file(infile, indir, f2, nt2, treenames, pidvar, ptvar, etavar, ntracksvar, transform, cut = None, pol_cut = "") :
  f1 = TFile.Open(infile)
#  gDirectory.cd(indir)
  print "file ", infile
  if not f1 : return False
  print pol_cut

  for treename in treenames : 
    print treename
    nt1 = f1.Get(treename)
    f2.cd()
    if not nt1 : 
      continue
    nentries = nt1.GetEntries()
    print "  tuple %s: %d entries" % (treename, nentries )
    n = 0
    if isinstance(transform, str) : 
      pid_code = transform
    else : 
      gamma = transform
      if gamma<0 : 
        pid_code = compile("(1.-(1.-i.%s)**%f)" % (pidvar, abs(gamma)), '<string>', 'eval')
      else : 
        pid_code = compile("(i.%s)**%f" % (pidvar, abs(gamma)), '<string>', 'eval')
    pt_code = compile("log(i.%s)" % ptvar, '<string>', 'eval')
    eta_code = compile("i.%s" % etavar, '<string>', 'eval')
    x_code = compile("i.%s" % pidvar, '<string>', 'eval')
    if ntracksvar : 
      ntracks_code = compile("log(i.%s)" % ntracksvar, '<string>', 'eval')
    if pol_cut : 
      pol_code = compile("i.%s" % pol_cut, '<string>', 'eval')
    cut_code = None
    if cut : 
      cut_code = compile(cut, "<string>", 'eval')
    for i in nt1 :
      n += 1
      if (n % 10000 == 0) : 
        print "    event %d/%d" % (n, nentries)
      try : 
#        if i.probe_MINIPCHI2 < 400 : continue
        if cut : 
          if not eval(cut_code) : continue
        if pol_cut : 
          if not eval(pol_code) : continue
        x = eval(x_code)
        pid = eval(pid_code)
        pt  = eval(pt_code)
        eta = eval(eta_code)
        ntracks = eval(ntracks_code)
        nt2.Fill(pid, pt, eta, ntracks)
      except : 
        continue

  f1.Close()
  return True

if len(sys.argv)>1 : 
  # config name, e.g. "p_ProbNNp"
  configname = sys.argv[1]
else : 
  print "Usage: python MakeTuples_MC.py [config] [option1:option2:...] [dataset1:dataset2:]"
  print "  configs are: "
  for i in sorted(ConfigMC.configs.keys()) : 
    print "    ", i
  print "  options are: "
  print "    test - run for just a single PIDCalib file rather than whole dataset"
  print "  datasets can be, e.g. "
  print "    MagDown_2012:MagUp_2011"
  print "    or leave empty to process all available datasets"
  sys.exit(0)

config = ConfigMC.configs[configname]
# sample name, e.g. "p" for proton
samplename = config['sample']
sample = ConfigMC.samples[samplename]
eosdir = ConfigMC.eosdir
eosrootdir = ConfigMC.eosrootdir

options = ""
if len(sys.argv)>2 : options = sys.argv[2].split(":")
if len(sys.argv)>3 : dslist = sys.argv[3].split(":")

# resampled variable name
var = configname

# other variable names
pidvar = config['var']
ptvar = sample['pt']
etavar = sample['eta']
ntracksvar = sample['ntracks']
treename = sample['trees']
indir = sample['dir']
transform = None
if 'gamma' in config.keys() : 
  transform = config['gamma']
elif 'transform_forward' in config.keys() : 
  transform = config['transform_forward']

cut = None
if "cut" in config.keys() : cut = config["cut"]

# Create dictionary with the lists of PIDCalib datasets for each year and magnet polarity
dsdict = {}

dss = sample['datasets']
for ds in dslist : 
  if not isinstance(dss[ds], tuple) : 
    dsdict[ds] = [ dss[ds] ] 
  else :
    dsdict[ds] = []
    n = dss[ds][1]
    for i in range(0, n) : 
      dsdict[ds] += [ dss[ds][0] % i ]

os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select mkdir -p %s/%s" % (eosdir, configname))

# Loop over PIDCalib datasets
for pol,dss in dsdict.iteritems() : 

  if "polarity" in options : 
    calibfile_p = pol + "_P.root"
    f2p = TFile.Open(calibfile_p,"RECREATE")
    nt2p = TNtuple("pid","pid", "PID:Pt:Eta:Ntracks")
    calibfile_m = pol + "_M.root"
    f2m = TFile.Open(calibfile_m,"RECREATE")
    nt2m = TNtuple("pid","pid", "PID:Pt:Eta:Ntracks")
  else : 
    calibfile = pol + ".root"
    f2 = TFile.Open(calibfile,"RECREATE")
    nt2 = TNtuple("pid","pid", "PID:Pt:Eta:Ntracks")

  print "Polarity " + pol

  ntracksvar2 = ntracksvar
  if isinstance(ntracksvar, dict) : 
    ntracksvar2 = ntracksvar[pol]

  nds = 0
  for ds in dss : 
    infile = eosrootdir + "/" + ds
    if "polarity" in options : 
      polarity_var = "probe_Brunel_charge"
      if "polarity" in sample : polarity_var = sample["polarity"]
      p_cut = "%s>0" % polarity_var
      m_cut = "%s<0" % polarity_var
      ok = convert_single_file(infile, indir, f2p, nt2p, treename, pidvar, ptvar, etavar, ntracksvar2, transform, cut, p_cut)
      ok = convert_single_file(infile, indir, f2m, nt2m, treename, pidvar, ptvar, etavar, ntracksvar2, transform, cut, m_cut)
    else : 
      ok = convert_single_file(infile, indir, f2, nt2, treename, pidvar, ptvar, etavar, ntracksvar2, transform, cut)
    if ok : nds += 1
    if nds >= 2 and "test" in options : 
      break

  if "polarity" in options : 
    f2p.cd()
    nt2p.Write()
    f2p.Close()
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile_p, eosdir, configname))
    os.remove(calibfile_p)
    f2m.cd()
    nt2m.Write()
    f2m.Close()
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile_m, eosdir, configname))
    os.remove(calibfile_m)
  else : 
    f2.cd()
    nt2.Write()
    f2.Close()
    os.system("/afs/cern.ch/project/eos/installation/0.3.15/bin/eos.select cp %s %s/%s/" % (calibfile, eosdir, configname))
    os.remove(calibfile)

  if "test" in options : 
    print "Now run:"
    print "root -l %s/%s/%s" % (eosrootdir, configname, calibfile)
