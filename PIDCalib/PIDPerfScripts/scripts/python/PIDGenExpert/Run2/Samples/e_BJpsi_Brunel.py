sample = {   'eta': 'probe_Brunel_ETA',
    'name': 'e_BJpsi_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': ('B_Jpsi_EPTuple/DecayTree', 'B_Jpsi_EMTuple/DecayTree'),
    'weight': 'probe_sWeight'}
