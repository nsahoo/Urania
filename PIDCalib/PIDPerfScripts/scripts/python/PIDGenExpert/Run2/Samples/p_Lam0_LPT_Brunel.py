sample = {   'eta': 'probe_Brunel_ETA',
    'name': 'p_Lam0_LPT_Brunel',
    'ntracks': 'nTracks',
    'pt': 'probe_Brunel_PT',
    'trees': ('Lam0_PTuple/DecayTree', 'Lam0_PbarTuple/DecayTree'),
    'weight': 'probe_sWeight'}
