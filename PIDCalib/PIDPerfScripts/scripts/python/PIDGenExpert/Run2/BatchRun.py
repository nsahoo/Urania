conf = [
#     "K_CombDLLK_Brunel", 
#     "K_CombDLLmu_Brunel",
#     "K_CombDLLp_Brunel",
#     "K_MC15TuneV1_ProbNNKNotpi_Brunel",
#     "K_MC15TuneV1_ProbNNK_Brunel",
#     "K_MC15TuneV1_ProbNNK_Brunel_Mod2",
#     "K_MC15TuneV1_ProbNNmu_Brunel",
#     "K_MC15TuneV1_ProbNNp_Brunel",
#     "K_MC15TuneV1_ProbNNpiNotK_Brunel",
#     "K_MC15TuneV1_ProbNNpi_Brunel",

#     "pi_CombDLLK_Brunel",
#     "pi_CombDLLe_Brunel",
#     "pi_CombDLLmu_Brunel",
#     "pi_CombDLLp_Brunel",
#     "pi_MC15TuneV1_ProbNNKNotpi_Brunel",
#     "pi_MC15TuneV1_ProbNNK_Brunel",
#     "pi_MC15TuneV1_ProbNNe_Brunel",
#     "pi_MC15TuneV1_ProbNNmu_Brunel",
#     "pi_MC15TuneV1_ProbNNp_Brunel",
#     "pi_MC15TuneV1_ProbNNpiNotK_Brunel",
#     "pi_MC15TuneV1_ProbNNpi_Brunel",
#     "pi_MC15TuneV1_ProbNNpi_Brunel_Mod2",

#     "p_LbLcPi_MC15TuneV1_ProbNNK_Brunel",
#     "p_LbLcPi_MC15TuneV1_ProbNNp_Brunel",
#     "p_LbLcPi_MC15TuneV1_ProbNNpi_Brunel",
#     "p_CombDLLK_Brunel", 
#     "p_CombDLLp_Brunel",

#     "mu_CombDLLK_Brunel", 
#     "mu_CombDLLK_IsMuon_Brunel", 
#     "mu_CombDLLe_Brunel", 
#     "mu_CombDLLmu_Brunel", 
#     "mu_CombDLLmu_IsMuon_Brunel", 
#     "mu_CombDLLp_Brunel", 
#     "mu_MC15TuneV1_ProbNNK_Brunel", 
#     "mu_MC15TuneV1_ProbNNmu_Brunel", 
#     "mu_MC15TuneV1_ProbNNpi_Brunel", 

#     "p_MC15TuneV1_ProbNNK_Brunel",
#     "p_MC15TuneV1_ProbNNp_Brunel",
#     "p_MC15TuneV1_ProbNNpi_Brunel",

      "p_LbLcPi_DLLpgt5_DLLpKgt5_MC15TuneV1_ProbNNp_Brunel", 

]

#dss = [ "MagUp_2018", "MagDown_2018" ] 
#dss = [ "MagUp_2017", "MagDown_2017" ] 
dss = [ "MagUp_2016", "MagDown_2016", "MagUp_2017","MagDown_2017", "MagUp_2018", "MagDown_2018" ]

import os

for i in conf : 
  for ds in dss : 
    os.system("python MakeTuples_numpy.py %s - %s" % (i, ds) )
#    os.system("python CreatePIDPdf_condor.py %s - %s" % (i, ds) )
#    os.system("python ComparePDFs.py %s %s" % (i, ds) )
#    os.system("python CreatePIDPdf_condor.py %s continue %s" % (i, ds) )
