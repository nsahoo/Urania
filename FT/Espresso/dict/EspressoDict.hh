#ifndef DICT_ESPRESSO_ESPRESSODICT_H 
#define DICT_ESPRESSO_ESPRESSODICT_H 1

#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winconsistent-missing-override"
#elif defined(__GNUC__) && __GNUC__ >= 5
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsuggest-override"
#endif

#include "Espresso/GLMBuilder.hh"
#include "Espresso/RooGLMFunction.hh"
#include "Espresso/GLMNSpline.hh"
#include "Espresso/GLMBSpline.hh"
#include "Espresso/GLMPolynomial.hh"
#include "Espresso/GSLWrappers.hh"

#endif // DICT_ESPRESSO_ESPRESSODICT_H
