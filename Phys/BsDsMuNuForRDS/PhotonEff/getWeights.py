#!/usr/bin/env python

from ROOT import *
from math import *

def getBin(gammaPT):
  """ Get the bin number in which the photon falls """
  binNumber = -1
  
  if gammaPT < binsPT[1]:
    binNumber = 0
  if gammaPT > binsPT[1] and gammaPT < binsPT[2]:
    binNumber = 1
  if gammaPT > binsPT[2] and gammaPT < binsPT[3]:
    binNumber = 2
  if gammaPT > binsPT[3] and gammaPT < binsPT[4]:
    binNumber = 3
  if gammaPT > binsPT[4] and gammaPT < binsPT[5]:
    binNumber = 4
  if gammaPT > binsPT[5] and gammaPT < binsPT[6]:
    binNumber = 5
  if gammaPT > binsPT[6] and gammaPT < binsPT[7]:
    binNumber = 6
  if gammaPT > binsPT[7] and gammaPT < binsPT[8]:
    binNumber = 7

  return binNumber


FileNum = TFile('BsMassForPhotonEff_WithSWeighted_Num.root','r')
TreeNum = FileNum.Get("DecayTree")

FileDen = TFile('BsMassForPhotonEff_WithSWeighted_Den.root','r')
TreeDen = FileDen.Get("DecayTree")

#binsPT = [0,250,400,500,600,800,1500,3000,20000]
binsPT = [0,2000,3000,4000,5000,6000,8000,10000,50000]

nentriesNum = TreeNum.GetEntries()
nentriesDen = TreeDen.GetEntries()
histosDen = {}
histosNum = {}
print "Total number of entries num = %s, and den = %s" %(nentriesNum,nentriesDen) 

#hTest = TH2F("htest","htest",100,0,20000,100,0,20000)

for i in range(nentriesNum):
  TreeNum.GetEntry(i)
  sigden = TreeNum.sigden
  signum = TreeNum.signum
  gammaPT = TreeNum.Gamma_PT
  gammaP = TreeNum.Gamma_P
  Ds_PT = TreeNum.Ds_PT
  mDsGPi = TreeNum.MassDsGPi
  mDsG = TreeNum.MassDsG
  Bs_visM = TreeNum.MassBs
  B_P = TreeNum.Bs_P
  B_PE = TreeNum.Bs_E
  #estimatePhotonP = (5366**2-Bs_visM**2)/(2*(B_PE-B_P))
  #hTest.Fill(estimatePhotonP,gammaP,signum)
  theBin = getBin(Ds_PT)
  if theBin < 0:
    print "ERROR: Not able to find the correct bin numerator %s" %(Ds_PT)
    continue
  try:
    histosNum[theBin].Fill(mDsGPi-mDsG+2112,signum)
  except:
    histosNum[theBin] = TH1F("h"+str(theBin),"h"+str(theBin),100,5250,5700)
    histosNum[theBin].Fill(mDsGPi-mDsG+2112,signum)

#hTest.Draw()

for i in range(nentriesDen):
  TreeDen.GetEntry(i)
  gammaPT = TreeDen.Gamma_PT
  DsPT = TreeDen.Ds_PT
  DsP = TreeDen.Ds_P
  Bs_visM = TreeDen.BMass
  B_P = TreeDen.B_P
  B_PE = TreeDen.B_E
  sigden = TreeDen.sigden
  #if gammaPT>0:
    #theBin = getBin(gammaPT)
  #estimatePhotonP = (5366**2-Bs_visM**2)/(2*(B_PE-B_P))
  theBin = getBin(DsPT)
  if theBin < 0:
    #print "ERROR: Not able to find the correct bin %s" %(gammaPT)
     print "ERROR: Not able to find the correct bin denominator %s" %(DsPT)
     continue
  try:
#      histosDen[theBin].Fill(gammaPT,sigden)
    histosDen[theBin].Fill(DsPT,sigden)
  except:
    histosDen[theBin] = TH1F("h"+str(theBin),"h"+str(theBin),100,0,50000)
#      histosDen[theBin].Fill(gammaPT,sigden)
    histosDen[theBin].Fill(DsPT,sigden)

cNum = {}
cDen = {}

#for key in histosNum.keys():
#  cNum[key] = TCanvas()
#  histosNum[key].Draw()
#  cNum[key].SaveAs("num_bin"+str(key)+".pdf")

#for key in histosDen.keys():
#  cDen[key] = TCanvas()
#  histosDen[key].Draw()
#  cDen[key].SaveAs("den_bin"+str(key)+".pdf")

totDen = 0
totNum = 0
out = open("PhotonEff.txt","w")

histoEffs = TGraphErrors()
cEffs = TCanvas()

for key in histosNum.keys(): 
  print "in bin = %s, number of photons = %s" %(key, histosNum[key].GetSumOfWeights())
  print "in bin = %s, number of denominator = %s" %(key, histosDen[key].GetSumOfWeights())
  totNum += histosNum[key].GetSumOfWeights() 
  totDen += histosDen[key].GetSumOfWeights()
  eff = histosNum[key].GetSumOfWeights()/histosDen[key].GetSumOfWeights()
  err = sqrt(eff*(1-eff)/histosDen[key].GetSumOfWeights())
  
  print "in bin = %s, eff = %s" %(key, eff)
  out.write(str(key)+" "+str(eff)+"\n")
  histoEffs.SetPoint(key,(binsPT[key]+binsPT[key+1])/2.,eff)
  histoEffs.SetPointError(key,(binsPT[key+1]-binsPT[key])/2.,err)
out.close() 
print totNum, totDen

#gPad.SetLogx()
histoEffs.GetYaxis().SetTitle("Efficiency")
histoEffs.GetXaxis().SetTitle("Ds pT (MeV)")
histoEffs.SetMinimum(0)
histoEffs.SetMarkerStyle(8)
histoEffs.Draw("AP")
cEffs.SaveAs("effs_vsDsPT.pdf")

