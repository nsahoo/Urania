import sys, os

#dataTags = ['MC_AllNeutrals_iso','InclDs_AllNeutrals_iso']
dataTags = ['MC_LbLcDs', 'MC_BdDstDs', 'MC_BsDsDs', 'MC_BuD0Ds', 'MC_Signal', 'MC_Tau']
#dataTags = ['Data15', 'MC_BuD0Ds']
#dataTags = ['MC_Signal', 'MC_LbLcDs', 'MC_BdDstDs', 'MC_BsDsDs', 'MC_BuD0Ds']
#dataTags = ['MC_Signal', 'MC_LbLcDs', 'MC_BuD0Ds']
#dataTags = ['MC_BsDsPi']

for dataTag in dataTags:
    os.system('python makeDalitzTuple.py %s' %(dataTag) )
