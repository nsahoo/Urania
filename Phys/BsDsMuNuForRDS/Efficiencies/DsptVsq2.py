#!/usr/bin/env python

from ROOT import *

f = TFile("/eos/lhcb/wg/semileptonic/Bs2DsX/WeightedNtuples/sigmu_SWeighted_Dsst_WeightPID_Sim09b.root")
t = f.Get("DecayTree")

binsPT = [0,3000,4000,5000,6000,7000,8000,10000,30000]
binsQ2 = [0,1000000,2000000,3000000,4000000,5000000,6000000,7000000,8000000,9000000,10000000,11000000]
h = TH2F("h", "h", 8, 0, 30000, 11, 0, 11000000)
entries = t.GetEntries()

for i in range(entries):
  t.GetEntry(i)
  q2 = t.q2Reg
  pt = t.Ds_PT
  w = t.sigw
  h.Fill(pt, q2, w)

c1 = TCanvas()
h.SetMinimum(0)
h.Draw("COLZ")
c1.SaveAs("pTvsq2.pdf")

print "Correlation Factor = %s" %(h.GetCorrelationFactor())
