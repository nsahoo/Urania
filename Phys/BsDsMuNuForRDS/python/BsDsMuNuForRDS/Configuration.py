################################################################################
## Module to hold all the configuration information for each particular
## data type. This includes the information for data retrieval, selections
## to be employed, truth matching for MC, etc.
################################################################################
# First some objects which we can reuse.
# These are the locations of the tuples in each file to be processed.
tupleListStandard = [ 'B2DsMuNuTuple/DecayTree' ]
tupleListSS = [ 'B2DsMuNuTuple/DecayTree', 'B2DsMuNuSSTuple/DecayTree' ]
tupleListFull = [ 'B2DsMuNuTuple/DecayTree' ,'B2DsMuNuSSTuple/DecayTree'
                 ,'B2DsMuNuTupleFake/DecayTree', 'B2DsMuNuSSTupleFake/DecayTree' ]
tupleListNoFake   = [ 'MB2DsMuNuTuple/DecayTree'
                      ,'B2DsMuNuTuple/DecayTree'
                      ,'B2DsMuNuSSTuple/DecayTree' ]
tupleListFakeOnly = [ 'B2DsMuNuTupleFake/DecayTree', 'B2DsMuNuSSTupleFake/DecayTree' ] 

################################################################################
# For each dataTag we have a configuration dictionary.
# In my experience it's better to be explicit about the configuration,
# so we have several mandatory entries to be filled out.
################################################################################
configurationDict = {}

# The signal mu+tau MC as example to describe the required fields.
configurationDict['MC_Signal'] = {
    # The tuple locations resident in the file.
    'tNames'      : tupleListStandard 
    # The list of input, non-processed files.
    ,'inputFiles' : [ 'TupleRDS_Sim09b_DsMuNu_Up.root', 'TupleRDS_Sim09b_DsMuNu_Down.root' ]
    # The name of the single output file.
    ,'outputFile' : 'TupleRDS_Sim09b_DsMuNu.root'
    # One of 'signal' or 'doubleCharm' or 'kinematics' if we want MC matching.
    ,'truthMatch' : 'signal'
    # Optional entry for when the input file is residing in
    # another location, and not the shared repository.
    ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/FF/MC/'
    }

configurationDict['MC_Tau'] = { 'tNames'      : tupleListStandard
                               ,'inputFiles' : [ 'TupleRDS_Sim09b_DsTauNu_Up.root', 'TupleRDS_Sim09b_DsTauNu_Down.root']
                               ,'outputFile' : 'TupleRDS_Sim09b_DsTauNu.root'
                               ,'truthMatch' : 'signal'
                               ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/FF/MC/'
                              }

### for kinematic studies
configurationDict['MC_BsDsPi'] = { 'tNames'      : tupleListStandard
                               ,'inputFiles' : [ 'BsDsPi_kinematics_MC15.root']
                               ,'outputFile' : 'BsDsPi_kinematics_MC15.root'
                               ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/BsDsPi/MC/'
                              }

configurationDict['Data_BsDsPi'] = { 'tNames'      : tupleListStandard
                               ,'inputFiles' : [ 'BsDsPi_kinematics_data.root']
                               ,'outputFile' : 'BsDsPi_kinematics_data.root'
                               ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/BsDsPi/Data/'
                              }

# Now add the data tags.
# This is the data for the RDs primary analysis.
configurationDict['Data15'] = { 'tNames'      : tupleListSS
                                ,'inputFiles' : ['TupleRDS_Data15_Up.root'
                                                ,'TupleRDS_Data15_Down.root']
                                ,'outputFile' : 'TupleRDS_Data15.root'
                                ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/FF/Data/'
                                }

configurationDict['Data16'] = { 'tNames'      : tupleListSS
                                ,'inputFiles' : ['TupleRDS_Data16_Up1.root'
                                                ,'TupleRDS_Data16_Up2.root'
                                                ,'TupleRDS_Data16_Up3.root'
                                                ,'TupleRDS_Data16_Up4.root'
                                                ,'TupleRDS_Data16_Up5.root'
                                                ,'TupleRDS_Data16_Up6.root'
                                                ,'TupleRDS_Data16_Up7.root'
                                                ,'TupleRDS_Data16_Down1.root'
                                                ,'TupleRDS_Data16_Down2.root'
                                                ,'TupleRDS_Data16_Down3.root'
                                                ,'TupleRDS_Data16_Down4.root'
                                                ,'TupleRDS_Data16_Down5.root'
                                                ,'TupleRDS_Data16_Down6.root'
                                                ,'TupleRDS_Data16_Down7.root']
                                ,'outputFile' : 'TupleRDS_Data16.root'
                                ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/FF/Data/'
                                }

# The dataset for the Ds*mu/Ds*pi branching ratio analysis.
configurationDict['Data15_BR'] = { 'tNames'      : tupleListStandard
                                   ,'inputFiles' : ['TupleRDS_DataUp_1_allNeutrals_iso.root'
                                                    ,'TupleRDS_DataUp_2_allNeutrals_iso.root'
                                                    ,'TupleRDS_DataDown_1_allNeutrals_iso.root'
                                                    ,'TupleRDS_DataDown_2_allNeutrals_iso.root']
                                   ,'outputFile' : 'Data15_BR.root'
                                   ,'inputDir'   : '/eos/lhcb/user/r/rvazquez/RDS/'
                                   }

# The MC08 double charm are all the same except for one string, so loop over these.
for doubleCharmTag in ['LbLcDs', 'BdDstDs', 'BsDsDs', 'BuD0Ds']:
    theDict = { 'tNames'      : tupleListStandard
                ,'inputFiles' : [ 'TupleRDS_Sim09b_%s_Up.root' %doubleCharmTag
                                  ,'TupleRDS_Sim09b_%s_Down.root' %doubleCharmTag ]
                ,'outputFile' : 'TupleRDS_Sim09b_%s.root' %doubleCharmTag
                ,'truthMatch' : 'doubleCharm'
                ,'inputDir'   : '/eos/lhcb/wg/semileptonic/Bs2DsX/OriginalTuples/FF/MC/'
                }
    configurationDict['MC_%s' %doubleCharmTag] = theDict

################################################################################
# Dump the information if the module is run directly.
################################################################################
if __name__=='__main__':
    print '- Dumping configuration settings.'
    for tag, configDict in configurationDict.iteritems():
        print '*'*20
        print '- For tag %s:' %tag
        for setting, value in configDict.iteritems():
            print '-- {:20s}'.format(setting), value
        
