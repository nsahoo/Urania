#ifndef DATA_H
#define DATA_H

#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "TVector3.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TLorentzVector.h"

using namespace std;

class Data {
public:
  vector<double> *pvx,*pvy,*pvz;
  vector<double> *tag_ipv,*tag_prt0,*tag_prt1;
  vector<double> *tag_px,*tag_py,*tag_pz,*tag_e;
  vector<double> *tag_x,*tag_y,*tag_z,*tag_brem;
  vector<double> *tag_chi2,*tag_ipchi2,*tag_fdchi2;
  vector<double> *prt_ipv,*prt_pid,*prt_brem;
  vector<double> *prt_px,*prt_py,*prt_pz,*prt_e;
  vector<double> *prt_ghost,*prt_pnne,*prt_ipchi2;
  vector<double> *prt_hx,*prt_hy,*prt_hz;
  vector<double> *prt_ecx,*prt_ecy,*prt_ecz;
  vector<double> *l0_x,*l0_y,*l0_z,*l0_dx,*l0_et,*l0_id;
  double spd;
  vector<double> *tag_gen,*prt_gen,*mctag_prt0,*mctag_prt1;
  vector<double> *mctag_px,*mctag_py,*mctag_pz,*mctag_e;
  vector<double> *mcprt_px,*mcprt_py,*mcprt_pz,*mcprt_e,*mcprt_pid;
  bool mc;

  Data(bool ismc=false){
    mc = ismc;
    pvx = new vector<double>();
    pvy = new vector<double>();
    pvz = new vector<double>();
    tag_ipv = new vector<double>();
    tag_prt0 = new vector<double>();
    tag_prt1 = new vector<double>();
    tag_px = new vector<double>();
    tag_py = new vector<double>();
    tag_pz = new vector<double>();
    tag_e = new vector<double>();
    tag_x = new vector<double>();
    tag_y = new vector<double>();
    tag_z = new vector<double>();
    tag_brem = new vector<double>();
    tag_chi2 = new vector<double>();
    tag_ipchi2 = new vector<double>();
    tag_fdchi2 = new vector<double>();
    prt_ipv = new vector<double>();
    prt_pid = new vector<double>();
    prt_brem = new vector<double>();
    prt_px = new vector<double>();
    prt_py = new vector<double>();
    prt_pz = new vector<double>();
    prt_e = new vector<double>();
    prt_ghost = new vector<double>();
    prt_pnne = new vector<double>();
    prt_ipchi2 = new vector<double>();
    prt_hx = new vector<double>();
    prt_hy = new vector<double>();
    prt_hz = new vector<double>();
    prt_ecx = new vector<double>();
    prt_ecy = new vector<double>();
    prt_ecz = new vector<double>();
    l0_x = new vector<double>();
    l0_y = new vector<double>();
    l0_z = new vector<double>();
    l0_dx = new vector<double>();
    l0_et = new vector<double>();
    l0_id = new vector<double>();
    if(mc){
      tag_gen = new vector<double>();
      prt_gen = new vector<double>();
      mctag_prt0 = new vector<double>();
      mctag_prt1 = new vector<double>();
      mctag_px = new vector<double>();
      mctag_py = new vector<double>();
      mctag_pz = new vector<double>();
      mctag_e = new vector<double>();
      mcprt_px = new vector<double>();
      mcprt_py = new vector<double>();
      mcprt_pz = new vector<double>();
      mcprt_e = new vector<double>();
      mcprt_pid = new vector<double>();
    }
  }

  ~Data(){
    delete pvx; delete pvy; delete pvz;
    delete tag_ipv; delete tag_prt0; delete tag_prt1;
    delete tag_px; delete tag_py; delete tag_pz; delete tag_e;
    delete tag_x; delete tag_y; delete tag_z; delete tag_brem;
    delete tag_chi2; delete tag_ipchi2; delete tag_fdchi2;
    delete prt_ipv; delete prt_pid; delete prt_brem;
    delete prt_px; delete prt_py; delete prt_pz; delete prt_e;
    delete prt_ghost; delete prt_pnne; delete prt_ipchi2;
    delete prt_hx; delete prt_hy; delete prt_hz;
    delete prt_ecx; delete prt_ecy; delete prt_ecz;
    delete l0_x; delete l0_y; delete l0_z; delete l0_dx; delete l0_et; delete l0_id;
    if(mc){
      delete tag_gen; delete prt_gen; delete mctag_prt0; delete mctag_prt1;
      delete mctag_px; delete mctag_py; delete mctag_pz; delete mctag_e;
      delete mcprt_px; delete mcprt_py; delete mcprt_pz; delete mcprt_e;
      delete mcprt_pid;
    }
  }

  void init(TTree *t){
    t->SetBranchAddress("pvr_x",&pvx);
    t->SetBranchAddress("pvr_y",&pvy);
    t->SetBranchAddress("pvr_z",&pvz);
    t->SetBranchAddress("tag_idx_pvr",&tag_ipv);
    t->SetBranchAddress("tag_idx_prt0",&tag_prt0);
    t->SetBranchAddress("tag_idx_prt1",&tag_prt1);
    t->SetBranchAddress("tag_px",&tag_px);
    t->SetBranchAddress("tag_py",&tag_py);
    t->SetBranchAddress("tag_pz",&tag_pz);
    t->SetBranchAddress("tag_e",&tag_e);
    t->SetBranchAddress("tag_x",&tag_x);
    t->SetBranchAddress("tag_y",&tag_y);
    t->SetBranchAddress("tag_z",&tag_z);
    t->SetBranchAddress("tag_brem",&tag_brem);
    t->SetBranchAddress("tag_chi2",&tag_chi2);
    t->SetBranchAddress("tag_ip_chi2",&tag_ipchi2);
    t->SetBranchAddress("tag_fd_chi2",&tag_fdchi2);
    t->SetBranchAddress("prt_idx_pvr",&prt_ipv);
    t->SetBranchAddress("prt_pid",&prt_pid);
    t->SetBranchAddress("prt_brem",&prt_brem);
    t->SetBranchAddress("prt_px",&prt_px);
    t->SetBranchAddress("prt_py",&prt_py);
    t->SetBranchAddress("prt_pz",&prt_pz);
    t->SetBranchAddress("prt_e",&prt_e);
    t->SetBranchAddress("prt_prb_ghost",&prt_ghost);
    t->SetBranchAddress("prt_pnn_e",&prt_pnne);
    t->SetBranchAddress("prt_ip_chi2",&prt_ipchi2);
    t->SetBranchAddress("prt_hit_x",&prt_hx);
    t->SetBranchAddress("prt_hit_y",&prt_hy);
    t->SetBranchAddress("prt_hit_z",&prt_hz);
    t->SetBranchAddress("prt_ecal_x",&prt_ecx);
    t->SetBranchAddress("prt_ecal_y",&prt_ecy);
    t->SetBranchAddress("prt_ecal_z",&prt_ecz);
    t->SetBranchAddress("l0_x",&l0_x);
    t->SetBranchAddress("l0_y",&l0_y);
    t->SetBranchAddress("l0_z",&l0_z);
    t->SetBranchAddress("l0_dx",&l0_dx);
    t->SetBranchAddress("l0_et",&l0_et);
    t->SetBranchAddress("l0_id",&l0_id);
    t->SetBranchAddress("evt_spd",&spd);
    if(mc){
      t->SetBranchAddress("tag_idx_gen",&tag_gen);
      t->SetBranchAddress("mctag_idx_prt0",&mctag_prt0);
      t->SetBranchAddress("mctag_idx_prt1",&mctag_prt1);
      t->SetBranchAddress("mctag_px",&mctag_px);
      t->SetBranchAddress("mctag_py",&mctag_py);
      t->SetBranchAddress("mctag_pz",&mctag_pz);
      t->SetBranchAddress("mctag_e",&mctag_e);
      t->SetBranchAddress("prt_idx_gen",&prt_gen);
      t->SetBranchAddress("mcprt_px",&mcprt_px);
      t->SetBranchAddress("mcprt_py",&mcprt_py);
      t->SetBranchAddress("mcprt_pz",&mcprt_pz);
      t->SetBranchAddress("mcprt_e",&mcprt_e);
      t->SetBranchAddress("mcprt_pid",&mcprt_pid);
    }
  }

  // particle p 4-momentum
  TLorentzVector getPrtP4(int p){
    TLorentzVector p4(prt_px->at(p),prt_py->at(p),prt_pz->at(p),prt_e->at(p));
    return p4;
  }

  // generator-level particle p 4-momentum
  TLorentzVector getMCPrtP4(int p){
    TLorentzVector p4(mcprt_px->at(p),mcprt_py->at(p),mcprt_pz->at(p),mcprt_e->at(p));
    return p4;
  }

  // generator-level J/psi 4-momentum (assumes there is only one per event)
  TLorentzVector getMCJpsiP4(){
    TLorentzVector p4(mctag_px->at(0),mctag_py->at(0),mctag_pz->at(0),mctag_e->at(0));
    return p4;
  }

  // get dielectron 4-momentum
  TLorentzVector getTagP4(int i){
    TLorentzVector p4(tag_px->at(i),tag_py->at(i),tag_pz->at(i),tag_e->at(i));
    return p4;
  }

  // number of L0 clusters
  int nl0(){return l0_et->size();}

  // x-y distance between particle p's track state at ECAL face and centroid
  // of cluster i (relative to L0 tolerance)
  double getPrtL0RelDist(int p, int i){
    // ignore z since we use a constant value for that, so any difference
    // there just means we chose a slightly incorrect value to project to
    double dx = prt_ecx->at(p)-l0_x->at(i);
    double dy = prt_ecy->at(p)-l0_y->at(i);
    return sqrt(dx*dx + dy*dy)/l0_dx->at(i);
  }

  // same as previous but using (approximately) the velo segment projected to
  // the ECAL rather than the track state itself (for brem recovery)
  double getPrtVeloL0RelDist(int p, int i){
    TVector3 prt_hit = getFirstHit(p);
    TLorentzVector p4 = getPrtP4(p);
    double tx = p4.Px()/p4.Pz(), ty = p4.Py()/p4.Pz();
    // 12500 is the location of the ECAL front face
    double x = tx*(12500-prt_hit.Z())+prt_hit.X();
    double y = ty*(12500-prt_hit.Z())+prt_hit.Y();
    double dx = x - l0_x->at(i), dy = y - l0_y->at(i);
    double d = sqrt(dx*dx + dy*dy) / l0_dx->at(i);
    return d;
  }

  // only ET is stored, this converts to E
  double getL0energy(int i){
    double x = l0_x->at(i), y = l0_y->at(i);
    double e = l0_et->at(i)*l0_z->at(i)/sqrt(x*x + y*y);
    return e;
  }

  // location of the first hit on the track
  TVector3 getFirstHit(int p){
    TVector3 hit(prt_hx->at(p),prt_hy->at(p),prt_hz->at(p));
    return hit;
  }

};

#endif
